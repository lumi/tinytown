{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
  };
  
  outputs = { nixpkgs, flake-utils, rust-overlay, ... }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs { inherit system; overlays = [ (import rust-overlay) ]; };
    in {
      devShells.default = pkgs.mkShell rec {
        nativeBuildInputs = with pkgs; [
          # Rust stuff
          (rust-bin.stable.latest.default.override {
            targets = [ "wasm32-unknown-unknown" ];
          })
          rust-analyzer
          rustfmt
      
          # Common dependencies
          pkg-config
          openssl

          # Graphics
          xorg.libX11
          xorg.libXcursor
          xorg.libXrandr
          xorg.libXi
          xorg.libXinerama
          xorg.libXext
          xorg.libXxf86vm
          wayland
          wayland-protocols
          vulkan-headers
          vulkan-tools
          vulkan-loader
          libva
          libGL
          libxkbcommon
          fontconfig

          # CMake
          cmake

          # Web
          wasm-pack
          binaryen
        ];

        shellHook = ''
          export LD_LIBRARY_PATH="${pkgs.lib.makeLibraryPath nativeBuildInputs}:$LD_LIBRARY_PATH"
        '';
      };
    }) // {
      pognix.extraModules = [({ ... }: {
        pognix.expose = {
          wayland = true;
          x11 = true;
          pulse = true;
          pipewire = true;
          input = true;
          gpu = true;
        };
      })];
    };
}
