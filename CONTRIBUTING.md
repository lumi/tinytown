# Contributing info

## Can I contribute?

At this point, the project is still quite in flux, but I can accept contributions.

Make sure to notify me first, though. This can be done via the issue tracker or on XMPP.

## Is there any mentoring?

Feel free to ask for mentoring on any of the open issues.
