use {
    image::RgbaImage,
    regenboog::RgbaU8,
    std::{collections::BTreeMap, fs::File, num::ParseIntError, path::PathBuf, str::FromStr},
    structopt::StructOpt,
    thiserror::Error,
};

#[derive(Debug, Error)]
enum SizeParseError {
    #[error("int parse error: {0:}")]
    ParseIntError(#[from] ParseIntError),
    #[error("malformed input")]
    Malformed,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
struct Size {
    x: u32,
    y: u32,
    z: u32,
}

impl FromStr for Size {
    type Err = SizeParseError;

    fn from_str(text: &str) -> Result<Self, Self::Err> {
        let mut sp = text.split('x');
        let x = sp
            .next()
            .ok_or_else(|| SizeParseError::Malformed)?
            .parse()?;
        let y = sp
            .next()
            .ok_or_else(|| SizeParseError::Malformed)?
            .parse()?;
        let z = sp
            .next()
            .ok_or_else(|| SizeParseError::Malformed)?
            .parse()?;
        Ok(Size { x, y, z })
    }
}

#[derive(Debug, Error)]
enum OffsetParseError {
    #[error("int parse error: {0:}")]
    ParseIntError(#[from] ParseIntError),
    #[error("malformed input")]
    Malformed,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
struct Offset {
    x: i32,
    y: i32,
    z: i32,
}

impl FromStr for Offset {
    type Err = OffsetParseError;

    fn from_str(text: &str) -> Result<Self, Self::Err> {
        let mut sp = text.split('x');
        let x = sp
            .next()
            .ok_or_else(|| OffsetParseError::Malformed)?
            .parse()?;
        let y = sp
            .next()
            .ok_or_else(|| OffsetParseError::Malformed)?
            .parse()?;
        let z = sp
            .next()
            .ok_or_else(|| OffsetParseError::Malformed)?
            .parse()?;
        Ok(Offset { x, y, z })
    }
}

#[derive(Debug, StructOpt)]
struct Opt {
    /// Path to the input image
    #[structopt(long = "image", short = "i")]
    image_path: PathBuf,
    /// Path to the output ttb
    #[structopt(long = "ttb", short = "t")]
    ttb_path: PathBuf,
    /// The build name of the output ttb.
    #[structopt(long = "name", short = "n", default_value = "img2ttb generated build")]
    ttb_name: String,
    /// The build description of the output ttb.
    #[structopt(long = "description", short = "d", default_value = "")]
    ttb_description: String,
    /// The size of the output ttb, in voxels, in a WxHxD format. One of the dimensions must be
    /// zero.
    #[structopt(long = "size", short = "s", default_value = "128x0x128")]
    size: Size,
    /// The offset to apply after placing a brick, in voxel, in XxYxZ format. One of the
    /// dimensions must be zero.
    #[structopt(long = "offset", short = "o", default_value = "1x0x1")]
    offset: Offset,
    /// The brick type to use
    #[structopt(long = "brick", short = "b", default_value = "1x1 Plate")]
    brick: String,
}

fn get_palette(img: &RgbaImage) -> Vec<RgbaU8> {
    let mut colors = BTreeMap::new();
    for pixel in img.pixels() {
        *colors
            .entry(RgbaU8::from([pixel[0], pixel[1], pixel[2], pixel[3]]))
            .or_insert(0) += 1;
    }
    let mut colors_vec: Vec<_> = colors.into_iter().collect();
    colors_vec.sort_by_key(|&(_, freq)| usize::MAX - freq);
    colors_vec
        .into_iter()
        .map(|(color, _)| color)
        .take(255)
        .collect()
}

fn color_diff(color_a: RgbaU8, color_b: RgbaU8) -> i32 {
    fn sqdiff(a: u8, b: u8) -> i32 {
        let diff = a as i32 - b as i32;
        diff * diff
    }
    sqdiff(color_a.red, color_b.red)
        + sqdiff(color_a.green, color_b.green)
        + sqdiff(color_a.blue, color_b.blue)
        + sqdiff(color_a.alpha, color_b.alpha)
}

fn get_closest_color_id(palette: &[RgbaU8], color: RgbaU8) -> u8 {
    let mut min_index = 0;
    let mut min_error = i32::MAX;
    for (i, &palette_color) in palette.iter().enumerate() {
        let error = color_diff(color, palette_color);
        if error < min_error {
            min_error = error;
            min_index = i;
        }
    }
    min_index as u8
}

fn main() -> anyhow::Result<()> {
    let opt = Opt::from_args();
    let img_raw = image::open(opt.image_path)?;
    let img = img_raw.to_rgba8();
    let (img_width, img_height) = img.dimensions();
    let palette = get_palette(&img);
    let file = File::create(opt.ttb_path)?;
    let mut writer = ttb::Writer::new(file);
    writer.write_header(&opt.ttb_name, &opt.ttb_description, &palette)?;
    let (size_a, size_b, offset_a, offset_b) = if opt.size.x == 0 {
        let offset_a = Offset {
            x: 0,
            y: 0,
            z: opt.offset.z,
        };
        let offset_b = Offset {
            x: 0,
            y: opt.offset.y,
            z: 0,
        };
        (opt.size.y, opt.size.z, offset_a, offset_b)
    } else if opt.size.y == 0 {
        let offset_a = Offset {
            x: opt.offset.x,
            y: 0,
            z: 0,
        };
        let offset_b = Offset {
            x: 0,
            y: 0,
            z: opt.offset.z,
        };
        (opt.size.x, opt.size.z, offset_a, offset_b)
    } else if opt.size.z == 0 {
        let offset_a = Offset {
            x: opt.offset.x,
            y: 0,
            z: 0,
        };
        let offset_b = Offset {
            x: 0,
            y: opt.offset.y,
            z: 0,
        };
        (opt.size.y, opt.size.x, offset_a, offset_b)
    } else {
        panic!("one of the output size components must be 0");
    };
    let mut cur_x = 0;
    let mut cur_y = 0;
    let mut cur_z = 0;
    for i in 0..size_a {
        cur_x += offset_a.x;
        cur_y += offset_a.y;
        cur_z += offset_a.z;
        let start = (cur_x, cur_y, cur_z);
        for j in 0..size_b {
            cur_x += offset_b.x;
            cur_y += offset_b.y;
            cur_z += offset_b.z;
            let img_x = i * img_width / size_a;
            let img_y = (size_b - j - 1) * img_height / size_b;
            let pixel = img.get_pixel(img_x, img_y);
            let pixel_color = RgbaU8::from([pixel[0], pixel[1], pixel[2], pixel[3]]);
            let color_id = get_closest_color_id(&palette, pixel_color);
            writer.write_brick(&opt.brick, cur_x, cur_y, cur_z, 0, color_id)?;
        }
        cur_x = start.0;
        cur_y = start.1;
        cur_z = start.2;
    }
    writer.finish()?;
    Ok(())
}
