# img2ttb

## What's this?

A tool that can convert images to tinytown builds.

## How do I use it?

Assuming you have a very cute image called `dee.png`, you can turn this into a ttb called `dee.ttb` using:

```
$ img2ttb -i dee.png -t dee.ttb
```

You can run this for more options:

```
$ img2ttb --help
```

## What license is it under?

AGPLv3 or later
