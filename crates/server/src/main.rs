use tinytown::asset::AssetManager;

use {
    std::{fs, path::PathBuf},
    structopt::StructOpt,
    tinytown::{
        backend::NullBackend,
        game::{Game, GameParams},
    },
};

#[derive(StructOpt)]
struct Opt {
    /// Load config.
    #[structopt(short = "c", long = "config", default_value = "game.toml")]
    config_path: PathBuf,

    /// Path to load the resources from.
    #[structopt(short = "r", long = "resources", default_value = "resources")]
    resources_path: PathBuf,

    /// Automatically load a build in .ttb format
    #[structopt(short = "l", long = "autoload")]
    autoload_path: Option<PathBuf>,

    /// Automatically save to a path.
    #[structopt(long = "autosave")]
    autosave_path: Option<PathBuf>,

    /// Address to listen on. Overrides the `listen` config option.
    #[structopt(long = "listen")]
    listen: Option<String>,
}

fn main() -> anyhow::Result<()> {
    pretty_env_logger::init();
    let opt = Opt::from_args();
    let config: tinytown::config::Config = {
        let data = fs::read_to_string(opt.config_path)?;
        toml::from_str(&data)?
    };
    let asset_manager = AssetManager::for_platform();
    let params = GameParams {
        resources_path: opt.resources_path,
        listen_addr: opt.listen.clone().or(config.server.listen.clone()),
        connect_addr: None,
        autosave_path: opt.autosave_path,
        autoload_path: opt.autoload_path,
        my_name: None,
    };
    let mut game = Game::new(
        NullBackend::new(&params.resources_path),
        asset_manager,
        config,
        params,
    );
    game.init()?;
    game.start()?;
    Ok(())
}
