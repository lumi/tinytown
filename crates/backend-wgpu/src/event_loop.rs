use winit::{event::Event, event_loop::EventLoop};

pub struct WinitEventLoop {
    event_loop: EventLoop<()>,
}

impl WinitEventLoop {
    pub fn from_winit(event_loop: EventLoop<()>) -> WinitEventLoop {
        WinitEventLoop { event_loop }
    }
}

impl tinytown::backend::EventLoop for WinitEventLoop {
    type Event = Event<'static, ()>;

    fn run(self, mut f: impl FnMut(Event<'static, ()>) -> anyhow::Result<bool> + 'static) -> ! {
        self.event_loop
            .run(move |event, _window_target, control_flow| {
                control_flow.set_poll();
                if let Some(static_event) = event.to_static() {
                    match f(static_event) {
                        Ok(true) => (),
                        Ok(false) => {
                            control_flow.set_exit();
                        }
                        Err(err) => {
                            // TODO: better reporting
                            Err::<(), _>(err).unwrap();
                            control_flow.set_exit();
                        }
                    }
                }
            })
    }
}
