struct VertexInput {
    @location(0) position: vec3<f32>,
    @location(1) color: vec4<f32>,
    @location(2) normal: vec3<f32>,
    @location(3) tex_uv: vec2<f32>,
    @location(4) tex_size: vec2<f32>,
    @location(5) flags: u32,
}

struct InstanceInput {
    @location(6) model0: vec4<f32>,
    @location(7) model1: vec4<f32>,
    @location(8) model2: vec4<f32>,
    @location(9) model3: vec4<f32>,
    @location(10) color: vec4<f32>,
    @location(11) flags: u32,
}

struct CameraUniform {
    view: mat4x4<f32>,
    projection: mat4x4<f32>,
}

struct LightingUniform {
    sun_dir: vec3<f32>,
}

@group(0) @binding(0) var<uniform> camera: CameraUniform;

@group(1) @binding(0) var<uniform> lighting: LightingUniform;

@group(2) @binding(0) var tex_top: texture_2d<f32>;
@group(2) @binding(1) var tex_top_sampler: sampler;
@group(2) @binding(2) var tex_side: texture_2d<f32>;
@group(2) @binding(3) var tex_side_sampler: sampler;
@group(2) @binding(4) var tex_bottom: texture_2d<f32>;
@group(2) @binding(5) var tex_bottom_sampler: sampler;

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) color: vec4<f32>,
    @location(1) normal: vec3<f32>,
    @location(2) tex_uv: vec2<f32>,
    @location(3) tex_size: vec2<f32>,
    @location(4) default_color: vec4<f32>,
    @location(5) flags: u32,
}

@vertex
fn vs_main(in: VertexInput, instance: InstanceInput) -> VertexOutput {
    var out: VertexOutput;
    let model = mat4x4<f32>(
        instance.model0,
        instance.model1,
        instance.model2,
        instance.model3,
    );
    var position = in.position;
    if (instance.flags & 1u) != 0u {
        position += in.normal * 0.02;
    }
    out.clip_position = camera.projection * camera.view * model * vec4<f32>(position, 1.0);
    out.color = in.color;
    out.default_color = instance.color;
    out.normal = in.normal;
    out.tex_uv = in.tex_uv;
    out.tex_size = in.tex_size;
    out.flags = in.flags;
    return out;
}

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
    let brightness = 0.2 + ((1. - dot(lighting.sun_dir, in.normal)) / 2.) * 0.8;

    var selected_uv: vec2<f32>;

    if (in.flags & 1u) == 0u {
        selected_uv = fract(in.tex_uv);
    }
    else {
        var tex_coord = vec2(0.5, 0.5);
        var modified = false;
        if in.tex_uv.x < 0.5 {
            tex_coord.x = in.tex_uv.x;
            modified = true;
        }
        if in.tex_uv.y < 0.5 {
            tex_coord.y = in.tex_uv.y;
            modified = true;
        }
        if in.tex_uv.x > in.tex_size.x - 0.5 {
            tex_coord.x = in.tex_uv.x - in.tex_size.x + 1.;
            modified = true;
        }
        if in.tex_uv.y > in.tex_size.y - 0.5 {
            tex_coord.y = in.tex_uv.y - in.tex_size.y + 1.;
            modified = true;
        }
        if modified {
            selected_uv = tex_coord;
        }
        else {
            selected_uv = vec2<f32>(0.5, 0.5);
        }
    }

    let tex_sample = textureSample(tex_top, tex_top_sampler, selected_uv);

    let color = in.default_color.xyz * in.color.xyz * tex_sample.xyz;

    return vec4<f32>(brightness * color, clamp(in.default_color.w * in.color.w, 0.2, 1.));
}
