use egui::Color32;
use specs::ReadStorage;
use tinytown::{
    brick::{BrickDataStore, BrickPalette},
    camera::CameraMode,
    components::Player,
    resources::Stats,
};

pub fn create_ui(ctx: &egui::Context, world: &specs::World) -> anyhow::Result<()> {
    let stats = world.fetch::<Stats>();
    let camera_mode = world.fetch::<CameraMode>();

    egui::Window::new("Timing").show(ctx, |ui| {
        egui::plot::Plot::new("stats_plot")
            .view_aspect(2.)
            .show(ui, |plot_ui| {
                plot_ui.line(
                    egui::plot::Line::new(
                        stats
                            .broad_phase_collision_time_ns
                            .iter()
                            .enumerate()
                            .map(|(i, &v)| [i as f64, v as f64 / 1000.])
                            .collect::<Vec<_>>(),
                    )
                    .color(Color32::from_rgb(255, 0, 0)),
                );

                plot_ui.line(
                    egui::plot::Line::new(
                        stats
                            .narrow_phase_collision_time_ns
                            .iter()
                            .enumerate()
                            .map(|(i, &v)| [i as f64, v as f64 / 1000.])
                            .collect::<Vec<_>>(),
                    )
                    .color(Color32::from_rgb(0, 255, 0)),
                );

                plot_ui.line(
                    egui::plot::Line::new(
                        stats
                            .render_time_ns
                            .iter()
                            .enumerate()
                            .map(|(i, &v)| [i as f64, v as f64 / 1000.])
                            .collect::<Vec<_>>(),
                    )
                    .color(Color32::from_rgb(0, 0, 255)),
                );
            });
    });

    let (players,): (ReadStorage<Player>,) = world.system_data();
    let bds = world.fetch::<BrickDataStore>();
    let palette = world.fetch::<BrickPalette>();

    let player = if let Some(player_entity) = camera_mode.entity() {
        players.get(player_entity)
    } else {
        None
    };

    egui::Window::new("Player")
        .title_bar(false)
        .show(ctx, |ui| {
            ui.set_max_width(320.);

            if let Some(player) = player {
                ui.label(&format!("Tool: {:?}", player.tool));

                let brick_type = &bds[player.selected_data_id];

                ui.label(&format!("Brick: {}", brick_type.name));

                ui.horizontal_wrapped(|ui| {
                    for (id, color) in palette.iter() {
                        let stroke = if id == player.selected_color_id {
                            (1., egui::Rgba::WHITE)
                        } else {
                            (1., to_egui_color(color))
                        };

                        ui.add(
                            egui::Button::new("")
                                .min_size(egui::Vec2::new(20., 20.))
                                .fill(to_egui_color(color))
                                .stroke(stroke),
                        );
                    }
                });
            }
        });

    Ok(())
}

fn to_egui_color(color: regenboog::RgbaU8) -> egui::Rgba {
    egui::Rgba::from_rgba_unmultiplied(
        (color.red as f32) / 255.,
        (color.green as f32) / 255.,
        (color.blue as f32) / 255.,
        (color.alpha as f32) / 255.,
    )
}
