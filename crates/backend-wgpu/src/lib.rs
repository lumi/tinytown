use cgmath::Vector2;
use std::path::Path;
use tinytown::{
    controls::{Action, ControlsManager, MouseButton},
    resources::{Cursor, CursorMode, WindowSize},
    texture_manager::TextureManagerProxy,
};
use winit::{
    dpi::PhysicalSize,
    event::{DeviceEvent, ElementState, Event, MouseButton as WinitMouseButton, WindowEvent},
    event_loop::EventLoop,
    window::{CursorGrabMode, Window, WindowBuilder},
};

mod brick;
mod data;
mod event_loop;
mod input;
mod renderer;
mod texture;
mod ui;
mod util;

use event_loop::WinitEventLoop;
use renderer::WgpuRenderer;

pub struct EguiUi {
    pub platform: egui_winit_platform::Platform,
}

impl EguiUi {
    pub fn new(scale_factor: f64) -> EguiUi {
        let platform =
            egui_winit_platform::Platform::new(egui_winit_platform::PlatformDescriptor {
                physical_width: 1366,
                physical_height: 768,
                scale_factor,
                font_definitions: egui::FontDefinitions::default(),
                style: Default::default(),
            });

        EguiUi { platform }
    }
}

pub struct WgpuBackend {
    window: Window,
    event_loop: Option<EventLoop<()>>,
    renderer: WgpuRenderer,
    ui: EguiUi,
}

impl WgpuBackend {
    pub async fn new(
        resources_path: impl AsRef<Path>,
        window_size: (u32, u32),
        title: &str,
    ) -> anyhow::Result<WgpuBackend> {
        let event_loop = EventLoop::new();

        let window = WindowBuilder::new()
            .with_title(title)
            .with_inner_size(PhysicalSize::new(window_size.0, window_size.1))
            .build(&event_loop)?;

        let renderer = WgpuRenderer::from_winit(&window, window_size, resources_path).await?;

        let ui = EguiUi::new(window.scale_factor());

        Ok(WgpuBackend {
            event_loop: Some(event_loop),
            window,
            renderer,
            ui,
        })
    }
}

impl tinytown::backend::BackendName for WgpuBackend {
    fn name() -> &'static str {
        "wgpu"
    }
}

impl tinytown::backend::Backend for WgpuBackend {
    type EventLoop = WinitEventLoop;
    type Event = Event<'static, ()>;

    fn create_event_loop(&mut self) -> anyhow::Result<WinitEventLoop> {
        Ok(WinitEventLoop::from_winit(
            self.event_loop
                .take()
                .ok_or_else(|| anyhow::anyhow!("can only take the event loop once"))?,
        ))
    }

    fn size(&self) -> (u32, u32) {
        self.window.inner_size().into()
    }

    fn aspect_ratio(&self) -> f32 {
        let size = self.window.inner_size();
        size.width as f32 / size.height as f32
    }

    fn texture_manager_proxy(&self) -> TextureManagerProxy {
        self.renderer.texture_manager_proxy()
    }

    fn handle_event(&mut self, world: &specs::World, event: Event<()>) -> anyhow::Result<()> {
        self.inner_handle_event(world, event)
    }
}

impl WgpuBackend {
    fn inner_handle_event(&mut self, world: &specs::World, event: Event<()>) -> anyhow::Result<()> {
        let mut refresh_framebuffer: Option<(u32, u32)> = None;
        self.ui.platform.handle_event(&event);
        match event {
            Event::WindowEvent {
                event: win_event, ..
            } => match win_event {
                WindowEvent::CloseRequested | WindowEvent::Destroyed => {
                    let mut controls = world.fetch_mut::<ControlsManager>();
                    controls.request_exit();
                }
                WindowEvent::Focused(is_focused) if is_focused => {
                    let cursor = world.fetch::<Cursor>();

                    if cursor.mode == CursorMode::Grabbed {
                        self.window
                            .set_cursor_grab(CursorGrabMode::Locked)
                            .or_else(|_| self.window.set_cursor_grab(CursorGrabMode::Confined))?;

                        self.window.set_cursor_visible(false);
                    }
                }
                WindowEvent::Resized(position) => {
                    refresh_framebuffer = Some(position.into());
                }
                WindowEvent::CursorEntered { .. } => {
                    let mut controls = world.fetch_mut::<ControlsManager>();
                    controls.handle_cursor_enter(true);
                }
                WindowEvent::CursorLeft { .. } => {
                    let mut controls = world.fetch_mut::<ControlsManager>();
                    controls.handle_cursor_enter(false);
                }
                WindowEvent::MouseInput { button, state, .. } => {
                    let action = match state {
                        ElementState::Pressed => Action::Press,
                        ElementState::Released => Action::Release,
                    };
                    let mouse_button = match button {
                        WinitMouseButton::Left => Some(MouseButton::Left),
                        WinitMouseButton::Right => Some(MouseButton::Right),
                        WinitMouseButton::Middle => Some(MouseButton::Middle),
                        _ => None,
                    };
                    if let Some(btn) = mouse_button {
                        let mut controls = world.fetch_mut::<ControlsManager>();
                        controls.handle_mouse_event(action, btn);
                    }
                }
                WindowEvent::CursorMoved { position, .. } => {
                    let mut controls = world.fetch_mut::<ControlsManager>();
                    controls.handle_cursor_pos(Vector2::new(position.x as f32, position.y as f32));
                }
                WindowEvent::KeyboardInput { input, .. } => {
                    let action = match input.state {
                        ElementState::Pressed => Action::Press,
                        ElementState::Released => Action::Release,
                    };
                    if let Some(key) = input::scancode_convert(input.scancode) {
                        let mut controls = world.fetch_mut::<ControlsManager>();
                        controls.handle_key_event(action, key);
                    }
                }
                _ => {}
            },
            Event::DeviceEvent { event, .. } => match event {
                DeviceEvent::MouseMotion { delta } => {
                    let mut controls = world.fetch_mut::<ControlsManager>();
                    controls.handle_cursor_delta(Vector2::new(delta.0 as f32, delta.1 as f32));
                }
                _ => {}
            },
            Event::MainEventsCleared => {
                self.window.request_redraw();
            }
            Event::RedrawRequested(window_id) => {
                if window_id == self.window.id() {
                    self.renderer.render(world, &mut self.ui)?;
                }
            }
            _ => {}
        }
        if let Some((width, height)) = refresh_framebuffer {
            let mut ws = world.fetch_mut::<WindowSize>();
            ws.width = width as u32;
            ws.height = height as u32;
            self.renderer.resize((width, height))?;
        }
        Ok(())
    }
}
