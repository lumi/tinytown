use cgmath::{EuclideanSpace, Matrix4, SquareMatrix};
use specs::{Join, WorldExt};
use tinytown::{
    backend::generate_brick_mesh,
    brick::{BrickDataStore, BrickPalette},
    components::GhostBrick,
    mesh::Mesh,
    util::Directional,
};
use wgpu::{util::DeviceExt, Buffer};

use crate::{
    data::{BrickInstance, BrickMeshMeta, BrickVertex, DrawIndexedIndirect, WgpuMesh},
    renderer::upload_mesh,
};

const FLAGS_GHOST: u32 = 1;

pub struct FullBrickMesh {
    opaque_mesh: Option<WgpuMesh>,
    transparent_mesh: Option<WgpuMesh>,
    instance: Buffer,
}

impl FullBrickMesh {
    pub fn new(device: &wgpu::Device) -> FullBrickMesh {
        let instance = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("brick vertex buffer"),
            contents: bytemuck::cast_slice(&[BrickInstance {
                model: Matrix4::identity().into(),
                color: [1.; 4],
                flags: 0,
            }]),
            usage: wgpu::BufferUsages::VERTEX,
        });
        FullBrickMesh {
            opaque_mesh: None,
            transparent_mesh: None,
            instance,
        }
    }

    pub fn refresh(&mut self, device: &wgpu::Device, world: &specs::World) {
        fn make_vertex(
            position: [f32; 3],
            color: [f32; 4],
            normal: [f32; 3],
            tex_size: [f32; 2],
            tex_uv: [f32; 2],
            flags: u32,
        ) -> BrickVertex {
            BrickVertex {
                position,
                color,
                normal,
                tex_size,
                tex_uv,
                flags,
            }
        }

        let mut make_mesh = |vertices: Vec<BrickVertex>, indices: Vec<u32>| {
            upload_mesh(device, &Mesh::new(vertices, indices))
        };

        let brick_mesh_opaque = generate_brick_mesh(false, world, make_vertex, &mut make_mesh);
        let brick_mesh_transparent = generate_brick_mesh(true, world, make_vertex, &mut make_mesh);

        self.opaque_mesh = Some(brick_mesh_opaque);
        self.transparent_mesh = Some(brick_mesh_transparent);
    }

    // TODO: maybe resolve the weird lifetime requirements
    pub fn render<'r, 's: 'r>(&'s self, render_pass: &mut wgpu::RenderPass<'r>) {
        if let Some(mesh) = &self.opaque_mesh {
            render_pass.set_vertex_buffer(0, mesh.vertices.slice(..));
            render_pass.set_vertex_buffer(1, self.instance.slice(..));
            render_pass.set_index_buffer(mesh.indices.slice(..), wgpu::IndexFormat::Uint32);
            render_pass.draw_indexed(0..mesh.vertex_count, 0, 0..1);
        }
        if let Some(mesh) = &self.transparent_mesh {
            render_pass.set_vertex_buffer(0, mesh.vertices.slice(..));
            render_pass.set_vertex_buffer(1, self.instance.slice(..));
            render_pass.set_index_buffer(mesh.indices.slice(..), wgpu::IndexFormat::Uint32);
            render_pass.draw_indexed(0..mesh.vertex_count, 0, 0..1);
        }
    }
}

pub struct BrickMeshes {
    meshes: Option<WgpuMesh>,
    meta: Vec<BrickMeshMeta>,
    instances: Option<wgpu::Buffer>,
    indirect: Option<wgpu::Buffer>,
    count: u32,
}

impl BrickMeshes {
    pub fn new() -> BrickMeshes {
        BrickMeshes {
            meshes: None,
            meta: Vec::new(),
            instances: None,
            indirect: None,
            count: 0,
        }
    }

    pub fn sync(&mut self, device: &wgpu::Device, world: &specs::World) {
        let brick_data_store = world.fetch::<BrickDataStore>();
        if self.meta.len() < brick_data_store.len() {
            log::info!("brick meshes not up to date, generating new mesh");
            self.meta.clear();
            let mut vertex_offset = 0;
            let mut index_offset = 0;
            let mut vertices = Vec::new();
            let mut indices = Vec::new();
            for (brick_data_id, brick_data) in brick_data_store.iter() {
                log::info!(
                    "creating mesh for brick id {}, brick name {:?}",
                    brick_data_id.0,
                    brick_data.name
                );
                let mesh = brick_data.create_mesh(
                    Directional::all(false),
                    |position, color, normal, tex_size, tex_uv, flags| BrickVertex {
                        position: position.into(),
                        color: color.into(),
                        normal: normal.into(),
                        tex_size: tex_size.into(),
                        tex_uv: tex_uv.into(),
                        flags,
                    },
                );
                let vertex_count = mesh.vertices.len() as u32;
                let index_count = mesh.indices.len() as u32;
                self.meta.push(BrickMeshMeta {
                    vertex_offset,
                    index_offset,
                    count: index_count,
                });
                vertices.extend_from_slice(&mesh.vertices);
                indices.extend_from_slice(&mesh.indices);
                vertex_offset += vertex_count;
                index_offset += index_count;
            }
            self.meshes = Some(upload_mesh(&device, &Mesh::new(vertices, indices)));
        }
    }

    pub fn prepare_render(&mut self, device: &wgpu::Device, world: &specs::World) {
        let bp = world.fetch::<BrickPalette>();

        let mut instances = Vec::new();
        let mut indirects = Vec::new();

        let mut count = 0;

        for (i, gb) in world.read_storage::<GhostBrick>().join().enumerate() {
            let meta = &self.meta[gb.data_id.0];
            let rot: Matrix4<f32> = gb.visual_rot.into();
            let model = Matrix4::from_translation(gb.visual_pos.to_vec()) * rot;
            let color = bp[gb.color_id];
            instances.push(BrickInstance {
                model: model.into(),
                color: color.into(),
                flags: FLAGS_GHOST,
            });
            indirects.push(DrawIndexedIndirect {
                vertex_count: meta.count,
                instance_count: 1,
                base_index: meta.index_offset,
                vertex_offset: meta.vertex_offset as i32,
                base_instance: i as u32,
            });
            count += 1;
        }

        let brick_instances = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("ghost brick instances"),
            usage: wgpu::BufferUsages::VERTEX,
            contents: bytemuck::cast_slice(&instances),
        });

        let brick_indirect = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("ghost brick indirect"),
            usage: wgpu::BufferUsages::INDIRECT,
            contents: bytemuck::cast_slice(&indirects),
        });

        self.instances = Some(brick_instances);
        self.indirect = Some(brick_indirect);
        self.count = count;
    }

    pub fn render<'r, 's: 'r>(&'s self, render_pass: &mut wgpu::RenderPass<'r>) {
        if let (Some(mesh), Some(instances), Some(indirect)) =
            (&self.meshes, &self.instances, &self.indirect)
        {
            if self.count > 0 {
                render_pass.set_vertex_buffer(0, mesh.vertices.slice(..));
                render_pass.set_vertex_buffer(1, instances.slice(..));
                render_pass.set_index_buffer(mesh.indices.slice(..), wgpu::IndexFormat::Uint32);
                render_pass.multi_draw_indexed_indirect(indirect, 0, self.count);
            }
        }
    }
}
