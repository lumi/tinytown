use std::path::Path;

use image::GenericImageView;
use tinytown::texture_manager::TextureLoader;
use wgpu::{Device, Queue};

pub struct WgpuTextureLoader<'a> {
    device: &'a wgpu::Device,
    queue: &'a wgpu::Queue,
}

impl<'a> WgpuTextureLoader<'a> {
    pub fn new(device: &'a wgpu::Device, queue: &'a wgpu::Queue) -> WgpuTextureLoader<'a> {
        WgpuTextureLoader { device, queue }
    }
}

impl<'a> TextureLoader for WgpuTextureLoader<'a> {
    type Texture = Texture;

    fn load_texture(&mut self, path: impl AsRef<Path>) -> anyhow::Result<Self::Texture> {
        let path = path.as_ref();
        log::info!("loading texture {}", path.display());
        let img = image::open(path)?;
        let path_text = path.display().to_string();
        Texture::from_image(self.device, self.queue, &img, Some(&path_text))
    }
}

pub struct Texture {
    pub inner: wgpu::Texture,
    pub view: wgpu::TextureView,
    pub sampler: wgpu::Sampler,
}

impl Texture {
    pub fn from_parts(
        inner: wgpu::Texture,
        view: wgpu::TextureView,
        sampler: wgpu::Sampler,
    ) -> Texture {
        Texture {
            inner,
            view,
            sampler,
        }
    }

    pub fn from_image(
        device: &Device,
        queue: &Queue,
        img: &image::DynamicImage,
        label: Option<&str>,
    ) -> anyhow::Result<Texture> {
        let rgba = img.to_rgba8();

        let dim = img.dimensions();

        let size = wgpu::Extent3d {
            width: dim.0,
            height: dim.1,
            depth_or_array_layers: 1,
        };

        let inner = device.create_texture(&wgpu::TextureDescriptor {
            label,
            size,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Rgba8UnormSrgb,
            usage: wgpu::TextureUsages::TEXTURE_BINDING | wgpu::TextureUsages::COPY_DST,
            view_formats: &[],
        });

        queue.write_texture(
            wgpu::ImageCopyTexture {
                aspect: wgpu::TextureAspect::All,
                texture: &inner,
                mip_level: 0,
                origin: wgpu::Origin3d::ZERO,
            },
            &rgba,
            wgpu::ImageDataLayout {
                offset: 0,
                bytes_per_row: Some(4 * dim.0),
                rows_per_image: Some(dim.1),
            },
            size,
        );

        let view = inner.create_view(&wgpu::TextureViewDescriptor::default());

        let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::Repeat,
            address_mode_v: wgpu::AddressMode::Repeat,
            address_mode_w: wgpu::AddressMode::Repeat,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            ..Default::default()
        });

        Ok(Texture::from_parts(inner, view, sampler))
    }

    pub fn depth_buffer(
        config: &wgpu::SurfaceConfiguration,
        device: &Device,
        label: Option<&str>,
    ) -> Texture {
        let size = wgpu::Extent3d {
            width: config.width,
            height: config.height,
            depth_or_array_layers: 1,
        };

        let inner = device.create_texture(&wgpu::TextureDescriptor {
            label,
            size,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Depth32Float,
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT | wgpu::TextureUsages::TEXTURE_BINDING,
            view_formats: &[],
        });

        let view = inner.create_view(&wgpu::TextureViewDescriptor::default());

        let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Linear,
            mipmap_filter: wgpu::FilterMode::Nearest,
            compare: Some(wgpu::CompareFunction::LessEqual),
            lod_min_clamp: 0.,
            lod_max_clamp: 100.,
            ..Default::default()
        });

        Texture {
            inner,
            view,
            sampler,
        }
    }
}
