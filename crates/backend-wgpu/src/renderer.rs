use cgmath::{Matrix4, SquareMatrix, Vector3};
use std::{path::Path, time::Instant};
use tinytown::{
    backend::BrickTextures,
    brick::{BrickData, BrickEventQueue},
    camera::Camera,
    mesh::Mesh,
    resources::{Stats, Sun},
    texture_manager::{TextureManager, TextureManagerProxy},
    util::Directional,
    vox::VOXEL_SIZE,
};
use wgpu::{
    util::DeviceExt, BindGroup, Buffer, Device, DeviceDescriptor, Instance, Queue, RenderPipeline,
    Surface, SurfaceConfiguration,
};

use crate::{
    brick::{BrickMeshes, FullBrickMesh},
    data::{BrickInstance, BrickVertex, CameraUniform, LightingUniform, WgpuMesh},
    texture, util, EguiUi,
};

pub fn upload_mesh<V: bytemuck::Pod + bytemuck::Zeroable>(
    device: &Device,
    mesh: &Mesh<V>,
) -> WgpuMesh {
    let vertices = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: Some("brick vertex buffer"),
        contents: bytemuck::cast_slice(&mesh.vertices),
        usage: wgpu::BufferUsages::VERTEX,
    });

    let indices = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: Some("brick index buffer"),
        contents: bytemuck::cast_slice(&mesh.indices),
        usage: wgpu::BufferUsages::INDEX,
    });

    WgpuMesh {
        vertices,
        indices,
        vertex_count: mesh.indices.len() as u32,
    }
}

pub struct WgpuRenderer {
    surface: Surface,
    config: SurfaceConfiguration,
    device: Device,
    queue: Queue,

    render_pipeline: RenderPipeline,

    depth_texture: texture::Texture,

    texture_manager: TextureManager<texture::Texture>,

    brick_meshes: BrickMeshes,
    full_brick_mesh: FullBrickMesh,

    camera_buffer: Buffer,
    camera_bind_group: BindGroup,

    lighting_buffer: Buffer,
    lighting_bind_group: BindGroup,

    brick_textures_bind_group: BindGroup,

    egui_render_pass: egui_wgpu_backend::RenderPass,
}

impl WgpuRenderer {
    pub async fn from_winit(
        window: &winit::window::Window,
        window_size: (u32, u32),
        resources_path: impl AsRef<Path>,
    ) -> anyhow::Result<WgpuRenderer> {
        let instance = Instance::new(wgpu::InstanceDescriptor {
            backends: wgpu::Backends::all(),
            ..Default::default()
        });

        let surface = unsafe { instance.create_surface(window) }?;

        let renderer = WgpuRenderer::new(instance, surface, window_size, resources_path).await?;

        Ok(renderer)
    }

    pub async fn new(
        instance: Instance,
        surface: Surface,
        window_size: (u32, u32),
        resources_path: impl AsRef<Path>,
    ) -> anyhow::Result<WgpuRenderer> {
        let mut texture_manager = TextureManager::new(resources_path);

        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::HighPerformance,
                force_fallback_adapter: false,
                compatible_surface: Some(&surface),
            })
            .await
            .ok_or_else(|| anyhow::anyhow!("could not get instance"))?;

        let (device, queue) = adapter
            .request_device(
                &DeviceDescriptor {
                    features: wgpu::Features::INDIRECT_FIRST_INSTANCE
                        | wgpu::Features::MULTI_DRAW_INDIRECT,
                    limits: wgpu::Limits::default(),
                    label: None,
                },
                None,
            )
            .await?;

        let config = SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: util::get_surface_format(&adapter, &surface),
            width: window_size.0,
            height: window_size.1,
            present_mode: wgpu::PresentMode::Fifo,
            alpha_mode: wgpu::CompositeAlphaMode::Opaque,
            view_formats: vec![],
        };

        surface.configure(&device, &config);

        let brick_textures = BrickTextures::new(&texture_manager.proxy());

        {
            let mut texture_loader = texture::WgpuTextureLoader::new(&device, &queue);
            texture_manager.sync(&mut texture_loader)?;
        }

        let shader = device.create_shader_module(wgpu::ShaderModuleDescriptor {
            label: Some("main shader"),
            source: wgpu::ShaderSource::Wgsl(
                // TODO: better path handling (maybe put in resources?)
                std::fs::read_to_string("crates/backend-wgpu/src/shader.wgsl")
                    .unwrap()
                    .into(),
            ),
        });

        let (camera_bind_group_layout, camera_bind_group, camera_buffer) =
            CameraUniform::create_bind_group(&device);

        let (lighting_bind_group_layout, lighting_bind_group, lighting_buffer) =
            LightingUniform::create_bind_group(&device);

        let (brick_textures_bind_group_layout, brick_textures_bind_group) =
            util::craete_brick_textures_bind_group(&device, &texture_manager, &brick_textures);

        let render_pipeline_layout =
            device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("main shader pipeline layout"),
                bind_group_layouts: &[
                    &camera_bind_group_layout,
                    &lighting_bind_group_layout,
                    &brick_textures_bind_group_layout,
                ],
                push_constant_ranges: &[],
            });

        let depth_texture = texture::Texture::depth_buffer(&config, &device, Some("depth buffer"));

        let render_pipeline =
            util::create_render_pipeline(&config, &device, &render_pipeline_layout, &shader);

        let full_brick_mesh = FullBrickMesh::new(&device);

        let brick_meshes = BrickMeshes::new();

        let egui_render_pass = egui_wgpu_backend::RenderPass::new(
            &device,
            util::get_surface_format(&adapter, &surface),
            1,
        );

        Ok(WgpuRenderer {
            texture_manager,
            surface,
            config,
            device,
            queue,
            render_pipeline,
            brick_meshes,
            full_brick_mesh,
            camera_buffer,
            camera_bind_group,
            lighting_buffer,
            lighting_bind_group,
            brick_textures_bind_group,
            depth_texture,
            egui_render_pass,
        })
    }

    pub fn texture_manager_proxy(&self) -> TextureManagerProxy {
        self.texture_manager.proxy()
    }

    fn create_render_pass(
        &self,
        encoder: &mut wgpu::CommandEncoder,
        view: &wgpu::TextureView,
        floor_mesh: &WgpuMesh,
        floor_mesh_instances: &Buffer,
    ) {
        let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: Some("render pass"),
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: &view,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color::BLACK),
                    store: true,
                },
            })],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                view: &self.depth_texture.view,
                depth_ops: Some(wgpu::Operations {
                    load: wgpu::LoadOp::Clear(1.0),
                    store: true,
                }),
                stencil_ops: None,
            }),
        });

        render_pass.set_pipeline(&self.render_pipeline);
        render_pass.set_bind_group(0, &self.camera_bind_group, &[]);
        render_pass.set_bind_group(1, &self.lighting_bind_group, &[]);
        render_pass.set_bind_group(2, &self.brick_textures_bind_group, &[]);

        render_pass.set_vertex_buffer(0, floor_mesh.vertices.slice(..));
        render_pass.set_vertex_buffer(1, floor_mesh_instances.slice(..));
        render_pass.set_index_buffer(floor_mesh.indices.slice(..), wgpu::IndexFormat::Uint32);
        render_pass.draw_indexed(0..floor_mesh.vertex_count, 0, 0..1);

        self.full_brick_mesh.render(&mut render_pass);
        self.brick_meshes.render(&mut render_pass);
    }

    pub fn resize(&mut self, new_size: (u32, u32)) -> anyhow::Result<()> {
        self.config.width = new_size.0;
        self.config.height = new_size.1;
        self.surface.configure(&self.device, &self.config);
        self.depth_texture =
            texture::Texture::depth_buffer(&self.config, &self.device, Some("depth buffer"));
        Ok(())
    }

    pub fn render(&mut self, world: &specs::World, ui: &mut EguiUi) -> anyhow::Result<()> {
        let start_render = Instant::now();
        {
            let mut texture_loader = texture::WgpuTextureLoader::new(&self.device, &self.queue);
            self.texture_manager.sync(&mut texture_loader)?;
        }

        self.brick_meshes.sync(&self.device, world);

        {
            let mut beq = world.fetch_mut::<BrickEventQueue>();
            if beq.dirty {
                self.full_brick_mesh.refresh(&self.device, world);
                beq.dirty = false;
            }
        }

        self.brick_meshes.prepare_render(&self.device, world);

        let floor_mesh = upload_mesh(
            &self.device,
            &BrickData {
                name: "".to_string(),
                size: Vector3::new(10000, 1, 10000),
                flat: false,
            }
            .create_mesh(
                Directional::all(false),
                |position, color, normal, tex_size, tex_uv, flags| BrickVertex {
                    position: (position - Vector3::new(0., VOXEL_SIZE.y * 0.5, 0.)).into(),
                    color: color.into(),
                    normal: normal.into(),
                    tex_size: tex_size.into(),
                    tex_uv: tex_uv.into(),
                    flags,
                },
            ),
        );

        let floor_mesh_instances =
            self.device
                .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                    label: Some("brick mesh opaque instances"),
                    usage: wgpu::BufferUsages::VERTEX,
                    contents: bytemuck::cast_slice(&[BrickInstance {
                        model: Matrix4::identity().into(),
                        color: [0.2, 0.3, 0.2, 1.],
                        flags: 0,
                    }]),
                });

        let camera = world.fetch::<Camera>();
        let camera_uniform = CameraUniform::from_camera(&camera);
        self.queue.write_buffer(
            &self.camera_buffer,
            0,
            bytemuck::cast_slice(&[camera_uniform]),
        );

        let sun = world.fetch::<Sun>();
        let lighting_uniform = LightingUniform::new(sun.sun_dir);
        self.queue.write_buffer(
            &self.lighting_buffer,
            0,
            bytemuck::cast_slice(&[lighting_uniform]),
        );

        let output = self.surface.get_current_texture()?;

        let view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("command encoder"),
                ..Default::default()
            });

        self.create_render_pass(&mut encoder, &view, &floor_mesh, &floor_mesh_instances);

        self.render_ui(world, &mut encoder, &view, ui)?;

        self.queue.submit([encoder.finish()]);

        output.present();

        world
            .fetch_mut::<Stats>()
            .render_time_ns
            .push(start_render.elapsed().subsec_nanos());

        Ok(())
    }

    fn render_ui(
        &mut self,
        world: &specs::World,
        encoder: &mut wgpu::CommandEncoder,
        view: &wgpu::TextureView,
        ui: &mut EguiUi,
    ) -> anyhow::Result<()> {
        ui.platform.begin_frame();

        crate::ui::create_ui(&ui.platform.context(), world)?;

        let full_output = ui.platform.end_frame(None);
        let paint_jobs = ui.platform.context().tessellate(full_output.shapes);

        let screen_descriptor = egui_wgpu_backend::ScreenDescriptor {
            physical_width: self.config.width,
            physical_height: self.config.height,
            // TODO: scale factor
            scale_factor: 1.,
        };

        self.egui_render_pass.add_textures(
            &self.device,
            &self.queue,
            &full_output.textures_delta,
        )?;

        self.egui_render_pass.update_buffers(
            &self.device,
            &self.queue,
            &paint_jobs,
            &screen_descriptor,
        );

        self.egui_render_pass
            .execute(encoder, &view, &paint_jobs, &screen_descriptor, None)?;

        Ok(())
    }
}
