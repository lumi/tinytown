use tinytown::asset::AssetManager;

use {
    std::{fs, net::SocketAddr, path::PathBuf},
    structopt::StructOpt,
    tinytown::{
        backend::{Backend, NullBackend},
        config::Config,
        game::{Game, GameParams},
    },
};

const DEFAULT_WINDOW_SIZE: (u32, u32) = (960, 960);
const WINDOW_TITLE: &'static str = "Tinytown";

#[derive(StructOpt)]
struct Opt {
    /// List backends.
    #[structopt(long = "list-backends")]
    list_backends: bool,

    /// Automatically load a build in .ttb format
    #[structopt(short = "l", long = "autoload")]
    autoload_path: Option<PathBuf>,

    /// Load config.
    #[structopt(short = "c", long = "config", default_value = "game.toml")]
    config_path: PathBuf,

    /// Path to load the resources from.
    #[structopt(short = "r", long = "resources", default_value = "resources")]
    resources_path: PathBuf,

    /// Select backend.
    #[structopt(short = "b", long = "backend")]
    backend: Option<String>,

    /// Choose a name.
    #[structopt(short = "n", long = "name")]
    name: Option<String>,

    /// Client mode.
    #[structopt(long = "connect")]
    connect: Option<SocketAddr>,

    /// Automatically save to a path.
    #[structopt(long = "autosave")]
    autosave_path: Option<PathBuf>,
}

static BACKENDS: &[(
    &'static str,
    fn(opt: Opt, config: Config) -> anyhow::Result<()>,
)] = &[
    #[cfg(feature = "backend-wgpu")]
    ("wgpu", start_wgpu),
    ("null", start_null),
];

#[cfg(feature = "backend-wgpu")]
fn start_wgpu(opt: Opt, config: Config) -> anyhow::Result<()> {
    cont(
        pollster::block_on(tinytown_backend_wgpu::WgpuBackend::new(
            &opt.resources_path,
            config.main.window_size.unwrap_or(DEFAULT_WINDOW_SIZE),
            WINDOW_TITLE,
        ))?,
        opt,
        config,
    )
}

fn start_null(opt: Opt, config: Config) -> anyhow::Result<()> {
    cont(NullBackend::new(&opt.resources_path), opt, config)
}

fn main() -> anyhow::Result<()> {
    pretty_env_logger::init();
    let opt = Opt::from_args();
    if opt.list_backends {
        println!("Available backends:");
        for (name, _) in BACKENDS {
            println!(" - {}", name);
        }
        return Ok(());
    }
    let config: Config = {
        let data = fs::read_to_string(&opt.config_path)?;
        toml::from_str(&data)?
    };
    let selected_backend = opt.backend.as_deref().unwrap_or_else(|| BACKENDS[0].0);
    if let Some((_, start)) = BACKENDS
        .into_iter()
        .find(|&(name, _)| name == &selected_backend)
    {
        start(opt, config)
    } else {
        panic!("invalid backend name {selected_backend:?}");
    }
}

fn cont<B: Backend + 'static>(backend: B, opt: Opt, config: Config) -> anyhow::Result<()> {
    let asset_manager = AssetManager::for_platform();
    let params = GameParams {
        resources_path: opt.resources_path,
        connect_addr: opt.connect,
        listen_addr: None,
        my_name: Some(opt.name.unwrap_or_else(|| "anon".to_owned())),
        autosave_path: opt.autosave_path,
        autoload_path: opt.autoload_path,
    };
    let mut game = Game::new(backend, asset_manager, config, params);
    game.init()?;
    game.start()?;
    Ok(())
}
