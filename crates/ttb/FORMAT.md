# Build format

## Notes

Everything is in big-endian.

The file extension is `.ttb`.

`u8_str` is a `u8` followed by that amount of utf8 bytes. `u16_str` is the same but with a `u16` length field.

## Overview

The format is composed of 3 sections:

 - Header
 - Palette
 - Blocks

## Header

```
magic_number: const b"ttb"
version_number: u8
build_name: u8_str
build_description: u16_str
palette_size: u8
```

## Palette color (repeated `palette_size` times)

```
r: u8
g: u8
b: u8
a: u8
```

## Blocks (repeated until EOF)

```
block_type: u8
block_size: u8
```

### Block type 1: Brick type

```
name: u8_str
```

### Block type 2: Brick data

```
brick type: u16 (from 0 to, but not including, the amount of brick types so far)
position: i32, i32, i32
orientation: u8 (0, 1, 2, 3)
color: u8 (from 0 to, but not including, palette_size)
```
