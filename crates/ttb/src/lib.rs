#![forbid(unsafe_code)]
#![forbid(missing_docs)]

//! This is a crate for saving and loading .ttb files.
//!
//! # Examples
//!
//! Basic usage:
//!
//! ```rust
//! use {
//!     std::{
//!         io::{
//!             Cursor,
//!         },
//!     },
//!     ttb::{
//!         Reader,
//!         ReaderEvent,
//!         Writer,
//!     },
//!     regenboog::{
//!         RgbaU8,
//!     },
//! };
//!
//! let mut buf = [0; 1024];
//!
//! let write_cursor = Cursor::new(&mut buf[..]);
//!
//! let mut writer = Writer::new(write_cursor);
//!
//! writer.write_header("Test", "This is a test. Meow.", &[
//!     RgbaU8::WHITE,
//!     RgbaU8::BLACK,
//!     RgbaU8::RED,
//!     RgbaU8::GREEN,
//!     RgbaU8::BLUE,
//! ]).unwrap();
//!
//! writer.write_brick("1x1 Brick", 0, 0, 0, 0, 0).unwrap();
//! writer.write_brick("1x1 Brick", 0, 3, 0, 0, 1).unwrap();
//! writer.write_brick("1x1 Brick", 0, 6, 0, 0, 2).unwrap();
//! writer.write_brick("1x1 Brick", 0, 9, 0, 0, 3).unwrap();
//! writer.write_brick("1x1 Plate", 0, 10, 0, 0, 4).unwrap();
//!
//! writer.finish().unwrap();
//!
//! let read_cursor = Cursor::new(&buf[..]);
//!
//! let mut reader = Reader::new(read_cursor);
//!
//! assert_eq!(reader.next_event().unwrap(), Some(ReaderEvent::Meta {
//!     build_name: "Test",
//!     build_description: "This is a test. Meow.",
//! }));
//!
//! assert_eq!(reader.next_event().unwrap(), Some(ReaderEvent::Color {
//!     id: 0,
//!     color: RgbaU8::WHITE,
//! }));
//! assert_eq!(reader.next_event().unwrap(), Some(ReaderEvent::Color {
//!     id: 1,
//!     color: RgbaU8::BLACK,
//! }));
//! assert_eq!(reader.next_event().unwrap(), Some(ReaderEvent::Color {
//!     id: 2,
//!     color: RgbaU8::RED,
//! }));
//! assert_eq!(reader.next_event().unwrap(), Some(ReaderEvent::Color {
//!     id: 3,
//!     color: RgbaU8::GREEN,
//! }));
//! assert_eq!(reader.next_event().unwrap(), Some(ReaderEvent::Color {
//!     id: 4,
//!     color: RgbaU8::BLUE,
//! }));
//!
//! assert_eq!(reader.next_event().unwrap(), Some(ReaderEvent::Brick {
//!     brick_type: "1x1 Brick",
//!     x: 0,
//!     y: 0,
//!     z: 0,
//!     orientation: 0,
//!     color_id: 0,
//! }));
//! assert_eq!(reader.next_event().unwrap(), Some(ReaderEvent::Brick {
//!     brick_type: "1x1 Brick",
//!     x: 0,
//!     y: 3,
//!     z: 0,
//!     orientation: 0,
//!     color_id: 1,
//! }));
//! assert_eq!(reader.next_event().unwrap(), Some(ReaderEvent::Brick {
//!     brick_type: "1x1 Brick",
//!     x: 0,
//!     y: 6,
//!     z: 0,
//!     orientation: 0,
//!     color_id: 2,
//! }));
//! assert_eq!(reader.next_event().unwrap(), Some(ReaderEvent::Brick {
//!     brick_type: "1x1 Brick",
//!     x: 0,
//!     y: 9,
//!     z: 0,
//!     orientation: 0,
//!     color_id: 3,
//! }));
//! assert_eq!(reader.next_event().unwrap(), Some(ReaderEvent::Brick {
//!     brick_type: "1x1 Plate",
//!     x: 0,
//!     y: 10,
//!     z: 0,
//!     orientation: 0,
//!     color_id: 4,
//! }));
//! ```

/// The magic number at the front of a .ttb file.
pub const MAGIC_NUMBER: &[u8] = b"ttb";

/// The current version of the TTB format this crate writes.
pub const VERSION: u8 = 1;

mod reader;
mod writer;

pub use {
    reader::{Error as ReaderError, Event as ReaderEvent, Reader},
    writer::Writer,
};
