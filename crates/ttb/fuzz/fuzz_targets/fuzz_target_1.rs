#![no_main]
use libfuzzer_sys::fuzz_target;

use {
    std::{
        io::{
            Cursor,
        },
    },
    ttb::{
        Reader,
    },
};

fuzz_target!(|data: &[u8]| {
    let cursor = Cursor::new(data);
    let mut reader = Reader::new(cursor);
    loop {
        match reader.next_event() {
            Ok(Some(_)) => (),
            Ok(None) => { break; },
            Err(_) => { break; },
        }
    }
});
