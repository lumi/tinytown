#![deny(missing_docs)]

//! Components used in tinytown

mod brick;
mod entity_kind;
mod entity_tick;
mod netid;
mod physical;
mod player;

pub use self::{
    brick::{Brick, GhostBrick},
    entity_kind::EntityKind,
    entity_tick::EntityTick,
    netid::NetId,
    physical::Physical,
    player::Player,
};
