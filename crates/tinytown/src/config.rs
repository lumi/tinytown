use {
    serde::{Deserialize, Serialize},
    std::{collections::HashMap, path::PathBuf},
};

use crate::controls::{Control, Key, MouseButton};

/// Mapping from keyboard keys and mouse buttons to controls.
#[derive(Clone, Debug, Serialize, Deserialize, Default)]
#[serde(rename_all = "kebab-case")]
pub struct ControlConfig {
    /// Mapping from keyboard keys to their controls
    pub keyboard: HashMap<Key, Control>,
    /// Mapping from mouse buttons to their controls
    pub mouse: HashMap<MouseButton, Control>,
}

/// The main section of the configuration.
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct ConfigMain {
    /// The default size of the window
    pub window_size: Option<(u32, u32)>,
    /// The palette to use.
    pub palette: String,
    /// Automatically save to this path.
    pub autosave_path: Option<PathBuf>,
    /// Automatically load this file on start.
    pub autoload_path: Option<PathBuf>,
}

/// The server section of the configuration.
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct ConfigServer {
    /// The address to listen on
    pub listen: Option<String>,
}

/// The client section of the configuration.
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct ConfigClient {
    /// The name of the player.
    pub player_name: String,
}

/// The tinytown configuration.
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Config {
    /// The main configuration section
    pub main: ConfigMain,
    /// The server configuration section
    pub server: ConfigServer,
    /// The client configuration section
    pub client: ConfigClient,
    /// The controls configuration
    #[serde(flatten)]
    pub control_config: ControlConfig,
}
