use std::{path::Path, process, thread, time::Duration};

use super::{Backend, BackendName, EventLoop};

use crate::texture_manager::{TextureManager, TextureManagerProxy};

pub struct NullBackend {
    texture_manager: TextureManager<()>,
}

impl NullBackend {
    pub fn new(resources_path: impl AsRef<Path>) -> NullBackend {
        NullBackend {
            texture_manager: TextureManager::new(resources_path),
        }
    }
}

impl BackendName for NullBackend {
    fn name() -> &'static str {
        "null"
    }
}

impl Backend for NullBackend {
    type EventLoop = NullEventLoop;
    type Event = ();

    fn create_event_loop(&mut self) -> anyhow::Result<NullEventLoop> {
        Ok(NullEventLoop)
    }

    fn aspect_ratio(&self) -> f32 {
        1.
    }

    fn size(&self) -> (u32, u32) {
        (1, 1)
    }

    fn texture_manager_proxy(&self) -> TextureManagerProxy {
        self.texture_manager.proxy()
    }

    fn handle_event(&mut self, _world: &specs::World, _event: ()) -> anyhow::Result<()> {
        Ok(())
    }
}

pub struct NullEventLoop;

impl EventLoop for NullEventLoop {
    type Event = ();

    fn run(self, mut f: impl FnMut(()) -> anyhow::Result<bool> + 'static) -> ! {
        loop {
            match f(()) {
                Ok(true) => {}
                Ok(false) => {
                    break;
                }
                Err(err) => {
                    // TODO: better erro reporting
                    Err::<(), _>(err).unwrap();
                    break;
                }
            }
            // TODO: maybe sleep for a better controlled time
            thread::sleep(Duration::from_millis(1));
        }
        process::exit(0);
    }
}
