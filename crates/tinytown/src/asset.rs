use std::{collections::HashMap, io::Cursor};

// TODO: represent assets that have not been loaded yet
#[derive(Debug, Clone)]
pub struct Asset {
    kind: AssetKind,
    name: String,
    data: Vec<u8>,
}

impl Asset {
    fn as_ref(&self) -> AssetRef {
        AssetRef {
            kind: self.kind,
            name: &self.name,
            data: &self.data,
        }
    }
}

// TODO: represent assets that have not been loaded yet
#[derive(Debug, Clone, Copy)]
pub struct AssetRef<'a> {
    kind: AssetKind,
    name: &'a str,
    data: &'a [u8],
}

impl<'a> AssetRef<'a> {
    pub fn kind(&self) -> AssetKind {
        self.kind
    }

    pub fn name(&self) -> &'a str {
        self.name
    }

    pub fn data(&self) -> &'a [u8] {
        self.data
    }

    pub fn cursor(&self) -> Cursor<&[u8]> {
        Cursor::new(self.data)
    }

    pub fn load_as_image(&self) -> anyhow::Result<image::DynamicImage> {
        let format = image::ImageFormat::from_path(self.name)?;
        let img = image::load_from_memory_with_format(self.data, format)?;
        Ok(img)
    }
}

pub trait AssetSource {
    fn list(&self, kind: AssetKind) -> anyhow::Result<Vec<String>>;
    fn load(&self, kind: AssetKind, name: &str) -> anyhow::Result<Asset>;
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum AssetKind {
    Texture,
    Brick,
    Palette,
}

mod native {
    use std::{
        fs,
        path::{Path, PathBuf},
    };

    use super::{Asset, AssetKind, AssetSource};

    fn kind_to_dirname(kind: AssetKind) -> &'static str {
        match kind {
            AssetKind::Texture => "textures",
            AssetKind::Brick => "bricks",
            AssetKind::Palette => "palettes",
        }
    }

    // TODO: should use resources_path
    pub struct NativeAssetSource {
        resources_path: PathBuf,
    }

    impl NativeAssetSource {
        pub fn new(resources_path: impl AsRef<Path>) -> NativeAssetSource {
            NativeAssetSource {
                resources_path: resources_path.as_ref().to_owned(),
            }
        }
    }

    impl AssetSource for NativeAssetSource {
        fn list(&self, kind: AssetKind) -> anyhow::Result<Vec<String>> {
            log::info!("listing assets of {:?}", kind);
            let path = self.resources_path.join(kind_to_dirname(kind));
            log::debug!("path {:?}", path);
            let entries = fs::read_dir(path)?;
            let mut out = Vec::new();
            for entry_ in entries {
                let entry = entry_?;
                let name = entry
                    .file_name()
                    .into_string()
                    .map_err(|_| anyhow::anyhow!("could not convert os string to string"))?;
                out.push(name);
            }
            Ok(out)
        }

        fn load(&self, kind: AssetKind, name: &str) -> anyhow::Result<Asset> {
            log::info!("loading asset {:?} {:?}", kind, name);
            // TODO: defend against path traversal attacks
            let path = self.resources_path.join(kind_to_dirname(kind)).join(name);
            log::debug!("path {:?}", path);
            let data = fs::read(path)?;
            Ok(Asset {
                kind,
                name: name.to_owned(),
                data,
            })
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct AssetId(pub usize);

pub struct AssetManager {
    source: Box<dyn AssetSource>,
    assets: Vec<Asset>,
    asset_map: HashMap<(AssetKind, String), AssetId>,
}

impl AssetManager {
    pub fn for_platform() -> AssetManager {
        // TODO: don't hard-code the resources path
        #[cfg(not(target_arch = "wasm32"))]
        let source = native::NativeAssetSource::new("resources");
        AssetManager::new(source)
    }

    pub fn new<S: AssetSource + 'static>(source: S) -> AssetManager {
        AssetManager {
            source: Box::new(source),
            assets: Vec::new(),
            asset_map: HashMap::new(),
        }
    }

    pub fn list(&self, kind: AssetKind) -> anyhow::Result<Vec<String>> {
        self.source.list(kind)
    }

    pub fn load(&mut self, kind: AssetKind, name: &str) -> anyhow::Result<AssetId> {
        let key = (kind, name.to_owned());
        if let Some(&asset_id) = self.asset_map.get(&key) {
            Ok(asset_id)
        } else {
            let new_asset_id = AssetId(self.assets.len());
            let asset = self.source.load(kind, name)?;
            self.assets.push(asset);
            self.asset_map.insert(key, new_asset_id);
            Ok(new_asset_id)
        }
    }

    pub fn get(&mut self, asset_id: AssetId) -> anyhow::Result<AssetRef> {
        Ok(self.assets[asset_id.0].as_ref())
    }
}
