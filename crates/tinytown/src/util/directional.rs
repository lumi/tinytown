use {
    cgmath::{BaseNum, Vector3},
    core::ops::{Index, IndexMut, Neg},
};

/// A direction along or against an axis.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum Direction {
    /// Eastwards, along the positive X axis.
    East = 0,
    /// Upwards, along the positive Y axis.
    Up = 1,
    /// Northwards, along the positive Z axis.
    North = 2,
    /// Westwards, along the negative X axis.
    West = 3,
    /// Downwards, along the negative Y axis.
    Down = 4,
    /// Southwards, along the negative Z axis.
    South = 5,
}

impl Direction {
    /// Turn a `Direction` into a `Vector3`.
    pub fn into_vector<S: BaseNum + Neg<Output = S>>(self) -> Vector3<S> {
        match self {
            Direction::East => Vector3::unit_x(),
            Direction::Up => Vector3::unit_y(),
            Direction::North => Vector3::unit_z(),
            Direction::West => -Vector3::unit_x(),
            Direction::Down => -Vector3::unit_y(),
            Direction::South => -Vector3::unit_z(),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Directional<T>(pub [T; 6]);

impl<T: Copy> Directional<T> {
    pub fn all(value: T) -> Directional<T> {
        Directional([value; 6])
    }

    pub fn west(self) -> T {
        self.0[0]
    }

    pub fn east(self) -> T {
        self.0[3]
    }

    pub fn up(self) -> T {
        self.0[1]
    }

    pub fn down(self) -> T {
        self.0[4]
    }

    pub fn north(self) -> T {
        self.0[2]
    }

    pub fn south(self) -> T {
        self.0[5]
    }
}

impl<T> Index<Direction> for Directional<T> {
    type Output = T;

    fn index(&self, dir: Direction) -> &T {
        &self.0[dir as u8 as usize]
    }
}

impl<T> IndexMut<Direction> for Directional<T> {
    fn index_mut(&mut self, dir: Direction) -> &mut T {
        &mut self.0[dir as u8 as usize]
    }
}
