use {cgmath::Point2, regenboog::RgbaU8};

use super::{rect::Rect, WidgetId};

use crate::tool::Tool;

#[derive(Debug, Clone)]
pub enum WidgetInner {
    Root,
    Column,
    Row,
    Button,
    ToolView(Tool),
    ColorView(RgbaU8),
    Crosshair,
    Label { label: String },
}

#[derive(Debug, Clone)]
pub struct WidgetVisual {
    pub rect: Rect,
    pub segtex_id: Option<usize>,
    pub color: RgbaU8,
}

impl WidgetVisual {
    pub fn new(rect: Rect) -> Self {
        WidgetVisual {
            rect,
            segtex_id: None,
            color: RgbaU8::WHITE,
        }
    }

    pub fn with_segtex_id<S: Into<Option<usize>>>(mut self, segtex_id: S) -> Self {
        self.segtex_id = segtex_id.into();
        self
    }

    pub fn with_color(mut self, color: RgbaU8) -> Self {
        self.color = color;
        self
    }
}

#[derive(Debug, Clone)]
pub struct Widget {
    pub parent_id: Option<WidgetId>,
    pub children: Vec<(Point2<f32>, WidgetId)>,
    pub inner: WidgetInner,
    pub visual: Option<WidgetVisual>,
}

impl Widget {
    pub fn root() -> Widget {
        Widget {
            parent_id: None,
            children: Vec::new(),
            inner: WidgetInner::Root,
            visual: None,
        }
    }

    pub fn column() -> Widget {
        Widget {
            parent_id: None,
            children: Vec::new(),
            inner: WidgetInner::Column,
            visual: None,
        }
    }

    pub fn button() -> Widget {
        Widget {
            parent_id: None,
            children: Vec::new(),
            inner: WidgetInner::Button,
            visual: None,
        }
    }

    pub fn color_view(color: RgbaU8) -> Widget {
        Widget {
            parent_id: None,
            children: Vec::new(),
            inner: WidgetInner::ColorView(color),
            visual: None,
        }
    }

    pub fn tool_view(tool: Tool) -> Widget {
        Widget {
            parent_id: None,
            children: Vec::new(),
            inner: WidgetInner::ToolView(tool),
            visual: None,
        }
    }

    pub fn crosshair() -> Widget {
        Widget {
            parent_id: None,
            children: Vec::new(),
            inner: WidgetInner::Crosshair,
            visual: None,
        }
    }
}
