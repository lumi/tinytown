use cgmath::{prelude::*, Point2, Vector2};

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Rect {
    pub min: Point2<f32>,
    pub max: Point2<f32>,
}

impl Rect {
    pub fn with_size(min: Point2<f32>, size: Vector2<f32>) -> Rect {
        Rect {
            min,
            max: min + size,
        }
    }

    pub fn with_center(center: Point2<f32>, size: Vector2<f32>) -> Rect {
        let half_size = size * 0.5;
        Rect {
            min: center - half_size,
            max: center + half_size,
        }
    }

    pub fn size(self) -> Vector2<f32> {
        self.max - self.min
    }

    pub fn inset(self, amount: f32) -> Rect {
        let off = Vector2::new(amount, amount);
        let min = self.min + off;
        let max = self.max - off;
        if max.x < min.x || max.y < min.y {
            let middle = min.midpoint(max);
            Rect {
                min: middle,
                max: middle,
            }
        } else {
            Rect { min, max }
        }
    }

    pub fn center(self) -> Point2<f32> {
        self.min.midpoint(self.max)
    }
}
