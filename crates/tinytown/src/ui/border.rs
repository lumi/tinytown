#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Border {
    pub left: f32,
    pub top: f32,
    pub right: f32,
    pub bottom: f32,
}

impl Border {
    pub fn all(amount: f32) -> Border {
        Border {
            left: amount,
            top: amount,
            right: amount,
            bottom: amount,
        }
    }
}


