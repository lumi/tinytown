use cgmath::Point2;

use super::rect::Rect;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum GravityAxis {
    Min,
    Center,
    Max,
}

impl GravityAxis {
    pub fn point(self, min: f32, max: f32) -> f32 {
        match self {
            GravityAxis::Min => min,
            GravityAxis::Center => min * 0.5 + max * 0.5,
            GravityAxis::Max => max,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Gravity {
    pub horizontal: GravityAxis,
    pub vertical: GravityAxis,
}

impl Gravity {
    pub fn top_left() -> Self {
        Self {
            horizontal: GravityAxis::Min,
            vertical: GravityAxis::Min,
        }
    }

    pub fn center() -> Self {
        Self {
            horizontal: GravityAxis::Center,
            vertical: GravityAxis::Center,
        }
    }

    pub fn point(self, rect: Rect) -> Point2<f32> {
        Point2::new(
            self.horizontal.point(rect.min.x, rect.max.x),
            self.vertical.point(rect.min.y, rect.max.y),
        )
    }
}
