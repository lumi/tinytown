use {
    cgmath::{prelude::*, Quaternion, Rad},
    pigeon::{Pack, Unpack},
};

/// The orientation of a brick, this can be one of four values:
///
/// ```plain
/// orientation = 0:
///
/// +-+-+-+
/// | | | |
/// +-+-+-+
///
/// -->
///
/// orientation = 1:
///
/// +-+
/// | |
/// +-+
/// | |
/// +-+ ^
/// | | |
/// +-+ |
///
/// orientation = 2:
///
///     <--
///
/// +-+-+-+
/// | | | |
/// +-+-+-+
///
/// orientation = 3:
///
/// | +-+
/// | | |
/// v +-+
///   | |
///   +-+
///   | |
///   +-+
/// ```
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct BrickOrientation(u8);

impl Default for BrickOrientation {
    fn default() -> BrickOrientation {
        BrickOrientation(0)
    }
}

impl Pack for BrickOrientation {
    fn pack<T: pigeon::Target>(&self, writer: &mut pigeon::Writer<T>) -> pigeon::WriteResult<()> {
        writer.write(self.as_u8())
    }
}

impl<'a> Unpack<'a> for BrickOrientation {
    fn unpack(reader: &mut pigeon::Reader<'a>) -> pigeon::ReadResult<Self> {
        reader.read().map(BrickOrientation::from_u8)
    }
}

impl BrickOrientation {
    /// Convert a `u8` to a `BrickOrientation`.
    ///
    /// This will wrap around when above 3.
    pub fn from_u8(num: u8) -> BrickOrientation {
        BrickOrientation(num % 4)
    }

    /// Convert this to a `u8`.
    pub fn as_u8(self) -> u8 {
        self.0
    }

    /// Rotate the brick by 90°
    pub fn next(self) -> BrickOrientation {
        BrickOrientation((self.0 + 1) % 4)
    }

    /// Rotate the brick by 90°
    pub fn prev(self) -> BrickOrientation {
        if self.0 == 0 {
            BrickOrientation(3)
        } else {
            BrickOrientation(self.0 - 1)
        }
    }

    /// Convert this orientation to a quaternion.
    pub fn to_quaternion(self) -> Quaternion<f32> {
        let angle = Rad::turn_div_4() * self.0 as f32;
        Quaternion::from_angle_y(angle)
    }
}
