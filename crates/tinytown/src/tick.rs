/// Represents a tick in the simulation.
#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Eq, Ord, Default)]
pub struct Tick(pub u32);

impl Tick {
    pub fn next(self) -> Self {
        Self(self.0 + 1)
    }
}
