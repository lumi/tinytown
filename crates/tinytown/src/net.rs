pub mod client;
pub mod connection;
pub mod controls;
pub mod data;
pub mod pending;
pub mod protocol;
pub mod push;
pub mod server;
pub mod tick_buffer;

pub use {
    client::ClientConnection,
    connection::Connection,
    controls::{ControlsQueue, ControlsQueueEntry},
    data::{BrickInfo, FullGhostBrickInfo, FullPlayerInfo},
    pending::{PendingQueues, Priority, TaggedQueue},
    push::{PushEvent, PushTag, PushWindow},
    server::ConnectionManager,
    tick_buffer::TickBuffer,
};

pub const MAX_PACKET_SIZE: usize = 1024;

#[derive(Debug, Clone)]
pub enum Controller {
    None,
    Local,
    Remote(SlotId),
}

impl Controller {
    pub fn is_local(&self) -> bool {
        if let Controller::Local = self {
            true
        } else {
            false
        }
    }

    pub fn is_remote_slot(&self, slot: SlotId) -> bool {
        if let Controller::Remote(this_slot) = *self {
            this_slot == slot
        } else {
            false
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct SlotId(pub usize);
