use std::{
    cell::{RefCell, RefMut},
    path::{Path, PathBuf},
    rc::Rc,
    sync::{Arc, Mutex},
};

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TextureId(usize);

#[derive(Clone, Debug)]
pub struct TextureManager<T> {
    textures: Vec<Rc<RefCell<T>>>,
    texture_paths: Arc<Mutex<Vec<PathBuf>>>,
    resources_path: PathBuf,
}

impl<T> TextureManager<T> {
    pub fn new(resources_path: impl AsRef<Path>) -> TextureManager<T> {
        TextureManager {
            textures: Vec::new(),
            texture_paths: Arc::new(Mutex::new(Vec::new())),
            resources_path: resources_path.as_ref().to_owned(),
        }
    }

    pub fn proxy(&self) -> TextureManagerProxy {
        TextureManagerProxy {
            texture_paths: self.texture_paths.clone(),
            resources_path: self.resources_path.clone(),
        }
    }

    pub fn load<P: AsRef<Path>>(&self, path: P) -> TextureId {
        let full_path = self.resources_path.join(path);
        log::info!("loading texture from {}", full_path.display());
        load_texture(&self.texture_paths, full_path)
    }

    pub fn sync<L: TextureLoader<Texture = T>>(&mut self, loader: &mut L) -> anyhow::Result<()> {
        let texture_paths = self.texture_paths.lock().unwrap();
        for i in self.textures.len()..texture_paths.len() {
            let path = &texture_paths[i];
            log::debug!("loading texture id {} with path {}", i, path.display());
            let tex = loader.load_texture(path)?;
            self.textures.push(Rc::new(RefCell::new(tex)));
        }
        Ok(())
    }

    pub fn get(&self, TextureId(idx): TextureId) -> RefMut<T> {
        // TODO: feels like a big hack
        self.textures[idx].borrow_mut()
    }
}

#[derive(Clone)]
pub struct TextureManagerProxy {
    texture_paths: Arc<Mutex<Vec<PathBuf>>>,
    resources_path: PathBuf,
}

impl TextureManagerProxy {
    pub fn load<P: AsRef<Path>>(&self, path: P) -> TextureId {
        let full_path = self.resources_path.join(path);
        log::info!("loading texture from {}", full_path.display());
        load_texture(&self.texture_paths, full_path)
    }
}

fn load_texture<P: AsRef<Path>>(texture_paths: &Mutex<Vec<PathBuf>>, path: P) -> TextureId {
    let path = path.as_ref();
    let mut texture_paths = texture_paths.lock().unwrap();
    // TODO: dedup
    //for (i, tex_path) in texture_paths.iter().enumerate() {
    //    if path == tex_path {
    //        return TextureId(i);
    //    }
    //}
    let len = texture_paths.len();
    texture_paths.push(path.to_owned());
    TextureId(len)
}

pub trait TextureLoader {
    type Texture;

    fn load_texture(&mut self, path: impl AsRef<Path>) -> anyhow::Result<Self::Texture>;
}
