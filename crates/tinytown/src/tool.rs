use pigeon::{Pack, Unpack};

/// The tool that a player has equipped.
#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum Tool {
    /// No tool
    None = 0,
    /// Brick building tool
    Brick = 1,
    /// Hammer tool
    Hammer = 2,
    /// Paint can tool
    PaintCan = 3,
    /// The best tool
    GimpPaprika = 4,
}

impl Tool {
    /// Cycle backwards through all tools.
    pub fn prev(self) -> Tool {
        match self {
            Tool::None => Tool::GimpPaprika,
            Tool::Brick => Tool::None,
            Tool::Hammer => Tool::Brick,
            Tool::PaintCan => Tool::Hammer,
            Tool::GimpPaprika => Tool::PaintCan,
        }
    }

    /// Cycle forwards through all tools.
    pub fn next(self) -> Tool {
        match self {
            Tool::None => Tool::Brick,
            Tool::Brick => Tool::Hammer,
            Tool::Hammer => Tool::PaintCan,
            Tool::PaintCan => Tool::GimpPaprika,
            Tool::GimpPaprika => Tool::None,
        }
    }

    /// Convert the tool to a byte.
    pub fn as_u8(self) -> u8 {
        self as u8
    }

    /// Create a tool from a byte.
    pub fn from_u8(byte: u8) -> Option<Self> {
        match byte {
            0 => Some(Tool::None),
            1 => Some(Tool::Brick),
            2 => Some(Tool::Hammer),
            3 => Some(Tool::PaintCan),
            4 => Some(Tool::GimpPaprika),
            _ => None,
        }
    }
}

impl Pack for Tool {
    fn pack<T: pigeon::Target>(&self, writer: &mut pigeon::Writer<T>) -> pigeon::WriteResult<()> {
        writer.write(self.as_u8())
    }
}

impl<'a> Unpack<'a> for Tool {
    fn unpack(reader: &mut pigeon::Reader<'a>) -> pigeon::ReadResult<Tool> {
        let byte = reader.read()?;
        Tool::from_u8(byte).ok_or_else(|| pigeon::ReadError::UnexpectedData)
    }
}
