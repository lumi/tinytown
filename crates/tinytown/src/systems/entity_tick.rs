use specs::prelude::*;

use crate::{components::EntityTick, resources::Sim};

pub struct EntityTickSystem;

impl<'a> System<'a> for EntityTickSystem {
    type SystemData = (WriteExpect<'a, Sim>, WriteStorage<'a, EntityTick>);

    fn run(&mut self, (mut sim, mut entity_ticks): Self::SystemData) {
        if sim.paused {
            return;
        }
        for (entity_tick,) in (&mut entity_ticks,).join() {
            if entity_tick.tick < sim.current_tick {
                entity_tick.tick = entity_tick.tick.next();
                if entity_tick.tick < sim.current_tick {
                    sim.repeat = true;
                }
            }
        }
    }
}
