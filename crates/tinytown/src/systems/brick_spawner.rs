use specs::prelude::*;

use crate::{
    brick::{BrickEvent, BrickEventQueue},
    components::{Brick, EntityKind, NetId},
    resources::{Autosave, NetIdAllocator, Network},
};

pub struct BrickSpawnerSystem;

impl<'a> System<'a> for BrickSpawnerSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, Network>,
        ReadExpect<'a, NetIdAllocator>,
        WriteExpect<'a, BrickEventQueue>,
        WriteExpect<'a, Autosave>,
        WriteStorage<'a, EntityKind>,
        WriteStorage<'a, Brick>,
        WriteStorage<'a, NetId>,
    );

    fn run(
        &mut self,
        (
            entities,
            network,
            nia,
            mut beq,
            mut autosave,
            mut entity_kinds,
            mut bricks,
            mut net_ids,
        ): Self::SystemData,
    ) {
        if network.is_authoritative() {
            beq.flush(|evt| match evt {
                BrickEvent::Build {
                    data_id,
                    color_id,
                    orientation,
                    pos,
                } => {
                    // TODO: maybe disallow bricks to intersect?
                    let net_id = nia.next_id();
                    let entity = entities
                        .build_entity()
                        .with(EntityKind::Brick, &mut entity_kinds)
                        .with(net_id, &mut net_ids)
                        .with(Brick::new(color_id, data_id, orientation, pos), &mut bricks)
                        .build();
                    nia.register_id(net_id, entity);
                    log::debug!("creating brick {:?} at {:?} with orientation={:?} color_id={:?} data_id={:?}", entity, pos, orientation, color_id, data_id);
                }
                BrickEvent::Paint { entity, color_id } => {
                    let brick = bricks.get_mut(entity).unwrap();
                    brick.color_id = color_id;
                    brick.version += 1;
                    log::debug!("painting brick {:?} to {:?}", entity, color_id);
                }
                BrickEvent::Destroy { entity } => {
                    log::debug!("removing brick {:?}", entity);
                    entities.delete(entity).expect("could not delete entity??");
                }
            });
            if beq.dirty {
                // TODO: make less hacky
                autosave.dirty = true;
                log::debug!("beq dirty, marking autosave dirty");
            }
        } else {
            beq.flush(|_| ());
        }
    }
}
