use {
    cgmath::{prelude::*, Point3, Quaternion, Vector3},
    specs::prelude::*,
    tinytown_collision::Shape,
};

use crate::{
    brick::BrickEventQueue,
    components::{Brick, EntityKind, EntityTick, GhostBrick, NetId, Physical, Player},
    net::{
        client::ConnectionEvent,
        data::{FullGhostBrickInfo, FullPlayerInfo},
        protocol::JoinRejectReason,
        Controller,
    },
    resources::{NetIdAllocator, Network, Sim},
    tick::Tick,
    vox::Vox,
};

pub struct ClientSyncStartSystem;

impl<'a> System<'a> for ClientSyncStartSystem {
    type SystemData = (
        Entities<'a>,
        WriteExpect<'a, Sim>,
        WriteExpect<'a, Network>,
        WriteExpect<'a, BrickEventQueue>,
        ReadExpect<'a, NetIdAllocator>,
        WriteStorage<'a, NetId>,
        WriteStorage<'a, Player>,
        WriteStorage<'a, Brick>,
        WriteStorage<'a, Physical>,
        WriteStorage<'a, GhostBrick>,
        WriteStorage<'a, EntityTick>,
        WriteStorage<'a, EntityKind>,
    );

    fn run(
        &mut self,
        (
            entities,
            mut sim,
            mut net,
            mut beq,
            nia,
            mut net_ids,
            mut players,
            mut bricks,
            mut physicals,
            mut ghost_bricks,
            mut entity_ticks,
            mut entity_kinds,
        ): Self::SystemData,
    ) {
        if !sim.initial {
            return;
        }
        if let Network::Client(connection) = &mut *net {
            sim.desync = false;
            connection
                .process(|event| {
                    log::debug!("connection event: {:?}", event);
                    match event {
                        ConnectionEvent::Joined { server_tick } => {
                            sim.current_tick.0 = server_tick + 10; // TODO: make this not magical
                            sim.paused = false;
                        }
                        ConnectionEvent::JoinRejected { reason } => match reason {
                            JoinRejectReason::Full => {
                                panic!("server full");
                            }
                        },
                        ConnectionEvent::DeleteGhost { net_id } => {
                            if let Some(entity) = nia.get_entity(net_id) {
                                if bricks.contains(entity) {
                                    beq.dirty = true;
                                }
                                entities.delete(entity).unwrap();
                            }
                        }
                        ConnectionEvent::UpdatePlayerGhost {
                            net_id,
                            position,
                            facing,
                        } => {
                            if let Some(entity) = nia.get_entity(net_id) {
                                if let Some(position) = position {
                                    if let Some(physical) = physicals.get_mut(entity) {
                                        physical.pos = position;
                                    }
                                }
                                if let Some(facing) = facing {
                                    if let Some(player) = players.get_mut(entity) {
                                        player.facing = facing;
                                    }
                                }
                            } else {
                                let player = Player::new(
                                    facing.unwrap_or(Vector3::new(0., 0., 0.)),
                                    Controller::None,
                                );
                                let physical = Physical::new(
                                    position.unwrap_or(Point3::origin()),
                                    Quaternion::one(),
                                    Shape::Cuboid(Vector3::new(0.5, 1., 0.5)),
                                );
                                let entity = entities
                                    .build_entity()
                                    .with(EntityKind::Player, &mut entity_kinds)
                                    .with(EntityTick::at(sim.current_tick), &mut entity_ticks)
                                    .with(net_id, &mut net_ids)
                                    .with(player, &mut players)
                                    .with(physical, &mut physicals)
                                    .build();
                                nia.register_id(net_id, entity);
                            }
                        }
                        ConnectionEvent::UpdateGhostBrickGhost {
                            net_id,
                            color_id,
                            data_id,
                            orientation,
                            pos,
                        } => {
                            if let Some(entity) = nia.get_entity(net_id) {
                                if let Some(gb) = ghost_bricks.get_mut(entity) {
                                    gb.color_id = color_id;
                                    gb.data_id = data_id;
                                    gb.orientation = orientation;
                                    gb.pos = pos;
                                } else {
                                    panic!("net id exists, associated component does not");
                                }
                            } else {
                                let gb = GhostBrick::new(
                                    None,
                                    color_id,
                                    data_id,
                                    orientation,
                                    Vox::from(pos),
                                );
                                let entity = entities
                                    .build_entity()
                                    .with(EntityKind::GhostBrick, &mut entity_kinds)
                                    .with(net_id, &mut net_ids)
                                    .with(gb, &mut ghost_bricks)
                                    .build();
                                nia.register_id(net_id, entity);
                            }
                        }
                        ConnectionEvent::UpdateBrickGhost {
                            net_id,
                            color_id,
                            data_id,
                            orientation,
                            pos,
                        } => {
                            // TODO: the BEQ should be handling this...
                            if let Some(entity) = nia.get_entity(net_id) {
                                if let Some(brick) = bricks.get_mut(entity) {
                                    brick.color_id = color_id;
                                    brick.data_id = data_id;
                                    brick.orientation = orientation;
                                    brick.pos = pos;
                                    beq.dirty = true;
                                } else {
                                    panic!("net id exists, associated component does not");
                                }
                            } else {
                                let brick =
                                    Brick::new(color_id, data_id, orientation, Vox::from(pos));
                                let entity = entities
                                    .build_entity()
                                    .with(EntityKind::Brick, &mut entity_kinds)
                                    .with(net_id, &mut net_ids)
                                    .with(brick, &mut bricks)
                                    .build();
                                nia.register_id(net_id, entity);
                                beq.dirty = true;
                            }
                        }
                        ConnectionEvent::UpdateOwnPlayer { tick, player_info } => {
                            for (entity, player, physical, entity_tick) in
                                (&entities, &mut players, &mut physicals, &mut entity_ticks).join()
                            {
                                sim.desync = true;
                                if player.controller.is_local() {
                                    physical.pos = player_info.pos;
                                    physical.vel = player_info.vel;
                                    player.tool = player_info.tool;
                                    player.selected_color_id = player_info.selected_color_id;
                                    if let Some(ghost_info) = &player_info.ghost_info {
                                        if let Some(gb_entity) = player.ghost_brick {
                                            let gb = ghost_bricks.get_mut(gb_entity).unwrap();
                                            gb.data_id = ghost_info.data_id;
                                            gb.color_id = ghost_info.color_id;
                                            gb.pos = ghost_info.pos;
                                            gb.orientation = ghost_info.orientation;
                                        } else {
                                            let gb = GhostBrick::new(
                                                Some(entity),
                                                ghost_info.color_id,
                                                ghost_info.data_id,
                                                ghost_info.orientation,
                                                ghost_info.pos,
                                            );
                                            let gb_entity = entities
                                                .build_entity()
                                                .with(EntityKind::GhostBrick, &mut entity_kinds)
                                                .with(gb, &mut ghost_bricks)
                                                .build();
                                            player.ghost_brick = Some(gb_entity);
                                        }
                                    } else {
                                        if let Some(gb_entity) = player.ghost_brick {
                                            entities.delete(gb_entity).unwrap();
                                            player.ghost_brick = None;
                                        }
                                    }
                                    entity_tick.tick = Tick(tick);
                                }
                            }
                        }
                    }
                })
                .expect("connection processing failure");
            if connection.is_connected() {
                let target_tick = connection.server_tick() + 10;
                if sim.current_tick.0 != target_tick {
                    log::warn!(
                        "ticking out of sync: target={} current={}",
                        target_tick,
                        sim.current_tick.0
                    );
                    sim.current_tick.0 = target_tick;
                }
            }
        }
    }
}

pub struct ClientSyncEndSystem;

impl<'a> System<'a> for ClientSyncEndSystem {
    type SystemData = (
        WriteExpect<'a, Network>,
        ReadExpect<'a, Sim>,
        ReadStorage<'a, Player>,
        ReadStorage<'a, Physical>,
        ReadStorage<'a, GhostBrick>,
    );

    fn run(&mut self, (mut net, sim, players, physicals, ghost_bricks): Self::SystemData) {
        // TODO: needs a lot of reworking to deal with its indexes
        if sim.repeat {
            return;
        }
        if let Network::Client(connection) = &mut *net {
            if connection.is_connected() {
                for (player, physical) in (&players, &physicals).join() {
                    if player.controller.is_local() {
                        let pos = physical.pos;
                        let vel = physical.vel;
                        let tool = player.tool;
                        let ghost_info = if let Some(gb_entity) = player.ghost_brick {
                            if let Some(gb) = ghost_bricks.get(gb_entity) {
                                Some(FullGhostBrickInfo {
                                    data_id: gb.data_id,
                                    color_id: gb.color_id,
                                    pos: gb.pos,
                                    orientation: gb.orientation,
                                })
                            } else {
                                log::error!("the player has a ghost brick entity assigned, but that entity does not exist");
                                None
                            }
                        } else {
                            None
                        };
                        let player_info = FullPlayerInfo {
                            pos,
                            vel,
                            tool,
                            selected_color_id: player.selected_color_id,
                            ghost_info,
                        };
                        connection.update_player_state(&player_info);
                        if let Some(cqe) = player.controls.at_tick(sim.current_tick.0) {
                            connection.push_controls(
                                sim.current_tick.0,
                                cqe.controls.clone(),
                                cqe.facing,
                            );
                        } else {
                            log::trace!("player controls queue: {:?}", player.controls);
                            log::error!("no controls to send!!");
                        }
                    }
                }
            }
            connection.flush_frames().unwrap();
        }
    }
}
