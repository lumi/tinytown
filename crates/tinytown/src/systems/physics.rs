use {cgmath::Vector3, specs::prelude::*};

use crate::{
    components::{EntityTick, Physical, Player},
    resources::Sim,
};

pub struct PhysicsSystem;

impl<'a> System<'a> for PhysicsSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, Sim>,
        WriteStorage<'a, Physical>,
        WriteStorage<'a, Player>,
        WriteStorage<'a, EntityTick>,
    );

    fn run(
        &mut self,
        (entities, sim, mut physicals, mut players, mut entity_ticks): Self::SystemData,
    ) {
        if sim.paused {
            return;
        }
        let gravity = Vector3::new(0., -0.06, 0.);
        for (ent, physical, entity_tick) in (&*entities, &mut physicals, &mut entity_ticks).join() {
            if entity_tick.tick < sim.current_tick {
                log::trace!("stepping entity {:?}", ent);
                physical.vel *= 0.88;
                physical.vel += gravity;
                physical.pos += physical.vel;
                // TODO: this really should be done with a plane collider
                let player_center_height = 1.;
                if physical.pos.y < player_center_height {
                    physical.pos.y = player_center_height;
                    if let Some(player) = players.get_mut(ent) {
                        player.can_jump = true;
                    }
                }
            }
        }
    }
}
