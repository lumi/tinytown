use specs::prelude::*;

use crate::{
    brick::BrickPalette,
    components::Player,
    resources::Sim,
    ui::{GameUi, Ui},
};

pub struct UiSystem;

impl<'a> System<'a> for UiSystem {
    type SystemData = (
        WriteExpect<'a, Ui>,
        ReadExpect<'a, BrickPalette>,
        ReadExpect<'a, Sim>,
        WriteExpect<'a, GameUi>,
        ReadStorage<'a, Player>,
    );

    fn run(&mut self, (mut ui, bp, sim, mut game_ui, players): Self::SystemData) {
        if sim.repeat {
            return;
        }
        for (player,) in (&players,).join() {
            if player.controller.is_local() {
                let color = bp[player.selected_color_id];
                game_ui.set_current_tool(&mut ui, player.tool);
                game_ui.set_current_color(&mut ui, color);
            }
        }
        game_ui.set_desync_indicator(&mut ui, sim.desync);
    }
}
