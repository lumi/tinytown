use {
    specs::prelude::*,
    std::{
        fs::File,
        time::{Duration, Instant},
    },
};

use crate::{
    brick::{BrickDataStore, BrickPalette},
    components::Brick,
    consts::AUTOSAVE_BACKOFF_MS,
    resources::Autosave,
};

pub struct AutosaveSystem;

impl<'a> System<'a> for AutosaveSystem {
    type SystemData = (
        WriteExpect<'a, Autosave>,
        ReadExpect<'a, BrickDataStore>,
        ReadExpect<'a, BrickPalette>,
        ReadStorage<'a, Brick>,
    );

    fn run(&mut self, (mut autosave, bds, bp, bricks): Self::SystemData) {
        if let Some(autosave_path) = &autosave.path {
            if autosave.dirty
                && autosave.last_save.elapsed() > Duration::from_millis(AUTOSAVE_BACKOFF_MS)
            {
                log::info!("saving the world to {}", autosave_path.display());
                let file = File::create(autosave_path).unwrap();
                let mut writer = ttb::Writer::new(file);
                let palette: Vec<_> = bp.iter().map(|(_, color)| color).collect();
                writer.write_header("autosave", "", &palette).unwrap();
                for (brick,) in (&bricks,).join() {
                    let bd = &bds[brick.data_id];
                    let name = &bd.name;
                    let color_id = brick.color_id.0 as u8;
                    writer
                        .write_brick(
                            name,
                            brick.pos.x,
                            brick.pos.y,
                            brick.pos.z,
                            brick.orientation.as_u8(),
                            color_id,
                        )
                        .unwrap();
                }
                writer.finish().unwrap();
                log::info!("saved!");
                autosave.dirty = false;
                autosave.last_save = Instant::now();
            }
        }
    }
}
