use {
    approx::ulps_eq,
    cgmath::{prelude::*, Quaternion, Rad, Vector3},
    specs::prelude::*,
};

use crate::{
    components::{EntityTick, Physical, Player},
    controls::Control,
    resources::Sim,
};

const MOVE_SPEED_FORWARD: f32 = 0.03;
const MOVE_SPEED_BACKWARD: f32 = 0.02;
const MOVE_SPEED_SIDEWAYS: f32 = 0.01;

pub struct MovementSystem;

impl<'a> System<'a> for MovementSystem {
    type SystemData = (
        ReadExpect<'a, Sim>,
        WriteStorage<'a, Player>,
        WriteStorage<'a, Physical>,
        WriteStorage<'a, EntityTick>,
    );

    fn run(&mut self, (sim, mut players, mut physicals, mut entity_ticks): Self::SystemData) {
        if sim.paused {
            return;
        }
        let up = Vector3::new(0., 1., 0.);
        for (player, physical, entity_tick) in
            (&mut players, &mut physicals, &mut entity_ticks).join()
        {
            let rot = player.facing.x.atan2(player.facing.z);
            physical.rot = Quaternion::from_angle_y(Rad(rot));
            if let Some(cqe) = player.controls.at_tick(entity_tick.tick.0) {
                let ctl = &cqe.controls;
                let facing = cqe.facing;
                let mut vel = Vector3::new(0., 0., 0.);
                let (forward, lateral) = if !ulps_eq!(facing.y.abs(), 1.) {
                    (
                        Vector3::new(facing.x, 0., facing.z).normalize(),
                        Vector3::new(-facing.z, 0., facing.x).normalize(),
                    )
                } else {
                    (Vector3::new(0., 0., 0.), Vector3::new(0., 0., 0.))
                };
                if ctl.activated(Control::Jump) && player.can_jump {
                    vel += up * 2.;
                }
                if ctl.activated(Control::MoveForward) {
                    log::trace!("moving forward: {:?}", forward);
                    vel += forward * MOVE_SPEED_FORWARD;
                }
                if ctl.activated(Control::MoveBackward) {
                    log::trace!("moving backward: {:?}", -forward);
                    vel -= forward * MOVE_SPEED_BACKWARD;
                }
                if ctl.activated(Control::MoveRight) {
                    log::trace!("moving right: {:?}", lateral);
                    vel += lateral * MOVE_SPEED_SIDEWAYS;
                }
                if ctl.activated(Control::MoveLeft) {
                    log::trace!("moving left: {:?}", -lateral);
                    vel -= lateral * MOVE_SPEED_SIDEWAYS;
                }
                if ctl.activated(Control::Jetpack) {
                    vel += up * 0.3;
                }
                physical.vel += vel;
            }
            player.can_jump = false;
        }
    }
}
