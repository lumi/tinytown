use specs::prelude::*;

use crate::{
    camera::CameraMode,
    controls::{Action, Control, ControlsManager},
};

pub struct CameraControlSystem;

impl<'a> System<'a> for CameraControlSystem {
    type SystemData = (ReadExpect<'a, ControlsManager>, WriteExpect<'a, CameraMode>);

    fn run(&mut self, (controls_manager, mut camera_mode): Self::SystemData) {
        for &(action, control) in &controls_manager.state.events {
            if action == Action::Press && control == Control::SwitchCamera {
                log::debug!("switching cameras");
                match *camera_mode {
                    CameraMode::None => (),
                    CameraMode::FirstPerson { entity } => {
                        *camera_mode = CameraMode::ThirdPerson { entity };
                    }
                    CameraMode::ThirdPerson { entity } => {
                        *camera_mode = CameraMode::FirstPerson { entity };
                    }
                }
            }
        }
    }
}
