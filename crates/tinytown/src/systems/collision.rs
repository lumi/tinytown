use {
    cgmath::{prelude::*, Quaternion, Vector3},
    fnv::FnvHashSet,
    specs::{prelude::*, world::Index},
    std::{
        cmp::{Eq, PartialEq},
        hash::{Hash, Hasher},
        time::Instant,
    },
    tinytown_collision::{
        broad::bvh::AabbQueryCache, narrow::gjk::gjk_collide, Aabb, CollisionMask, Contact,
        ContactPair, Shape,
    },
};

use crate::{
    brick::BrickDataStore,
    collision_manager::CollisionManager,
    components::{Brick, Physical, Player},
    resources::{Sim, Stats},
    vox::{VoxAabb, VOXEL_SIZE},
};

struct CandidatePair(Index, Index);

impl PartialEq for CandidatePair {
    fn eq(&self, other: &CandidatePair) -> bool {
        let mut s0 = self.0;
        let mut s1 = self.1;
        let mut o0 = other.0;
        let mut o1 = other.1;
        if s0 > s1 {
            let t = s0;
            s0 = s1;
            s1 = t;
        }
        if o0 > o1 {
            let t = o0;
            o0 = o1;
            o1 = t;
        }
        (s0, s1) == (o0, o1)
    }
}

impl Eq for CandidatePair {}

impl Hash for CandidatePair {
    fn hash<H: Hasher>(&self, state: &mut H) {
        if self.0 > self.1 {
            self.0.hash(state);
            self.1.hash(state);
        } else {
            self.1.hash(state);
            self.0.hash(state);
        }
    }
}

pub struct CollisionIndexSystem {
    bricks_reader_id: ReaderId<ComponentEvent>,
    physicals_reader_id: ReaderId<ComponentEvent>,
    bricks_added: BitSet,
    bricks_modified: BitSet,
    bricks_removed: BitSet,
    physicals_added: BitSet,
    physicals_modified: BitSet,
    physicals_removed: BitSet,
}

impl CollisionIndexSystem {
    pub fn new(world: &World) -> CollisionIndexSystem {
        let mut bricks = world.write_storage::<Brick>();
        let mut physicals = world.write_storage::<Physical>();
        let bricks_reader_id = bricks.channel_mut().register_reader();
        let physicals_reader_id = physicals.channel_mut().register_reader();
        CollisionIndexSystem {
            bricks_reader_id,
            physicals_reader_id,
            bricks_added: BitSet::new(),
            bricks_modified: BitSet::new(),
            bricks_removed: BitSet::new(),
            physicals_added: BitSet::new(),
            physicals_modified: BitSet::new(),
            physicals_removed: BitSet::new(),
        }
    }
}

impl<'a> System<'a> for CollisionIndexSystem {
    type SystemData = (
        Write<'a, CollisionManager>,
        ReadExpect<'a, BrickDataStore>,
        Entities<'a>,
        ReadStorage<'a, Physical>,
        ReadStorage<'a, Brick>,
    );

    fn run(&mut self, (mut collision_manager, bds, entities, physicals, bricks): Self::SystemData) {
        self.bricks_added.clear();
        self.bricks_modified.clear();
        self.bricks_removed.clear();
        self.physicals_added.clear();
        self.physicals_modified.clear();
        self.physicals_removed.clear();
        for &event in physicals.channel().read(&mut self.physicals_reader_id) {
            match event {
                ComponentEvent::Inserted(idx) => {
                    self.physicals_added.add(idx);
                }
                ComponentEvent::Modified(idx) => {
                    self.physicals_modified.add(idx);
                }
                ComponentEvent::Removed(idx) => {
                    self.physicals_removed.add(idx);
                }
            }
        }
        for &event in bricks.channel().read(&mut self.bricks_reader_id) {
            match event {
                ComponentEvent::Inserted(idx) => {
                    self.bricks_added.add(idx);
                }
                ComponentEvent::Modified(_idx) => {
                    // TODO: check brick modifications
                }
                ComponentEvent::Removed(idx) => {
                    self.bricks_removed.add(idx);
                }
            }
        }
        for (entity, physical, _) in (&*entities, &physicals, &self.physicals_added).join() {
            log::debug!("adding entity {:?} to bvh as physical", entity);
            let shape = physical.shape.with_transform(physical.pos, physical.rot);
            // TODO: expand shape AABB out to account for velocity
            collision_manager.add(
                entity.id(),
                CollisionMask::PHYSICAL,
                CollisionMask::BRICK.union(CollisionMask::PHYSICAL),
                shape,
            );
        }
        for (entity, brick, _) in (&*entities, &bricks, &self.bricks_added).join() {
            log::debug!("adding entity {:?} to the brick grid", entity);
            let brick_data = &bds[brick.data_id];
            let vox_aabb = VoxAabb::with_orientation(brick.pos, brick_data.size, brick.orientation);
            log::trace!("vox_aabb={:?}", vox_aabb);
            collision_manager.add_brick(entity.id(), vox_aabb);
        }
        for (entity, physical, _) in (&*entities, &physicals, &self.physicals_modified).join() {
            log::debug!("modified entity {:?} from bvh", entity);
            let shape = physical.shape.with_transform(physical.pos, physical.rot);
            // TODO: expand shape AABB out to account for velocity
            collision_manager.update(
                entity.id(),
                CollisionMask::PHYSICAL,
                CollisionMask::BRICK.union(CollisionMask::PHYSICAL),
                shape,
            );
        }
        // TODO: check brick modifications
        for (entity,) in (&self.physicals_removed,).join() {
            log::debug!("removing entity {:?} from bvh", entity);
            collision_manager.remove(entity);
        }
        for (entity,) in (&self.bricks_removed,).join() {
            log::debug!("removing entity {:?} from the brick grid", entity);
            collision_manager.remove_brick(entity);
        }
    }
}

pub struct CollisionSystem {
    candidates_cache: FnvHashSet<CandidatePair>,
    aabb_query_cache: AabbQueryCache,
}

impl CollisionSystem {
    pub fn new() -> CollisionSystem {
        CollisionSystem {
            candidates_cache: FnvHashSet::default(),
            aabb_query_cache: AabbQueryCache::new(),
        }
    }
}

impl<'a> System<'a> for CollisionSystem {
    type SystemData = (
        ReadExpect<'a, Sim>,
        Write<'a, CollisionManager>,
        ReadExpect<'a, BrickDataStore>,
        WriteExpect<'a, Stats>,
        Entities<'a>,
        WriteStorage<'a, Physical>,
        WriteStorage<'a, Player>,
        ReadStorage<'a, Brick>,
    );

    fn run(
        &mut self,
        (sim, mut collision_manager, bds, mut stats, entities, mut physicals, mut players, bricks): Self::SystemData,
    ) {
        if sim.paused {
            return;
        }
        let start_broad = Instant::now();
        self.candidates_cache.clear();
        for (entity1, entity2) in collision_manager.colliding_pairs() {
            log::debug!(
                "potential collision between {:?} and {:?}",
                entity1,
                entity2
            );
            self.candidates_cache
                .insert(CandidatePair(entity1, entity2));
        }
        collision_manager.colliding_with_bricks(|e1, e2| {
            self.candidates_cache.insert(CandidatePair(e1, e2));
        });
        stats
            .broad_phase_collision_time_ns
            .push(start_broad.elapsed().subsec_nanos());
        stats
            .narrow_phase_collisions
            .push(self.candidates_cache.len() as u32);
        let start_narrow = Instant::now();
        let mut collisions = Vec::with_capacity(self.candidates_cache.len());
        for CandidatePair(ca, cb) in self.candidates_cache.drain() {
            let a = entities.entity(ca);
            let b = entities.entity(cb);
            if bricks.contains(a) && bricks.contains(b) {
                log::error!("bricks should not be able to collide, something went really wrong");
                continue;
            }
            let shape_a = if let Some(brick) = bricks.get(a) {
                let brick_data = &bds[brick.data_id];
                let vox_aabb =
                    VoxAabb::with_orientation(brick.pos, brick_data.size, brick.orientation);
                let world_aabb = Aabb::from(vox_aabb);
                Shape::Cuboid(world_aabb.radius())
                    .with_transform(world_aabb.center(), Quaternion::one())
            } else if let Some(physical) = physicals.get(a) {
                physical.transformed_shape()
            } else {
                unreachable!()
            };
            let shape_b = if let Some(brick) = bricks.get(b) {
                let brick_data = &bds[brick.data_id];
                let vox_aabb =
                    VoxAabb::with_orientation(brick.pos, brick_data.size, brick.orientation);
                let world_aabb = Aabb::from(vox_aabb);
                Shape::Cuboid(world_aabb.radius())
                    .with_transform(world_aabb.center(), Quaternion::one())
            } else if let Some(physical) = physicals.get(b) {
                physical.transformed_shape()
            } else {
                unreachable!()
            };
            if let Some(face) = gjk_collide(&shape_a, &shape_b) {
                collisions.push(ContactPair {
                    value_a: a,
                    value_b: b,
                    contact: Contact {
                        normal: face.normal,
                        depth: face.depth,
                    },
                });
            }
        }
        stats
            .narrow_phase_collision_time_ns
            .push(start_narrow.elapsed().subsec_nanos());
        for contact in collisions.drain(..) {
            let mut entity_a = contact.value_a;
            let mut entity_b = contact.value_b;
            let mut normal = contact.contact.normal;
            let depth = contact.contact.depth;
            if bricks.contains(entity_a) {
                let temp = entity_a;
                entity_a = entity_b;
                entity_b = temp;
                normal = -normal;
            }
            if !bricks.contains(entity_b) {
                // not a brick collision
                continue;
            }
            let brick = bricks.get(entity_b).unwrap();
            let physical = physicals.get_mut(entity_a).unwrap();
            let brick_data = &bds[brick.data_id];
            let vox_aabb = VoxAabb::with_orientation(brick.pos, brick_data.size, brick.orientation);
            let world_aabb = Aabb::from(vox_aabb);
            let brick_shape = Shape::Cuboid(world_aabb.radius())
                .with_transform(world_aabb.center(), Quaternion::one());
            let physical_shape = physical.transformed_shape();
            // TODO: this is some duplication of work
            if let Some(_) = gjk_collide(&physical_shape, &brick_shape) {
                if normal.y == 0. {
                    let brick_data = &bds[brick.data_id];
                    let brick_top_y =
                        brick.pos.world_min().y + VOXEL_SIZE.y * brick_data.size.y as f32; // TODO: clean up
                    let physical_aabb = physical.aabb();
                    let new_aabb = physical_aabb + Vector3::new(0., brick_top_y + 0.01, 0.);
                    let hit = collision_manager
                        .query_aabb(&mut self.aabb_query_cache, CollisionMask::BRICK, new_aabb)
                        .next()
                        .is_some();
                    if new_aabb.min.y >= physical_aabb.min.y
                        && !hit
                        && (new_aabb.min.y - physical_aabb.min.y).abs() <= 1.3
                    {
                        physical.vel.y = 0.;
                        physical.pos = new_aabb.center();
                    } else {
                        physical.vel -= physical.vel.project_on(normal);
                        physical.pos -= normal * depth;
                    }
                } else {
                    physical.vel -= physical.vel.project_on(normal);
                    physical.pos -= normal * depth;
                }
                if let Some(p) = players.get_mut(entity_a) {
                    if normal.dot(Vector3::new(0., -1., 0.)) > 0.5 {
                        p.can_jump = true;
                    }
                }
            }
        }
    }
}
