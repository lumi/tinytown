use {
    cgmath::{prelude::*, Point3, Quaternion, Vector3},
    crc::crc16,
    specs::prelude::*,
    tinytown_collision::Shape,
};

use crate::{
    components::{Brick, EntityKind, EntityTick, GhostBrick, NetId, Physical, Player},
    consts::SERVER_CONTROLS_QUEUE_SIZE,
    net::{
        data::{FullGhostBrickInfo, FullPlayerInfo},
        push::GhostTag,
        server::ConnectionEvent,
        Controller,
    },
    resources::{NetIdAllocator, Network, Sim},
};

pub struct ServerSyncStartSystem;

impl<'a> System<'a> for ServerSyncStartSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, Sim>,
        ReadExpect<'a, NetIdAllocator>,
        WriteExpect<'a, Network>,
        WriteStorage<'a, NetId>,
        WriteStorage<'a, Player>,
        WriteStorage<'a, Physical>,
        WriteStorage<'a, EntityTick>,
        WriteStorage<'a, EntityKind>,
        ReadStorage<'a, GhostBrick>,
        ReadStorage<'a, Brick>,
    );

    fn run(
        &mut self,
        (
            entities,
            sim,
            nia,
            mut net,
            mut net_ids,
            mut players,
            mut physicals,
            mut entity_ticks,
            mut entity_kinds,
            ghost_bricks,
            bricks,
        ): Self::SystemData,
    ) {
        if !sim.initial {
            return;
        }
        if let Network::Server(connection_manager) = &mut *net {
            connection_manager
                .start_tick(sim.current_tick.0, |conn, evt| match evt {
                    ConnectionEvent::PlayerLeft { slot } => {
                        log::info!("player in slot {:?} left", slot);
                        if let Some(entity) = conn.player_entity {
                            let player = players.get(entity).unwrap();
                            if let Some(gb_entity) = player.ghost_brick {
                                entities.delete(gb_entity).unwrap();
                            }
                            entities.delete(entity).unwrap();
                        }
                    }
                    _ => (),
                })
                .unwrap();
            connection_manager
            .handle_frames(|conn, evt| match evt {
                ConnectionEvent::PlayerJoined { slot, .. } => {
                    log::info!("player in slot {:?} joined", slot);
                    let net_id = nia.next_id();
                    let entity = entities
                        .build_entity()
                        .with(EntityKind::Player, &mut entity_kinds)
                        .with(net_id, &mut net_ids)
                        .with(EntityTick::at(sim.current_tick), &mut entity_ticks)
                        .with(
                            Player::new(Vector3::new(1., 0., 0.), Controller::Remote(slot)),
                            &mut players,
                        )
                        .with(
                            Physical::new(
                                Point3::new(0., 10., 0.),
                                Quaternion::one(),
                                Shape::Cuboid(Vector3::new(0.5, 1., 0.5)),
                            ),
                            &mut physicals,
                        )
                        .build();
                    nia.register_id(net_id, entity);
                    conn.player_entity = Some(entity);
                    log::debug!("sending initial brick ghosts");
                    for (net_id, brick) in (&net_ids, &bricks).join() {
                        log::trace!("sending net_id {:?} brick {:?}", net_id, brick);
                        conn.add_brick_ghost(*net_id, &*brick);
                    }
                    for (net_id, gb) in (&net_ids, &ghost_bricks).join() {
                        log::trace!("sending net_id {:?} ghost brick {:?}", net_id, gb);
                        conn.add_ghost_brick_ghost(*net_id, &*gb);
                    }
                }
                ConnectionEvent::LostGhost { net_id, tag, .. } => {
                    if let Some(entity) = nia.get_entity(net_id) {
                        match tag {
                            GhostTag::None => {
                                // ok this really shouldn't happen
                                //
                                // it would mean that an entity's deletion got lost but then it
                                // also still exists on the server, so it was never deleted, so
                                // this packet never would've been sent
                                unreachable!()
                            }
                            GhostTag::Brick { brick_version } => {
                                if let Some(brick) = bricks.get(entity) {
                                    log::trace!(
                                        "ghost lost for brick {:?} version {}",
                                        net_id,
                                        brick_version,
                                    );
                                    if brick.version == brick_version {
                                        conn.add_brick_ghost(net_id, &brick);
                                    }
                                }
                            }
                            GhostTag::Player {
                                player_version,
                                physical_version,
                            } => {
                                if let (Some(player), Some(physical)) = (players.get(entity), physicals.get(entity)) {
                                    log::trace!(
                                        "ghost lost for player {:?} player_version={} physical_version={}",
                                        net_id,
                                        player_version,
                                        physical_version,
                                    );
                                    conn.update_player_ghost(net_id, player_version, &player, physical_version, &physical);
                                }
                            }
                            GhostTag::GhostBrick {
                                ghost_brick_version,
                            } => {
                                if let Some(gb) = ghost_bricks.get(entity) {
                                    log::trace!(
                                        "ghost lost for ghost brick {:?} version {}",
                                        net_id,
                                        ghost_brick_version,
                                    );
                                    if gb.version == ghost_brick_version {
                                        conn.add_ghost_brick_ghost(net_id, &gb);
                                    }
                                }
                            }
                        }
                    } else {
                        conn.add_ghost_delete(net_id);
                    }
                }
                _ => (),
            })
            .unwrap();
            for (player,) in (&mut players,).join() {
                if let Controller::Remote(slot_id) = player.controller {
                    if let Some(conn) = &mut connection_manager.slots[slot_id.0] {
                        if let Some((controls, facing, player_crc)) =
                            conn.controls_for_tick(sim.current_tick.0)
                        {
                            log::trace!("player entity of slot {:?} controls info", slot_id);
                            log::trace!("  controls={:?}", controls);
                            log::trace!("  facing={:?}", facing);
                            log::trace!("  crc={:?}", player_crc);
                            player.controls.submit(
                                sim.current_tick.0,
                                player_crc,
                                controls,
                                facing,
                            );
                        } else {
                            log::warn!("not enough controls for player {:?}, going through history to find the last one", slot_id);
                            for offset in 0..SERVER_CONTROLS_QUEUE_SIZE as u32 {
                                if offset > sim.current_tick.0 {
                                    break;
                                }
                                if let Some((controls, facing, player_crc)) =
                                    conn.controls_for_tick(sim.current_tick.0 - offset)
                                {
                                    log::trace!(
                                        "player entity of slot {:?} controls info",
                                        slot_id
                                    );
                                    log::trace!("  controls={:?}", controls);
                                    log::trace!("  facing={:?}", facing);
                                    log::trace!("  crc={:?}", player_crc);
                                    player.controls.submit(
                                        sim.current_tick.0,
                                        player_crc,
                                        controls,
                                        facing,
                                    );
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

pub struct ServerSyncEndSystem {
    bricks_reader_id: ReaderId<ComponentEvent>,
    bricks_added: BitSet,
    bricks_modified: BitSet,
    bricks_removed: BitSet,
    ghost_bricks_reader_id: ReaderId<ComponentEvent>,
    ghost_bricks_added: BitSet,
    ghost_bricks_modified: BitSet,
    ghost_bricks_removed: BitSet,
    physicals_reader_id: ReaderId<ComponentEvent>,
    physicals_added: BitSet,
    physicals_modified: BitSet,
    physicals_removed: BitSet,
}

impl ServerSyncEndSystem {
    pub fn new(world: &World) -> ServerSyncEndSystem {
        let mut bricks = world.write_storage::<Brick>();
        let bricks_reader_id = bricks.channel_mut().register_reader();
        let mut ghost_bricks = world.write_storage::<GhostBrick>();
        let ghost_bricks_reader_id = ghost_bricks.channel_mut().register_reader();
        let mut physicals = world.write_storage::<Physical>();
        let physicals_reader_id = physicals.channel_mut().register_reader();
        ServerSyncEndSystem {
            bricks_reader_id,
            bricks_added: BitSet::new(),
            bricks_modified: BitSet::new(),
            bricks_removed: BitSet::new(),
            ghost_bricks_reader_id,
            ghost_bricks_added: BitSet::new(),
            ghost_bricks_modified: BitSet::new(),
            ghost_bricks_removed: BitSet::new(),
            physicals_reader_id,
            physicals_added: BitSet::new(),
            physicals_modified: BitSet::new(),
            physicals_removed: BitSet::new(),
        }
    }
}

impl<'a> System<'a> for ServerSyncEndSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, Sim>,
        ReadExpect<'a, NetIdAllocator>,
        WriteExpect<'a, Network>,
        ReadStorage<'a, NetId>,
        ReadStorage<'a, Player>,
        ReadStorage<'a, Physical>,
        ReadStorage<'a, Brick>,
        ReadStorage<'a, GhostBrick>,
    );

    fn run(
        &mut self,
        (entities, sim, nia, mut net, net_ids, players, physicals, bricks, ghost_bricks): Self::SystemData,
    ) {
        if let Network::Server(connection_manager) = &mut *net {
            if sim.repeat {
                panic!("the server should not be doing any rewinding");
            }
            for (player, physical) in (&players, &physicals).join() {
                if let Controller::Remote(slot_id) = player.controller {
                    if let Some(conn) = &mut connection_manager.slots[slot_id.0] {
                        if let Some((_, _, client_player_crc)) =
                            conn.controls_for_tick(sim.current_tick.0)
                        {
                            let mut buf = [0; 128];
                            let pos = physical.pos;
                            let vel = physical.vel;
                            let tool = player.tool;
                            let ghost_info = if let Some(gb_entity) = player.ghost_brick {
                                if let Some(gb) = ghost_bricks.get(gb_entity) {
                                    Some(FullGhostBrickInfo {
                                        data_id: gb.data_id,
                                        color_id: gb.color_id,
                                        pos: gb.pos,
                                        orientation: gb.orientation,
                                    })
                                } else {
                                    log::error!("the player has a ghost brick entity assigned, but that entity does not exist");
                                    None
                                }
                            } else {
                                None
                            };
                            let player_info = FullPlayerInfo {
                                pos,
                                vel,
                                tool,
                                selected_color_id: player.selected_color_id,
                                ghost_info,
                            };
                            pigeon::Writer::with(&mut buf[..], |f| f.write(&player_info)).unwrap();
                            let server_player_crc = crc16::checksum_x25(&buf[..]);
                            if server_player_crc != client_player_crc {
                                log::debug!(
                                    "client and server crc don't match: {} != {}",
                                    server_player_crc,
                                    client_player_crc
                                );
                                conn.add_player_update(sim.current_tick.0, &player_info);
                            }
                        }
                    }
                }
            }
            self.physicals_added.clear();
            self.physicals_modified.clear();
            self.physicals_removed.clear();
            for &evt in physicals.channel().read(&mut self.physicals_reader_id) {
                match evt {
                    ComponentEvent::Inserted(idx) => {
                        self.physicals_added.add(idx);
                    }
                    ComponentEvent::Modified(idx) => {
                        self.physicals_modified.add(idx);
                    }
                    ComponentEvent::Removed(idx) => {
                        self.physicals_removed.add(idx);
                    }
                }
            }
            for (net_id, physical, player, _) in
                (&net_ids, &physicals, &players, &self.physicals_added).join()
            {
                log::debug!("creating player with net_id {:?}", net_id);
                for conn_slot in &mut connection_manager.slots {
                    if let Some(conn) = conn_slot {
                        if player.controller.is_remote_slot(conn.slot) {
                            continue;
                        }
                        conn.update_player_ghost(
                            *net_id,
                            player.version,
                            &*player,
                            physical.version,
                            &*physical,
                        );
                    }
                }
            }
            for (net_id, physical, player, _) in
                (&net_ids, &physicals, &players, &self.physicals_modified).join()
            {
                log::debug!("updating player with net_id {:?}", net_id);
                for conn_slot in &mut connection_manager.slots {
                    if let Some(conn) = conn_slot {
                        if player.controller.is_remote_slot(conn.slot) {
                            continue;
                        }
                        conn.update_player_ghost(
                            *net_id,
                            player.version,
                            &*player,
                            physical.version,
                            &*physical,
                        );
                    }
                }
            }
            // TODO: may want to just check for net_id removals
            for (index,) in (&self.physicals_removed,).join() {
                if let Some(net_id) = nia.get_net_id_from_index(index) {
                    log::debug!("removing physical with net_id {:?}", net_id);
                    for conn_slot in &mut connection_manager.slots {
                        if let Some(conn) = conn_slot {
                            conn.add_ghost_delete(net_id);
                        }
                    }
                }
            }
            self.ghost_bricks_added.clear();
            self.ghost_bricks_modified.clear();
            self.ghost_bricks_removed.clear();
            for &evt in ghost_bricks
                .channel()
                .read(&mut self.ghost_bricks_reader_id)
            {
                match evt {
                    ComponentEvent::Inserted(idx) => {
                        self.ghost_bricks_added.add(idx);
                    }
                    ComponentEvent::Modified(idx) => {
                        self.ghost_bricks_modified.add(idx);
                    }
                    ComponentEvent::Removed(idx) => {
                        self.ghost_bricks_removed.add(idx);
                    }
                }
            }
            for (gb_entity, net_id, gb, _) in
                (&entities, &net_ids, &ghost_bricks, &self.ghost_bricks_added).join()
            {
                log::debug!("creating ghost brick with net_id {:?}", net_id);
                'outer_a: for conn_slot in &mut connection_manager.slots {
                    if let Some(conn) = conn_slot {
                        if let Some(player_entity) = gb.player {
                            if let Some(player) = players.get(player_entity) {
                                if player.ghost_brick == Some(gb_entity) {
                                    continue 'outer_a;
                                }
                            }
                        }
                        conn.add_ghost_brick_ghost(*net_id, &*gb);
                        conn.add_ghost_brick_ghost(*net_id, &*gb);
                    }
                }
            }
            for (gb_entity, net_id, gb, _) in (
                &entities,
                &net_ids,
                &ghost_bricks,
                &self.ghost_bricks_modified,
            )
                .join()
            {
                log::debug!("updating ghost brick with net_id {:?}", net_id);
                'outer_b: for conn_slot in &mut connection_manager.slots {
                    if let Some(conn) = conn_slot {
                        if let Some(player_entity) = gb.player {
                            if let Some(player) = players.get(player_entity) {
                                if player.ghost_brick == Some(gb_entity) {
                                    continue 'outer_b;
                                }
                            }
                        }
                        conn.add_ghost_brick_ghost(*net_id, &*gb);
                    }
                }
            }
            // TODO: may want to just check for net_id removals
            for (index,) in (&self.ghost_bricks_removed,).join() {
                if let Some(net_id) = nia.get_net_id_from_index(index) {
                    log::debug!("removing ghost brick with net_id {:?}", net_id);
                    for conn_slot in &mut connection_manager.slots {
                        if let Some(conn) = conn_slot {
                            conn.add_ghost_delete(net_id);
                        }
                    }
                }
            }
            self.bricks_added.clear();
            self.bricks_modified.clear();
            self.bricks_removed.clear();
            for &evt in bricks.channel().read(&mut self.bricks_reader_id) {
                match evt {
                    ComponentEvent::Inserted(idx) => {
                        self.bricks_added.add(idx);
                    }
                    ComponentEvent::Modified(idx) => {
                        self.bricks_modified.add(idx);
                    }
                    ComponentEvent::Removed(idx) => {
                        self.bricks_removed.add(idx);
                    }
                }
            }
            for (net_id, brick, _) in (&net_ids, &bricks, &self.bricks_added).join() {
                log::debug!("creating brick with net_id {:?}", net_id);
                for conn_slot in &mut connection_manager.slots {
                    if let Some(conn) = conn_slot {
                        conn.add_brick_ghost(*net_id, &*brick);
                    }
                }
            }
            for (net_id, brick, _) in (&net_ids, &bricks, &self.bricks_modified).join() {
                log::debug!("updating brick with net_id {:?}", net_id);
                for conn_slot in &mut connection_manager.slots {
                    if let Some(conn) = conn_slot {
                        conn.add_brick_ghost(*net_id, &*brick);
                    }
                }
            }
            // TODO: may want to just check for net_id removals
            for (index,) in (&self.bricks_removed,).join() {
                if let Some(net_id) = nia.get_net_id_from_index(index) {
                    log::debug!("removing brick with net_id {:?}", net_id);
                    for conn_slot in &mut connection_manager.slots {
                        if let Some(conn) = conn_slot {
                            conn.add_ghost_delete(net_id);
                        }
                    }
                }
            }
            connection_manager.flush_frames().unwrap();
        }
    }
}
