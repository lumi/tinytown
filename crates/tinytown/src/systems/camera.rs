use specs::prelude::*;

use crate::{
    camera::{Camera, CameraMode},
    components::{Physical, Player},
    resources::Sim,
};

pub struct CameraSystem;

impl<'a> System<'a> for CameraSystem {
    type SystemData = (
        ReadExpect<'a, Sim>,
        ReadStorage<'a, Player>,
        ReadStorage<'a, Physical>,
        WriteExpect<'a, Camera>,
        ReadExpect<'a, CameraMode>,
    );

    fn run(&mut self, (sim, players, physicals, mut camera, camera_mode): Self::SystemData) {
        if sim.repeat {
            return;
        }
        match *camera_mode {
            CameraMode::None => {}
            CameraMode::FirstPerson { entity } => {
                if let (Some(player), Some(physical)) = (players.get(entity), physicals.get(entity))
                {
                    camera.eye = physical.pos + player.eye_offset();
                    camera.dir = player.facing;
                } else {
                    log::error!("camera can't be set as the camera entity does not have a `Player` and `Physical` component!");
                }
            }
            CameraMode::ThirdPerson { entity } => {
                if let (Some(player), Some(physical)) = (players.get(entity), physicals.get(entity))
                {
                    let offset = -player.facing * 10.;
                    camera.eye = physical.pos + player.eye_offset() + offset;
                    camera.dir = player.facing;
                } else {
                    log::error!("camera can't be set as the camera entity does not have a `Player` and `Physical` component!");
                }
            }
        }
    }
}
