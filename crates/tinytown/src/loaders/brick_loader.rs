use crate::{asset::AssetRef, brick::BrickData};

use {
    serde::{Deserialize, Serialize},
    std::io,
    thiserror::Error,
    toml,
};

#[derive(Serialize, Deserialize)]
struct BrickDefinitionFile {
    brick: BrickDefinition,
    model: Option<BrickModelDefinition>,
}

#[derive(Serialize, Deserialize)]
struct BrickDefinition {
    name: String,
    size: (u32, u32, u32),
    #[serde(default)]
    flat: bool,
}

#[derive(Serialize, Deserialize)]
struct BrickModelDefinition {
    name: String,
}

#[derive(Debug, Error)]
pub enum BrickLoaderError {
    #[error("IO error: {0:}")]
    IoError(#[from] io::Error),

    #[error("TOML error: {0:}")]
    TomlError(#[from] toml::de::Error),
}

pub fn load(asset_ref: AssetRef) -> Result<BrickData, BrickLoaderError> {
    let bdf: BrickDefinitionFile = toml::from_slice(asset_ref.data())?;
    if bdf.model.is_some() {
        panic!("Brick models are currently unsupported!");
    }
    Ok(BrickData {
        name: bdf.brick.name,
        size: bdf.brick.size.into(),
        flat: bdf.brick.flat,
    })
}
