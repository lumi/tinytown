use crate::asset::AssetRef;

use regenboog::RgbaU8;

pub fn load(asset_ref: AssetRef) -> anyhow::Result<Vec<RgbaU8>> {
    log::debug!("loading color palette {}", asset_ref.name());
    let img_raw = asset_ref.load_as_image()?;
    let img = img_raw.into_rgba8();
    let (w, h) = img.dimensions();
    let mut palette = Vec::new();
    for iy in 0..h {
        for ix in 0..w {
            let pixel = img.get_pixel(ix, iy);
            let color = pixel.0.into();
            palette.push(color);
        }
    }
    Ok(palette)
}
