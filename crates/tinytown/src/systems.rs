mod autosave;
mod brick_spawner;
mod building;
mod camera;
mod camera_control;
mod client_sync;
mod collision;
mod controls;
mod entity_tick;
mod movement;
mod net_id_maintainer;
mod physics;
mod server_sync;
mod ui;

pub use self::{
    autosave::AutosaveSystem,
    brick_spawner::BrickSpawnerSystem,
    building::BuildingSystem,
    camera::CameraSystem,
    camera_control::CameraControlSystem,
    client_sync::{ClientSyncEndSystem, ClientSyncStartSystem},
    collision::{CollisionIndexSystem, CollisionSystem},
    controls::ControlsSystem,
    entity_tick::EntityTickSystem,
    movement::MovementSystem,
    net_id_maintainer::NetIdMaintainerSystem,
    physics::PhysicsSystem,
    server_sync::{ServerSyncEndSystem, ServerSyncStartSystem},
    ui::UiSystem,
};
