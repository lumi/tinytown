use {
    cgmath::Vector2,
    pigeon::{Pack, Unpack},
    serde::{Deserialize, Serialize},
    std::collections::HashSet,
};

use crate::config::ControlConfig;

/// Whether a key or mouse button has been pressed or released.
#[derive(Clone, Copy, Debug, PartialEq, PartialOrd, Eq, Ord, Hash)]
pub enum Action {
    /// The control has been pressed.
    Press,
    /// The control has been released.
    Release,
}

/// A mouse button
#[derive(Clone, Copy, Debug, PartialEq, PartialOrd, Eq, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub enum MouseButton {
    /// The left mouse button
    Left,
    /// The middle mouse button
    Middle,
    /// The right mouse button
    Right,
}

/// A key on the keyboard
#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Eq, Ord, Hash, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub enum Key {
    Escape,
    Space,
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
    Tilde,
    #[serde(rename = "0")]
    Top0,
    #[serde(rename = "1")]
    Top1,
    #[serde(rename = "2")]
    Top2,
    #[serde(rename = "3")]
    Top3,
    #[serde(rename = "4")]
    Top4,
    #[serde(rename = "5")]
    Top5,
    #[serde(rename = "6")]
    Top6,
    #[serde(rename = "7")]
    Top7,
    #[serde(rename = "8")]
    Top8,
    #[serde(rename = "9")]
    Top9,
    Minus,
    Plus,
    Backspace,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
    Num0,
    Num1,
    Num2,
    Num3,
    Num4,
    Num5,
    Num6,
    Num7,
    Num8,
    Num9,
    Enter,
    NumpadEnter,
    LeftShift,
    RightShift,
    Semicolon,
    LeftBracket,
    RightBracket,
    Tab,
    Quote,
    LeftAlt,
    RightAlt,
    LeftCtrl,
    RightCtrl,
    NumLock,
    ScrollLock,
    NumpadPlus,
    NumpadMinus,
    NumpadStar,
    NumpadDiv,
    NumpadPeriod,
    Comma,
    Period,
    Slash,
    Backslash,
    CapsLock,
}

/// All controls
#[derive(Clone, Copy, Debug, PartialEq, PartialOrd, Eq, Ord, Hash, Serialize, Deserialize)]
#[repr(u8)]
#[serde(rename_all = "kebab-case")]
pub enum Control {
    /// Move forward
    MoveForward = 1,
    /// Move backward
    MoveBackward = 2,
    /// Move left
    MoveLeft = 3,
    /// Move right
    MoveRight = 4,
    /// Move the ghost brick forward
    MoveGhostForward = 5,
    /// Move the ghost brick backward
    MoveGhostBackward = 6,
    /// Move the ghost brick left
    MoveGhostLeft = 7,
    /// Move the ghost brick right
    MoveGhostRight = 8,
    /// Move the ghost brick up
    MoveGhostUp = 9,
    /// Move the ghost brick down
    MoveGhostDown = 10,
    /// Enable/disable proportional movement on the ghost brick
    MoveGhostProportional = 11,
    /// Rotate the ghost brick clockwise
    RotateGhostCw = 12,
    /// Rotate the ghost brick counter-clockwise
    RotateGhostCcw = 13,
    /// Cycle the ghost brick to the previous brick type
    CycleGhostPrev = 14,
    /// Cycle the ghost brick to the next brick type
    CycleGhostNext = 15,
    /// Cycle the ghost brick to the previous color in the palette
    PalettePrev = 16,
    /// Cycle the ghost brick to the next color in the palette
    PaletteNext = 17,
    /// Remove the ghost brick
    DispelGhost = 18,
    /// Jump
    Jump = 19,
    /// Enable/disable crawling
    Crawl = 20,
    /// Build the ghost brick
    Build = 21,
    /// Interact with whatever is in front of the player
    Interact = 22,
    /// Use the jetpack
    Jetpack = 23,
    /// Cycle to the previous tool
    PrevTool = 24,
    /// Cycle to the next tool
    NextTool = 25,
    /// Switch between first-person and third-person cameras
    SwitchCamera = 26,
}

impl Control {
    /// Turn a `u8` into a `Control`.
    pub fn from_u8(control: u8) -> Option<Control> {
        match control {
            1 => Some(Control::MoveForward),
            2 => Some(Control::MoveBackward),
            3 => Some(Control::MoveLeft),
            4 => Some(Control::MoveRight),
            5 => Some(Control::MoveGhostForward),
            6 => Some(Control::MoveGhostBackward),
            7 => Some(Control::MoveGhostLeft),
            8 => Some(Control::MoveGhostRight),
            9 => Some(Control::MoveGhostUp),
            10 => Some(Control::MoveGhostDown),
            11 => Some(Control::MoveGhostProportional),
            12 => Some(Control::RotateGhostCw),
            13 => Some(Control::RotateGhostCcw),
            14 => Some(Control::CycleGhostPrev),
            15 => Some(Control::CycleGhostNext),
            16 => Some(Control::PalettePrev),
            17 => Some(Control::PaletteNext),
            18 => Some(Control::DispelGhost),
            19 => Some(Control::Jump),
            20 => Some(Control::Crawl),
            21 => Some(Control::Build),
            22 => Some(Control::Interact),
            23 => Some(Control::Jetpack),
            24 => Some(Control::PrevTool),
            25 => Some(Control::NextTool),
            26 => Some(Control::SwitchCamera),
            _ => None,
        }
    }
}

/// Keeps track of what controls are activated and when controls are activated or deactivated.
#[derive(Debug, Clone, Default)]
pub struct ControlState {
    pub pressed: u32,
    pub events: Vec<(Action, Control)>,
}

impl Pack for ControlState {
    fn pack<T: pigeon::Target>(&self, writer: &mut pigeon::Writer<T>) -> pigeon::WriteResult<()> {
        writer.write(self.pressed)?;
        writer.write(pigeon::U5(self.events.len() as u8))?;
        for (action, control) in &self.events {
            let action_bit = match action {
                Action::Press => true,
                Action::Release => false,
            };
            writer.write(action_bit)?;
            writer.write(pigeon::U6(*control as u8))?;
        }
        Ok(())
    }
}

impl<'a> Unpack<'a> for ControlState {
    fn unpack(reader: &mut pigeon::Reader<'a>) -> pigeon::ReadResult<ControlState> {
        let pressed = reader.read()?;
        let pigeon::U5(len) = reader.read()?;
        let mut events = Vec::with_capacity(len as usize);
        for _ in 0..len {
            let down: bool = reader.read()?;
            let pigeon::U6(evt) = reader.read()?;
            let action = if down { Action::Press } else { Action::Release };
            let control = Control::from_u8(evt).ok_or_else(|| pigeon::ReadError::UnexpectedData)?;
            events.push((action, control));
        }
        Ok(ControlState { pressed, events })
    }
}

impl ControlState {
    /// Handle a control event.
    pub fn handle(&mut self, action: Action, control: Control) {
        self.events.push((action, control));
        let bit_offset = control as u8 - 1;
        match action {
            Action::Press => {
                self.pressed = self.pressed | (1 << bit_offset);
            }
            Action::Release => {
                self.pressed = self.pressed & !(1 << bit_offset);
            }
        }
    }

    /// Check whether a control is currently activated.
    pub fn activated(&self, control: Control) -> bool {
        let bit_offset = control as u8 - 1;
        self.pressed & (1 << bit_offset) != 0
    }
}

/// Manages the controls
#[derive(Debug)]
pub struct ControlsManager {
    mouse_pressed: HashSet<MouseButton>,
    cursor_entered: bool,
    cursor_pos: Option<Vector2<f32>>,
    cursor_delta: Vector2<f32>,
    last_cursor_pos: Option<Vector2<f32>>,
    exit_requested: bool,
    /// The current control state
    pub state: ControlState,
    /// The current control configuration
    pub config: ControlConfig,
}

impl ControlsManager {
    /// Create a new controls manager.
    pub fn new() -> ControlsManager {
        ControlsManager {
            mouse_pressed: HashSet::new(),
            cursor_entered: false,
            cursor_pos: None,
            cursor_delta: Vector2::new(0., 0.),
            last_cursor_pos: None,
            exit_requested: false,
            state: ControlState::default(),
            config: ControlConfig::default(),
        }
    }

    /// Handle a key event.
    pub fn handle_key_event(&mut self, action: Action, key: Key) {
        if let Some(control) = self.config.keyboard.get(&key) {
            self.state.handle(action, *control);
            match action {
                Action::Press => {
                    log::debug!("control {:?} activated", control);
                }
                Action::Release => {
                    log::debug!("control {:?} deactivated", control);
                }
            }
        }
    }

    /// Handle a mouse event.
    pub fn handle_mouse_event(&mut self, action: Action, button: MouseButton) {
        match action {
            Action::Press => {
                self.mouse_pressed.insert(button);
            }
            Action::Release => {
                self.mouse_pressed.remove(&button);
            }
        }
        if let Some(control) = self.config.mouse.get(&button) {
            self.state.handle(action, *control);
            match action {
                Action::Press => {
                    log::debug!("control {:?} activated", control);
                }
                Action::Release => {
                    log::debug!("control {:?} deactivated", control);
                }
            }
        }
    }

    /// Reset the control state for a new frame.
    ///
    /// This will reset the event queue and the mouse cursor position.
    pub fn reset(&mut self) {
        self.state.events.clear();
        self.last_cursor_pos = self.cursor_pos;
        self.cursor_delta = Vector2::new(0., 0.);
    }

    /// Handle a cursor position change.
    pub fn handle_cursor_pos(&mut self, pos: Vector2<f32>) {
        self.cursor_pos = Some(pos);
    }

    /// Handle a relative cursor position change.
    pub fn handle_cursor_delta(&mut self, delta: Vector2<f32>) {
        self.cursor_delta = delta;
    }

    /// Handle the cursor entering and leaving the window.
    pub fn handle_cursor_enter(&mut self, entered: bool) {
        self.cursor_entered = entered;
        if !entered {
            self.cursor_pos = None;
        }
    }

    /// Get the position of the cursor.
    pub fn cursor_pos(&self) -> Option<Vector2<f32>> {
        self.cursor_pos
    }

    /// Get the previous cursor position.
    pub fn last_cursor_pos(&self) -> Option<Vector2<f32>> {
        self.last_cursor_pos
    }

    /// Get the cursor delta for this frame
    pub fn cursor_delta(&self) -> Vector2<f32> {
        self.cursor_delta
    }

    /// Request the game to exit.
    pub fn request_exit(&mut self) {
        self.exit_requested = true;
    }

    /// Has an exit been requested?
    pub fn exit_requested(&self) -> bool {
        self.exit_requested
    }
}
