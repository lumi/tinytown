use {
    cgmath::{prelude::*, Point2, Vector2},
    regenboog::RgbaU8,
    slotmap::DenseSlotMap,
    std::collections::VecDeque,
};

use crate::{
    texture_manager::{TextureId, TextureManagerProxy},
    tool::Tool,
};

mod border;
mod gravity;
mod rect;
mod widget;

pub use {
    border::Border,
    gravity::Gravity,
    rect::Rect,
    widget::{Widget, WidgetInner, WidgetVisual},
};

slotmap::new_key_type! {
    pub struct WidgetId;
}

pub struct GameUi {
    pub tool_indicator_root: WidgetId,
    pub tool_indicator_button: WidgetId,
    pub tool_indicator_view: WidgetId,
    pub crosshair_root: WidgetId,
    pub crosshair: WidgetId,
    pub palette_root: WidgetId,
    pub palette_view: WidgetId,
    pub palette_current: WidgetId,
    pub desync_indicator_root: WidgetId,
    pub desync_indicator: WidgetId,
}

impl GameUi {
    pub fn new(ui: &mut Ui) -> GameUi {
        let tool_indicator_root = ui.create_root(
            Rect::with_size(Point2::new(10., 10.), Vector2::new(134., 134.)),
            Gravity::top_left(),
        );
        let tool_indicator_button = ui.create_widget(tool_indicator_root, Widget::button());
        let tool_indicator_view =
            ui.create_widget(tool_indicator_button, Widget::tool_view(Tool::None));
        let crosshair_root = ui.create_root(
            Rect::with_center(ui.screen_rect().center(), Vector2::new(32., 32.)),
            Gravity::center(),
        );
        let crosshair = ui.create_widget(crosshair_root, Widget::crosshair());
        let palette_root = ui.create_root(
            Rect::with_size(Point2::new(10., 154.), Vector2::new(38., 38.)),
            Gravity::top_left(),
        );
        let palette_view = ui.create_widget(palette_root, Widget::column());
        let palette_current =
            ui.create_widget(palette_view, Widget::color_view([255, 255, 255].into()));
        let desync_indicator_root = ui.create_root(
            Rect::with_size(Point2::new(58., 154.), Vector2::new(38., 38.)),
            Gravity::top_left(),
        );
        let desync_indicator = ui.create_widget(
            desync_indicator_root,
            Widget::color_view([255, 255, 255].into()),
        );
        GameUi {
            tool_indicator_root,
            tool_indicator_button,
            tool_indicator_view,
            crosshair_root,
            crosshair,
            palette_root,
            palette_view,
            palette_current,
            desync_indicator_root,
            desync_indicator,
        }
    }

    pub fn set_current_tool(&mut self, ui: &mut Ui, tool: Tool) {
        let tool_indicator_view = ui.get_widget(self.tool_indicator_view);
        tool_indicator_view.inner = WidgetInner::ToolView(tool);
    }

    pub fn set_current_color(&mut self, ui: &mut Ui, color: RgbaU8) {
        let palette_current = ui.get_widget(self.palette_current);
        palette_current.inner = WidgetInner::ColorView(color);
    }

    pub fn set_desync_indicator(&mut self, ui: &mut Ui, desync: bool) {
        let desync_indicator = ui.get_widget(self.desync_indicator);
        let color = if desync {
            [255, 0, 0].into()
        } else {
            [255, 255, 255].into()
        };
        desync_indicator.inner = WidgetInner::ColorView(color);
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct SegmentTexture {
    pub src: Rect,
    pub border: Border,
    pub stretch: bool,
}

#[derive(Debug, Clone)]
pub struct Ui {
    screen_rect: Rect,
    roots: Vec<(Rect, Gravity, WidgetId)>,
    widgets: DenseSlotMap<WidgetId, Widget>,
    atlas: TextureId,
    segtex: Vec<SegmentTexture>,
}

impl Ui {
    pub fn new(proxy: &TextureManagerProxy, initial_window_size: Vector2<u32>) -> Ui {
        let widgets = DenseSlotMap::with_key();
        let atlas = proxy.load("textures/ui_atlas_big.png");
        let mut segtex = Vec::new();
        // 0
        segtex.push(SegmentTexture {
            src: Rect::with_size(Point2::new(0., 0.), Vector2::new(32., 32.)),
            border: Border::all(3.),
            stretch: false,
        });
        // 1
        segtex.push(SegmentTexture {
            src: Rect::with_size(Point2::new(0., 32.), Vector2::new(32., 32.)),
            border: Border::all(0.),
            stretch: true,
        });
        // 2
        segtex.push(SegmentTexture {
            src: Rect::with_size(Point2::new(0., 64.), Vector2::new(32., 32.)),
            border: Border::all(0.),
            stretch: true,
        });
        // 3
        segtex.push(SegmentTexture {
            src: Rect::with_size(Point2::new(0., 96.), Vector2::new(32., 32.)),
            border: Border::all(0.),
            stretch: true,
        });
        // 4
        segtex.push(SegmentTexture {
            src: Rect::with_size(Point2::new(32., 0.), Vector2::new(32., 32.)),
            border: Border::all(0.),
            stretch: true,
        });
        // 5
        segtex.push(SegmentTexture {
            src: Rect::with_size(Point2::new(32., 32.), Vector2::new(32., 32.)),
            border: Border::all(0.),
            stretch: true,
        });
        // 6
        segtex.push(SegmentTexture {
            src: Rect::with_size(Point2::new(32., 64.), Vector2::new(32., 32.)),
            border: Border::all(0.),
            stretch: true,
        });
        // 7
        segtex.push(SegmentTexture {
            src: Rect::with_size(Point2::new(128., 0.), Vector2::new(128., 128.)),
            border: Border::all(0.),
            stretch: true,
        });
        // 8
        segtex.push(SegmentTexture {
            src: Rect::with_size(Point2::new(128., 128.), Vector2::new(128., 128.)),
            border: Border::all(0.),
            stretch: true,
        });
        // 9
        segtex.push(SegmentTexture {
            src: Rect::with_size(Point2::new(256., 0.), Vector2::new(128., 128.)),
            border: Border::all(0.),
            stretch: true,
        });
        let roots = Vec::new();
        Ui {
            screen_rect: Rect::with_size(
                Point2::origin(),
                initial_window_size.cast::<f32>().unwrap(),
            ),
            roots,
            widgets,
            atlas,
            segtex,
        }
    }

    pub fn screen_rect(&self) -> Rect {
        self.screen_rect
    }

    pub fn create_root(&mut self, rect: Rect, gravity: Gravity) -> WidgetId {
        let widget = Widget::root();
        let widget_id = self.widgets.insert(widget);
        self.roots.push((rect, gravity, widget_id));
        widget_id
    }

    pub fn create_widget(&mut self, parent_id: WidgetId, mut widget: Widget) -> WidgetId {
        widget.parent_id = Some(parent_id);
        let widget_id = self.widgets.insert(widget);
        let parent = self
            .widgets
            .get_mut(parent_id)
            .expect("creating widget with an invalid parent?");
        parent.children.push((Point2::origin(), widget_id));
        widget_id
    }

    pub fn get_widget(&mut self, widget_id: WidgetId) -> &mut Widget {
        &mut self.widgets[widget_id]
    }

    pub fn atlas(&self) -> TextureId {
        self.atlas
    }

    pub fn layout(&mut self, window_size: Vector2<u32>) {
        let old_screen_rect = self.screen_rect;
        self.screen_rect = Rect::with_size(Point2::origin(), window_size.cast::<f32>().unwrap());
        if self.screen_rect != old_screen_rect {
            for &mut (ref mut rect, gravity, _root_id) in &mut self.roots {
                let gravity_point_old = gravity.point(old_screen_rect);
                let gravity_point_new = gravity.point(self.screen_rect);
                let diff = gravity_point_new - gravity_point_old;
                rect.min += diff * 0.5;
                rect.max += diff * 0.5;
            }
        }
        let mut stack = Vec::new();
        for &(rect, _, root_id) in &self.roots {
            let root = &mut self.widgets[root_id];
            root.visual = Some(WidgetVisual::new(rect));
            for &(_, child_id) in &root.children {
                // TODO: maybe they shouldn't all be stacked
                // TODO: maybe enforce only one child in a root?
                stack.push((child_id, rect.size()));
            }
        }
        while let Some((widget_id, bounds)) = stack.pop() {
            let widget = &mut self.widgets[widget_id];
            let rect = Rect::with_size(Point2::origin(), bounds);
            match widget.inner {
                WidgetInner::Column => {
                    widget.visual = Some(WidgetVisual::new(rect).with_segtex_id(0));
                    let children_rect = rect.inset(3.);
                    let children_size = children_rect.size();
                    let height_per_child = children_size.y / widget.children.len() as f32;
                    let child_size = Vector2::new(children_size.x, height_per_child);
                    let mut current_y = children_rect.min.y;
                    for &mut (ref mut child_pos, child_id) in &mut widget.children {
                        *child_pos = Point2::new(children_rect.min.x, current_y);
                        stack.push((child_id, child_size));
                        current_y += child_size.y;
                    }
                }
                WidgetInner::Button => {
                    widget.visual = Some(WidgetVisual::new(rect).with_segtex_id(0));
                    let child_rect = rect.inset(3.);
                    for &mut (ref mut child_pos, child_id) in &mut widget.children {
                        *child_pos = child_rect.min;
                        stack.push((child_id, child_rect.size()));
                    }
                }
                WidgetInner::ColorView(color) => {
                    widget.visual =
                        Some(WidgetVisual::new(rect).with_segtex_id(6).with_color(color));
                }
                WidgetInner::ToolView(tool) => {
                    let segtex_id = match tool {
                        Tool::None => None,
                        Tool::Brick => Some(7),
                        Tool::Hammer => Some(8),
                        Tool::PaintCan => Some(9),
                        Tool::GimpPaprika => Some(5),
                    };
                    widget.visual = Some(WidgetVisual::new(rect).with_segtex_id(segtex_id));
                }
                WidgetInner::Crosshair => {
                    widget.visual = Some(WidgetVisual::new(rect).with_segtex_id(4));
                }
                _ => {
                    // Uninteresting default
                    widget.visual = Some(WidgetVisual::new(rect));
                    for &(_, child_id) in &widget.children {
                        stack.push((child_id, rect.size()));
                    }
                }
            }
        }
    }

    pub fn render(&self, mut inner: impl FnMut(&SegmentTexture, RgbaU8, Rect)) {
        let mut queue = VecDeque::new();
        for &(_, _, root_id) in &self.roots {
            queue.push_front((Point2::origin(), root_id));
        }
        while let Some((offset, widget_id)) = queue.pop_back() {
            let widget = &self.widgets[widget_id];
            if let Some(vis) = &widget.visual {
                let abs_rect = Rect {
                    min: vis.rect.min + offset.to_vec(),
                    max: vis.rect.max + offset.to_vec(),
                };
                if let Some(segtex_id) = vis.segtex_id {
                    inner(&self.segtex[segtex_id], vis.color, abs_rect);
                }
                for &(child_pos, child_id) in &widget.children {
                    queue.push_front((abs_rect.min + child_pos.to_vec(), child_id));
                }
            }
        }
    }
}
