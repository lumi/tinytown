//! Exposes a common API for backends.

use specs::{Join, WorldExt};

use crate::{
    brick::{BrickDataStore, BrickPalette, FLAGS_FLAT},
    components::Brick,
    texture_manager::{TextureId, TextureManagerProxy},
    vox::VOXEL_SIZE,
};

mod null;

pub use null::NullBackend;

#[derive(Clone, Debug)]
pub struct BrickTextures {
    pub top: TextureId,
    pub side: TextureId,
    pub bottom: TextureId,
}

impl BrickTextures {
    pub fn new(proxy: &TextureManagerProxy) -> BrickTextures {
        let top = proxy.load("textures/brick_top.png");
        let side = proxy.load("textures/brick_top.png");
        let bottom = proxy.load("textures/brick_top.png");
        BrickTextures { top, side, bottom }
    }
}

pub trait BackendName {
    fn name() -> &'static str;
}

pub trait EventLoop {
    type Event;

    fn run(self, f: impl FnMut(Self::Event) -> anyhow::Result<bool> + 'static) -> !;
}

pub trait Backend {
    type EventLoop: EventLoop<Event = Self::Event>;
    type Event;

    fn create_event_loop(&mut self) -> anyhow::Result<Self::EventLoop>;

    fn aspect_ratio(&self) -> f32;
    fn size(&self) -> (u32, u32);
    fn texture_manager_proxy(&self) -> TextureManagerProxy;
    fn handle_event(&mut self, world: &specs::World, event: Self::Event) -> anyhow::Result<()>;
}

pub fn generate_brick_mesh<V, M>(
    transparent: bool,
    world: &specs::World,
    make_vertex: impl Fn([f32; 3], [f32; 4], [f32; 3], [f32; 2], [f32; 2], u32) -> V,
    mut make_mesh: impl FnMut(Vec<V>, Vec<u32>) -> M,
) -> M {
    let mut vertices = Vec::new();
    let mut indices: Vec<u32> = Vec::new();
    let palette = world.fetch::<BrickPalette>();
    let brick_data_store = world.fetch::<BrickDataStore>();
    for brick in world.read_storage::<Brick>().join() {
        let color: [f32; 4] = palette[brick.color_id].into();
        if color[3] < 1. && !transparent {
            continue;
        }
        if color[3] >= 1. && transparent {
            continue;
        }
        let data = &brick_data_store[brick.data_id];
        let bsw = brick.pos.world_min();
        let orient = brick.orientation.as_u8();
        let (bx, by, bz) = (bsw.x, bsw.y, bsw.z);
        let size = data.size;
        let (mut bw, bh, mut bd) = (size.x, size.y, size.z);
        if orient % 2 == 1 {
            let t = bw;
            bw = bd;
            bd = t;
        }
        let (w, h, d) = (
            bw as f32 * VOXEL_SIZE.x,
            bh as f32 * VOXEL_SIZE.y,
            bd as f32 * VOXEL_SIZE.z,
        );
        let (hw, hh, hd) = (w / 2., h / 2., d / 2.);
        let (bwf, bhf, bdf) = (bw as f32, bh as f32, bd as f32);
        let (x, y, z) = (bx + hw, by + hh, bz + hd);
        let c = &brick.covered;
        if !c.up() {
            let si = vertices.len() as u32;
            let normal = [0., 1., 0.];
            let flags = if data.flat { FLAGS_FLAT } else { 0 };
            vertices.push(make_vertex(
                [x - hw, y + hh, z - hd],
                color,
                normal,
                [bwf, bdf],
                [0., 0.],
                flags,
            ));
            vertices.push(make_vertex(
                [x + hw, y + hh, z - hd],
                color,
                normal,
                [bwf, bdf],
                [bwf, 0.],
                flags,
            ));
            vertices.push(make_vertex(
                [x + hw, y + hh, z + hd],
                color,
                normal,
                [bwf, bdf],
                [bwf, bdf],
                flags,
            ));
            vertices.push(make_vertex(
                [x - hw, y + hh, z + hd],
                color,
                normal,
                [bwf, bdf],
                [0., bdf],
                flags,
            ));
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        if !c.down() {
            let si = vertices.len() as u32;
            let normal = [0., -1., 0.];
            let flags = 0;
            vertices.push(make_vertex(
                [x - hw, y - hh, z - hd],
                color,
                normal,
                [bwf, bdf],
                [0., 0.],
                flags,
            ));
            vertices.push(make_vertex(
                [x - hw, y - hh, z + hd],
                color,
                normal,
                [bwf, bdf],
                [0., bdf],
                flags,
            ));
            vertices.push(make_vertex(
                [x + hw, y - hh, z + hd],
                color,
                normal,
                [bwf, bdf],
                [bwf, bdf],
                flags,
            ));
            vertices.push(make_vertex(
                [x + hw, y - hh, z - hd],
                color,
                normal,
                [bwf, bdf],
                [bwf, 0.],
                flags,
            ));
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        if !c.north() {
            let si = vertices.len() as u32;
            let normal = [0., 0., -1.];
            let flags = FLAGS_FLAT;
            vertices.push(make_vertex(
                [x - hw, y - hh, z - hd],
                color,
                normal,
                [bwf, bhf],
                [0., 0.],
                flags,
            ));
            vertices.push(make_vertex(
                [x + hw, y - hh, z - hd],
                color,
                normal,
                [bwf, bhf],
                [bwf, 0.],
                flags,
            ));
            vertices.push(make_vertex(
                [x + hw, y + hh, z - hd],
                color,
                normal,
                [bwf, bhf],
                [bwf, bhf],
                flags,
            ));
            vertices.push(make_vertex(
                [x - hw, y + hh, z - hd],
                color,
                normal,
                [bwf, bhf],
                [0., bhf],
                flags,
            ));
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        if !c.south() {
            let si = vertices.len() as u32;
            let normal = [0., 0., 1.];
            let flags = FLAGS_FLAT;
            vertices.push(make_vertex(
                [x - hw, y - hh, z + hd],
                color,
                normal,
                [bwf, bhf],
                [0., 0.],
                flags,
            ));
            vertices.push(make_vertex(
                [x - hw, y + hh, z + hd],
                color,
                normal,
                [bwf, bhf],
                [0., bhf],
                flags,
            ));
            vertices.push(make_vertex(
                [x + hw, y + hh, z + hd],
                color,
                normal,
                [bwf, bhf],
                [bwf, bhf],
                flags,
            ));
            vertices.push(make_vertex(
                [x + hw, y - hh, z + hd],
                color,
                normal,
                [bwf, bhf],
                [bwf, 0.],
                flags,
            ));
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        if !c.west() {
            let si = vertices.len() as u32;
            let normal = [-1., 0., 0.];
            let flags = FLAGS_FLAT;
            vertices.push(make_vertex(
                [x - hw, y - hh, z - hd],
                color,
                normal,
                [bhf, bdf],
                [0., 0.],
                flags,
            ));
            vertices.push(make_vertex(
                [x - hw, y + hh, z - hd],
                color,
                normal,
                [bhf, bdf],
                [bhf, 0.],
                flags,
            ));
            vertices.push(make_vertex(
                [x - hw, y + hh, z + hd],
                color,
                normal,
                [bhf, bdf],
                [bhf, bdf],
                flags,
            ));
            vertices.push(make_vertex(
                [x - hw, y - hh, z + hd],
                color,
                normal,
                [bhf, bdf],
                [0., bdf],
                flags,
            ));
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        if !c.east() {
            let si = vertices.len() as u32;
            let normal = [1., 0., 0.];
            let flags = FLAGS_FLAT;
            vertices.push(make_vertex(
                [x + hw, y - hh, z - hd],
                color,
                normal,
                [bhf, bdf],
                [0., 0.],
                flags,
            ));
            vertices.push(make_vertex(
                [x + hw, y - hh, z + hd],
                color,
                normal,
                [bhf, bdf],
                [0., bdf],
                flags,
            ));
            vertices.push(make_vertex(
                [x + hw, y + hh, z + hd],
                color,
                normal,
                [bhf, bdf],
                [bhf, bdf],
                flags,
            ));
            vertices.push(make_vertex(
                [x + hw, y + hh, z - hd],
                color,
                normal,
                [bhf, bdf],
                [bhf, 0.],
                flags,
            ));
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
    }
    make_mesh(vertices, indices)
}
