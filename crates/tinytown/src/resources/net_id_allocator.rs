use {
    dashmap::DashMap,
    specs::{prelude::*, world::Index},
    std::sync::atomic::{AtomicU32, Ordering},
};

use crate::components::NetId;

/// An allocator of network ids
#[derive(Debug)]
pub struct NetIdAllocator {
    last_id: AtomicU32,
    mapping: DashMap<NetId, Entity>,
    inverse_mapping: DashMap<Index, NetId>,
}

impl NetIdAllocator {
    /// Create a new network id allocator.
    pub fn new() -> NetIdAllocator {
        NetIdAllocator {
            last_id: AtomicU32::new(0),
            mapping: DashMap::new(),
            inverse_mapping: DashMap::new(),
        }
    }

    /// Get the next id from the allocator.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::{
    /// #         resources::NetIdAllocator,
    /// #         components::NetId,
    /// #     },
    /// # };
    /// let mut nia = NetIdAllocator::new();
    ///
    /// let net_id_a = nia.next_id();
    /// let net_id_b = nia.next_id();
    ///
    /// assert_ne!(net_id_a, net_id_b);
    /// ```
    pub fn next_id(&self) -> NetId {
        NetId(self.last_id.fetch_add(1, Ordering::SeqCst))
    }

    /// Register an entity with the allocator.
    pub fn register_id(&self, net_id: NetId, entity: Entity) {
        self.mapping.insert(net_id, entity);
        self.inverse_mapping.insert(entity.id(), net_id);
    }

    /// Unregister an entity from the allocator.
    pub fn unregister_id(&self, net_id: NetId) {
        if let Some((_, entity)) = self.mapping.remove(&net_id) {
            self.inverse_mapping.remove(&entity.id());
        }
    }

    /// Get the entity associated with a network id.
    pub fn get_entity(&self, net_id: NetId) -> Option<Entity> {
        self.mapping.get(&net_id).map(|e| *e)
    }

    /// Get the network id associated with an entity index.
    pub fn get_net_id_from_index(&self, index: Index) -> Option<NetId> {
        self.inverse_mapping.get(&index).map(|e| *e)
    }
}
