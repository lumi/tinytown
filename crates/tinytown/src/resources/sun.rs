use cgmath::Vector3;

#[derive(Debug, Clone)]
pub struct Sun {
    pub sun_dir: Vector3<f32>,
}

impl Sun {
    pub fn new(sun_dir: Vector3<f32>) -> Sun {
        Sun { sun_dir }
    }
}
