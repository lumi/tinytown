use crate::tick::Tick;

/// Simulation controls
#[derive(Debug, Clone)]
pub struct Sim {
    /// Current tick the simulation is on
    pub current_tick: Tick,
    /// Whether the simulation if paused
    pub paused: bool,
    /// Whether this is the first run of the dispatcher this tick
    pub initial: bool,
    /// Whether the simulation should be repeated
    pub repeat: bool,
    /// Whether the simulation is not in-sync
    pub desync: bool,
}
