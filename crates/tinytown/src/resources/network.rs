use crate::net::{client::ClientConnection, server::ConnectionManager};

pub enum Network {
    None,
    Client(ClientConnection),
    Server(ConnectionManager),
}

impl Network {
    pub fn is_none(&self) -> bool {
        if let Network::None = *self {
            true
        } else {
            false
        }
    }

    pub fn is_client(&self) -> bool {
        if let Network::Client(_) = *self {
            true
        } else {
            false
        }
    }

    pub fn is_server(&self) -> bool {
        if let Network::Server(_) = *self {
            true
        } else {
            false
        }
    }

    pub fn is_authoritative(&self) -> bool {
        !self.is_client()
    }
}
