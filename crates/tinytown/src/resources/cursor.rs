#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum CursorMode {
    Grabbed,
    Ungrabbed,
}

#[derive(Debug, Clone)]
pub struct Cursor {
    pub mode: CursorMode,
}

impl Cursor {
    pub fn new() -> Cursor {
        Cursor {
            mode: CursorMode::Ungrabbed,
        }
    }
}
