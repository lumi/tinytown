use std::{path::PathBuf, time::Instant};

#[derive(Debug, Clone)]
pub struct Autosave {
    pub last_save: Instant,
    pub dirty: bool,
    pub path: Option<PathBuf>,
}

impl Autosave {
    pub fn new(path: Option<PathBuf>) -> Autosave {
        Autosave {
            last_save: Instant::now(),
            dirty: false,
            path,
        }
    }
}
