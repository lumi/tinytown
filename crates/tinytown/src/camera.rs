#![deny(missing_docs)]

//! Camera handling

use {
    cgmath::{prelude::*, Matrix4, PerspectiveFov, Point3, Vector3},
    specs::prelude::*,
};

/// Camera mode
#[derive(Clone, Debug)]
pub enum CameraMode {
    /// No camera
    None,
    /// First person mode, with focus on this entity with a `Player` component.
    FirstPerson {
        /// The entity to focus on
        entity: Entity,
    },
    /// Third person mode, with focus on this entity with a `Player` component.
    ThirdPerson {
        /// The entity to focus on
        entity: Entity,
    },
}

impl CameraMode {
    /// Does this mode correspond to there being no camera?
    pub fn is_none(&self) -> bool {
        if let CameraMode::None = *self {
            true
        } else {
            false
        }
    }

    /// Is this a first person camera?
    pub fn is_first_person(&self) -> bool {
        if let CameraMode::FirstPerson { .. } = *self {
            true
        } else {
            false
        }
    }

    /// Is this a third person camera?
    pub fn is_third_person(&self) -> bool {
        if let CameraMode::ThirdPerson { .. } = *self {
            true
        } else {
            false
        }
    }

    /// Get the entity that the camera is using, or `None` if there is none.
    pub fn entity(&self) -> Option<Entity> {
        match self {
            CameraMode::None => None,
            CameraMode::FirstPerson { entity } => Some(*entity),
            CameraMode::ThirdPerson { entity } => Some(*entity),
        }
    }
}

/// A camera at location `eye` which points in direction `dir` with up vector `up`.
#[derive(Clone, Debug)]
pub struct Camera {
    /// The eye position of the camera
    pub eye: Point3<f32>,
    /// The direction the camera is facing
    pub dir: Vector3<f32>,
    /// The up vector of the camera
    pub up: Vector3<f32>,
    /// The field of view information about the camera
    pub perspective_fov: PerspectiveFov<f32>,
}

impl Camera {
    /// Create a new camera from its components.
    pub fn new(
        eye: Point3<f32>,
        dir: Vector3<f32>,
        up: Vector3<f32>,
        perspective_fov: PerspectiveFov<f32>,
    ) -> Camera {
        Camera {
            eye,
            dir: dir.normalize(),
            up,
            perspective_fov,
        }
    }

    /// Get the projection matrix of the camera.
    pub fn projection(&self) -> Matrix4<f32> {
        let perspective = self.perspective_fov.to_perspective();

        Matrix4::from(perspective)
    }

    /// Get the view matrix of the camera.
    pub fn view(&self) -> Matrix4<f32> {
        Matrix4::look_to_rh(self.eye, self.dir, self.up)
    }

    /// Get the rotation component of the view matrix of the camera.
    pub fn view_rot(&self) -> Matrix4<f32> {
        Matrix4::look_to_rh(Point3::origin(), self.dir, self.up)
    }

    /// Make the camera look at a location.
    pub fn look_at(&mut self, target: Point3<f32>) {
        self.dir = (target - self.eye).normalize();
    }
}
