use {
    cgmath::{prelude::*, PerspectiveFov, Point3, Quaternion, Rad, Vector3},
    regenboog::RgbaU8,
    specs::prelude::*,
    std::{
        collections::VecDeque,
        net::SocketAddr,
        path::{Path, PathBuf},
        time::{Duration, Instant},
    },
    tinytown_collision::Shape,
};

use crate::{
    asset::{AssetKind, AssetManager},
    backend::{Backend, EventLoop},
    brick::{BrickDataStore, BrickEventQueue, BrickOrientation, BrickPalette},
    camera::{Camera, CameraMode},
    collision_manager::CollisionManager,
    components::{Brick, EntityKind, EntityTick, GhostBrick, NetId, Physical, Player},
    config::Config,
    consts::{FAR, FOV_DEG, NEAR, TICK_LENGTH},
    controls::ControlsManager,
    net::{ClientConnection, ConnectionManager, Controller},
    resources::{Autosave, Cursor, NetIdAllocator, Network, Sim, Stats, Sun, WindowSize},
    systems::{
        AutosaveSystem, BrickSpawnerSystem, BuildingSystem, CameraControlSystem, CameraSystem,
        ClientSyncEndSystem, ClientSyncStartSystem, CollisionIndexSystem, CollisionSystem,
        ControlsSystem, EntityTickSystem, MovementSystem, NetIdMaintainerSystem, PhysicsSystem,
        ServerSyncEndSystem, ServerSyncStartSystem, UiSystem,
    },
    tick::Tick,
    ui::{GameUi, Ui},
    vox::Vox,
};

// TODO: put in its own crate or module
fn color_diff(color_a: RgbaU8, color_b: RgbaU8) -> i32 {
    fn sqdiff(a: u8, b: u8) -> i32 {
        let diff = a as i32 - b as i32;
        diff * diff
    }
    sqdiff(color_a.red, color_b.red)
        + sqdiff(color_a.green, color_b.green)
        + sqdiff(color_a.blue, color_b.blue)
        + sqdiff(color_a.alpha, color_b.alpha)
}

#[derive(Debug, Clone)]
enum Command {
    LoadBuild(PathBuf),
}

#[derive(Debug, Clone)]
pub struct GameParams {
    pub listen_addr: Option<String>,
    pub connect_addr: Option<SocketAddr>,
    pub resources_path: PathBuf,
    pub my_name: Option<String>,
    pub autosave_path: Option<PathBuf>,
    pub autoload_path: Option<PathBuf>,
}

pub struct Game<B: Backend + 'static> {
    config: Config,
    params: GameParams,
    has_player: bool,
    is_authoritative: bool,
    is_graphical: bool,
    command_queue: VecDeque<Command>,
    asset_manager: AssetManager,
    world: World,
    backend: B,
}

impl<B: Backend + 'static> Game<B> {
    pub fn new(
        backend: B,
        asset_manager: AssetManager,
        config: Config,
        params: GameParams,
    ) -> Game<B> {
        let has_player = params.listen_addr.is_none(); // for now
        let is_authoritative = params.connect_addr.is_none();
        let is_graphical = params.listen_addr.is_none(); // for now
        Game {
            config,
            params,
            has_player,
            is_authoritative,
            is_graphical,
            command_queue: VecDeque::new(),
            asset_manager,
            world: World::new(),
            backend,
        }
    }

    pub fn init_camera(&mut self) {
        let perspective_fov = PerspectiveFov {
            near: NEAR,
            far: FAR,
            fovy: Rad(FOV_DEG * ::std::f32::consts::PI / 180.),
            aspect: self.backend.aspect_ratio(),
        };
        let mut camera = Camera::new(
            Point3::new(0., 10., 0.),
            Vector3::new(0., 0., 1.),
            Vector3::new(0., 1., 0.),
            perspective_fov,
        );
        camera.look_at(Point3::new(10., 0.3, 10.));
        self.world.insert(camera);
        self.world.insert(CameraMode::None);
    }

    pub fn init_components(&mut self) {
        self.world.register::<Brick>();
        self.world.register::<GhostBrick>();
        self.world.register::<Player>();
        self.world.register::<Physical>();
        self.world.register::<NetId>(); // TODO: might want to make it so this isn't needed when netcode isn't in use
        self.world.register::<EntityTick>();
        self.world.register::<EntityKind>();
    }

    pub fn init_resources(&mut self) {
        let autosave_opt = self.params.autosave_path.as_ref();
        let autosave_config = self.config.main.autosave_path.as_ref();
        let autosave_path = autosave_opt.or(autosave_config);
        if self.is_authoritative {
            self.world
                .insert(Autosave::new(autosave_path.map(PathBuf::to_owned)));
        } else {
            self.world.insert(Autosave::new(None));
        }
        self.world.insert(BrickDataStore::new());
        self.world.insert(CollisionManager::default());
        self.world.insert(BrickPalette::new());
        self.world.insert(BrickEventQueue::new());
        self.world.insert(Stats::new());
        self.world.insert(NetIdAllocator::new());
        self.world.insert(Sim {
            current_tick: Tick(0),
            paused: !self.is_authoritative,
            repeat: false,
            desync: false,
            initial: false,
        });
        self.world.insert(Cursor::new());
        self.world.insert(ControlsManager::new());
        self.world
            .insert(Sun::new(Vector3::new(3., -1., 3.).normalize()));
        let (width, height) = self.backend.size();
        self.world.insert(WindowSize { width, height });
        self.init_camera();
        if let Some(addr) = &self.params.listen_addr {
            // TODO: may not wanna unwrap
            self.world
                .insert(Network::Server(ConnectionManager::bind(addr, 16).unwrap()));
        } else if let Some(addr) = &self.params.connect_addr {
            let my_name = self
                .params
                .my_name
                .as_ref()
                .unwrap_or(&self.config.client.player_name);
            // TODO: may not wanna unwrap
            let client_connection = ClientConnection::connect(addr, my_name).unwrap();
            self.world.insert(Network::Client(client_connection));
        } else {
            self.world.insert(Network::None);
        }
        if self.is_graphical {
            let window_size = self.world.fetch::<WindowSize>();
            let proxy = self.backend.texture_manager_proxy();
            let mut ui = Ui::new(&proxy, window_size.size());
            let game_ui = GameUi::new(&mut ui);
            drop(window_size);
            self.world.insert(ui);
            self.world.insert(game_ui);
        }
    }

    pub fn load_bricks(&mut self) -> anyhow::Result<()> {
        log::info!("loading bricks");
        let mut bds = self.world.fetch_mut::<BrickDataStore>();
        let mut brick_names = self.asset_manager.list(AssetKind::Brick)?;
        brick_names.sort();
        for brick_name in brick_names {
            log::info!("loading brick {}", brick_name);
            let asset_id = self.asset_manager.load(AssetKind::Brick, &brick_name)?;
            let asset_ref = self.asset_manager.get(asset_id)?;
            bds.load_brick(asset_ref)?;
        }
        Ok(())
    }

    pub fn load_palette(&mut self, palette_name: &str) -> anyhow::Result<()> {
        let asset_id = self
            .asset_manager
            .load(AssetKind::Palette, &format!("{}.png", palette_name))?;
        let asset_ref = self.asset_manager.get(asset_id)?;
        log::info!(
            "loading brick color palette {} from {}",
            palette_name,
            asset_ref.name()
        );
        let mut bp = self.world.fetch_mut::<BrickPalette>();
        bp.load_palette(asset_ref)?;
        Ok(())
    }

    pub fn load_build(&mut self, path: impl AsRef<Path>) {
        self.command_queue
            .push_back(Command::LoadBuild(path.as_ref().to_owned()));
    }

    fn load_build_now(&mut self, path: impl AsRef<Path>) -> anyhow::Result<()> {
        // preconditions:
        //   - palette is empty
        //   - the brick types are already loaded in the brick data store
        let path = path.as_ref();
        log::info!("loading ttb {}", path.display());
        let f = std::fs::File::open(path)?;
        let mut reader = ttb::Reader::new(f);
        let palette = self.world.fetch_mut::<BrickPalette>();
        let bds = self.world.fetch_mut::<BrickDataStore>();
        let mut color_ids = [None; 256];
        let mut bricks = Vec::new();
        while let Some(evt) = reader.next_event()? {
            match evt {
                ttb::ReaderEvent::Meta {
                    build_name,
                    build_description,
                } => {
                    log::info!("build name: {}", build_name);
                    log::info!("build description: {}", build_description);
                }
                ttb::ReaderEvent::Color { id, color } => {
                    let mut min_color_id = palette.first().unwrap();
                    let mut min_error = ::std::i32::MAX;
                    for (i, palette_color) in palette.iter() {
                        let error = color_diff(color, palette_color);
                        if error < min_error {
                            min_error = error;
                            min_color_id = i;
                        }
                    }
                    color_ids[id as usize] = Some(min_color_id);
                    log::trace!("color id {} mapped to {:?}", id, min_color_id);
                }
                ttb::ReaderEvent::Brick {
                    brick_type,
                    x,
                    y,
                    z,
                    orientation,
                    color_id,
                } => {
                    log::trace!(
                        "loading brick {:?} at {} {} {}, orientation {}, color_id {}",
                        brick_type,
                        x,
                        y,
                        z,
                        orientation,
                        color_id
                    );
                    if let Some(brick_data_id) = bds.find_by_name(brick_type) {
                        if let Some(brick_color_id) = color_ids[color_id as usize] {
                            bricks.push(Brick::new(
                                brick_color_id,
                                brick_data_id,
                                BrickOrientation::from_u8(orientation),
                                Vox::new(x, y, z),
                            ));
                        } else {
                            unreachable!()
                        }
                    } else {
                        log::error!(
                            "can't find brick type {:?} in the brick data store",
                            brick_type
                        );
                    }
                }
            }
        }
        drop(palette);
        drop(bds);
        let mut entities = Vec::new();
        for brick in bricks {
            let entity = self.world.create_entity().with(brick).build();
            entities.push(entity);
        }
        let net = self.world.fetch::<Network>();
        if net.is_server() {
            let nia = self.world.fetch::<NetIdAllocator>();
            let mut net_ids = self.world.write_storage::<NetId>();
            for entity in entities {
                let net_id = nia.next_id();
                nia.register_id(net_id, entity);
                net_ids.insert(entity, net_id).unwrap();
            }
        }
        self.world.fetch_mut::<BrickEventQueue>().dirty = true;
        Ok(())
    }

    pub fn init(&mut self) -> anyhow::Result<()> {
        self.init_components();
        self.init_resources();
        self.load_bricks()?;
        self.load_palette(&self.config.main.palette.to_owned())?; // TODO: should be able to borrow...
        Ok(())
    }

    pub fn start(mut self) -> anyhow::Result<()> {
        let mut dispatcher = {
            let net = self.world.fetch::<Network>();
            let mut dis = DispatcherBuilder::new();
            if net.is_client() {
                dis.add(ClientSyncStartSystem, "client_sync_start", &[]);
            } else if net.is_server() {
                dis.add(ServerSyncStartSystem, "server_sync_start", &[]);
            }
            dis.add_barrier(); // Pre-sim block
            dis.add(ControlsSystem, "controls", &[]);
            dis.add_barrier(); // Sim block
            dis.add(MovementSystem, "movement", &[]);
            dis.add(PhysicsSystem, "physics", &["movement"]);
            dis.add(
                CollisionIndexSystem::new(&self.world),
                "collision_index",
                &["physics"],
            );
            dis.add(CollisionSystem::new(), "collision", &["collision_index"]);
            dis.add(BuildingSystem, "building", &["collision"]);
            dis.add(BrickSpawnerSystem, "brick_spawner", &["building"]);
            dis.add_barrier();
            dis.add(EntityTickSystem, "entity_tick_system", &[]);
            dis.add_barrier(); // Post-sim block
            dis.add(CameraControlSystem, "camera_control", &[]);
            dis.add(CameraSystem, "camera", &["camera_control"]);
            if self.is_graphical {
                // TODO: this is ugly
                dis.add(UiSystem, "ui", &[]);
            }
            dis.add(AutosaveSystem, "autosave", &[]);
            if net.is_client() {
                dis.add(ClientSyncEndSystem, "client_sync_end", &[]);
            } else if net.is_server() {
                dis.add(
                    ServerSyncEndSystem::new(&self.world),
                    "server_sync_end",
                    &[],
                );
            }
            dis.add_barrier(); // Cleanup block
            dis.add(
                NetIdMaintainerSystem::new(&self.world),
                "net_id_maintainer",
                &[],
            );
            dis.build()
        };

        if self.has_player {
            self.world.fetch_mut::<ControlsManager>().config = self.config.control_config.clone();
            // TODO: don't do this on the client
            let player_entity = self
                .world
                .create_entity()
                .with(EntityTick::at(Tick(0)))
                .with(EntityKind::Player)
                .with(Player::new(Vector3::new(1., 0., 0.), Controller::Local))
                .with(Physical::new(
                    Point3::new(0., 10., 0.),
                    Quaternion::one(),
                    Shape::Cuboid(Vector3::new(0.5, 1., 0.5)),
                ))
                .build();
            *self.world.fetch_mut::<CameraMode>() = CameraMode::FirstPerson {
                entity: player_entity,
            };
        }

        if self.is_authoritative {
            let autoload_path_params = self.params.autoload_path.as_ref();
            let autoload_path_config = self.config.main.autoload_path.as_ref();
            if let Some(path) = autoload_path_params
                .or(autoload_path_config)
                .map(PathBuf::to_owned)
            {
                self.load_build(path);
            }
        }

        for command in self.command_queue.clone().drain(..) {
            match command {
                Command::LoadBuild(path) => {
                    if let Err(err) = self.load_build_now(&path) {
                        log::error!("couldn't load build at {}: {}", path.display(), err);
                    }
                }
            }
        }

        let event_loop = self.backend.create_event_loop()?;

        let mut time_buffer = Duration::ZERO;

        let mut last_iter = Instant::now();

        event_loop.run(move |event| {
            self.backend.handle_event(&self.world, event)?;

            time_buffer += last_iter.elapsed();
            last_iter = Instant::now();

            while time_buffer >= TICK_LENGTH {
                time_buffer -= TICK_LENGTH;
                self.world.fetch_mut::<Sim>().initial = true;
                loop {
                    log::info!(
                        "starting sim tick {}",
                        self.world.fetch::<Sim>().current_tick.0
                    );
                    self.world.fetch_mut::<Sim>().repeat = false;
                    dispatcher.dispatch(&self.world);
                    self.world.maintain();
                    self.world.fetch_mut::<Sim>().initial = false;
                    self.world.fetch_mut::<ControlsManager>().reset();
                    if self.world.fetch::<Sim>().repeat {
                        log::debug!("sim repeat requested");
                    } else {
                        break;
                    }
                }
                let mut sim = self.world.fetch_mut::<Sim>();
                sim.current_tick = sim.current_tick.next();
            }

            if self.is_graphical {
                self.world
                    .fetch_mut::<Ui>()
                    .layout(self.world.fetch::<WindowSize>().size());
                let controls_manager = self.world.fetch::<ControlsManager>();
                if controls_manager.exit_requested() {
                    return Ok(false);
                }
            }

            self.world.fetch::<Stats>().print_stats();

            Ok(true)
        });
    }
}
