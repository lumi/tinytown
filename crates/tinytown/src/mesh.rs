use cgmath::{Point2, Point3, Vector3};

#[derive(Debug, Clone)]
pub struct Mesh<V> {
    pub vertices: Vec<V>,
    pub indices: Vec<u32>,
}

impl<V> Mesh<V> {
    pub fn new(vertices: Vec<V>, indices: Vec<u32>) -> Mesh<V> {
        Mesh { vertices, indices }
    }
}

impl<V: Clone> Mesh<V> {
    pub fn cuboid(
        radius: Vector3<f32>,
        mut make_vertex: impl FnMut(Point3<f32>, Vector3<f32>, Point2<f32>) -> V,
    ) -> Mesh<V> {
        let mut vertices = Vec::new();
        let mut indices = Vec::new();
        let (hw, hh, hd) = (radius.x, radius.y, radius.z);
        let (x, y, z) = (0., 0., 0.);
        {
            let si = vertices.len() as u32;
            let normal = Vector3::new(0., 1., 0.);
            vertices.extend_from_slice(&[
                make_vertex(
                    Point3::new(x - hw, y + hh, z - hd),
                    normal,
                    Point2::new(0., 0.),
                ),
                make_vertex(
                    Point3::new(x + hw, y + hh, z - hd),
                    normal,
                    Point2::new(1., 0.),
                ),
                make_vertex(
                    Point3::new(x + hw, y + hh, z + hd),
                    normal,
                    Point2::new(1., 1.),
                ),
                make_vertex(
                    Point3::new(x - hw, y + hh, z + hd),
                    normal,
                    Point2::new(0., 1.),
                ),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        {
            let si = vertices.len() as u32;
            let normal = Vector3::new(0., -1., 0.);
            vertices.extend_from_slice(&[
                make_vertex(
                    Point3::new(x - hw, y - hh, z - hd),
                    normal,
                    Point2::new(0., 0.),
                ),
                make_vertex(
                    Point3::new(x - hw, y - hh, z + hd),
                    normal,
                    Point2::new(1., 0.),
                ),
                make_vertex(
                    Point3::new(x + hw, y - hh, z + hd),
                    normal,
                    Point2::new(1., 1.),
                ),
                make_vertex(
                    Point3::new(x + hw, y - hh, z - hd),
                    normal,
                    Point2::new(0., 1.),
                ),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        {
            let si = vertices.len() as u32;
            let normal = Vector3::new(0., 0., -1.);
            vertices.extend_from_slice(&[
                make_vertex(
                    Point3::new(x - hw, y - hh, z - hd),
                    normal,
                    Point2::new(0., 0.),
                ),
                make_vertex(
                    Point3::new(x + hw, y - hh, z - hd),
                    normal,
                    Point2::new(1., 0.),
                ),
                make_vertex(
                    Point3::new(x + hw, y + hh, z - hd),
                    normal,
                    Point2::new(1., 1.),
                ),
                make_vertex(
                    Point3::new(x - hw, y + hh, z - hd),
                    normal,
                    Point2::new(0., 1.),
                ),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        {
            let si = vertices.len() as u32;
            let normal = Vector3::new(0., 0., 1.);
            vertices.extend_from_slice(&[
                make_vertex(
                    Point3::new(x - hw, y - hh, z + hd),
                    normal,
                    Point2::new(0., 0.),
                ),
                make_vertex(
                    Point3::new(x - hw, y + hh, z + hd),
                    normal,
                    Point2::new(0., 1.),
                ),
                make_vertex(
                    Point3::new(x + hw, y + hh, z + hd),
                    normal,
                    Point2::new(1., 1.),
                ),
                make_vertex(
                    Point3::new(x + hw, y - hh, z + hd),
                    normal,
                    Point2::new(1., 0.),
                ),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        {
            let si = vertices.len() as u32;
            let normal = Vector3::new(-1., 0., 0.);
            vertices.extend_from_slice(&[
                make_vertex(
                    Point3::new(x - hw, y - hh, z - hd),
                    normal,
                    Point2::new(0., 0.),
                ),
                make_vertex(
                    Point3::new(x - hw, y + hh, z - hd),
                    normal,
                    Point2::new(1., 0.),
                ),
                make_vertex(
                    Point3::new(x - hw, y + hh, z + hd),
                    normal,
                    Point2::new(1., 1.),
                ),
                make_vertex(
                    Point3::new(x - hw, y - hh, z + hd),
                    normal,
                    Point2::new(0., 1.),
                ),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        {
            let si = vertices.len() as u32;
            let normal = Vector3::new(1., 0., 0.);
            vertices.extend_from_slice(&[
                make_vertex(
                    Point3::new(x + hw, y - hh, z - hd),
                    normal,
                    Point2::new(0., 0.),
                ),
                make_vertex(
                    Point3::new(x + hw, y - hh, z + hd),
                    normal,
                    Point2::new(0., 1.),
                ),
                make_vertex(
                    Point3::new(x + hw, y + hh, z + hd),
                    normal,
                    Point2::new(1., 1.),
                ),
                make_vertex(
                    Point3::new(x + hw, y + hh, z - hd),
                    normal,
                    Point2::new(1., 0.),
                ),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        Mesh::new(vertices, indices)
    }
}
