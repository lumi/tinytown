use std::time::Duration;

pub const NEAR: f32 = 0.1;
pub const FAR: f32 = 1000.;
pub const FOV_DEG: f32 = 45.;
pub const TICKS_PER_SECOND: u64 = 60;
pub const TICK_NANOS: u64 = 1_000_000_000 / TICKS_PER_SECOND;
pub const TICK_LENGTH: Duration = Duration::from_nanos(TICK_NANOS);
pub const CONTROLS_QUEUE_SIZE: usize = 8;
pub const SERVER_CONTROLS_QUEUE_SIZE: usize = 4 * CONTROLS_QUEUE_SIZE;
pub const CLIENT_TIMEOUT_MS: u64 = 10000;
pub const AUTOSAVE_BACKOFF_MS: u64 = 1000;
