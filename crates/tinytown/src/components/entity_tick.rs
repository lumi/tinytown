use specs::prelude::*;

use crate::tick::Tick;

/// Marks an entity which is at a specific tick.
///
/// This is used for fast-forwarding in the netcode.
pub struct EntityTick {
    /// The current tick the entity is at
    pub tick: Tick,
}

impl Component for EntityTick {
    type Storage = VecStorage<Self>;
}

impl EntityTick {
    /// Create an `EntityTick` at this tick.
    pub fn at(tick: Tick) -> Self {
        Self { tick }
    }
}
