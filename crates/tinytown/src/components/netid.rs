use specs::prelude::*;

/// An id which identifies an entity over the network.
///
/// This is required because we can't just send an entity's id and generation over the network.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct NetId(pub u32);

impl Component for NetId {
    type Storage = FlaggedStorage<Self, VecStorage<Self>>;
}

impl From<u32> for NetId {
    fn from(net_id: u32) -> NetId {
        NetId(net_id)
    }
}

impl From<NetId> for u32 {
    fn from(NetId(net_id): NetId) -> u32 {
        net_id
    }
}
