use specs::prelude::*;

/// Component which stores the kind of entity this is.
///
/// This is used inside the netcode to define what the `components` bitset means.
///
/// ```plain
/// | entity kind | bit 0      | bit 1 | bit 2 | bit 3 | bit 4 | bit 5 | bit 6 | bit 7 |
/// |           0 | Physical   |       |       |       |       |       |       |       |
/// |           1 | Brick      |       |       |       |       |       |       |       |
/// |           2 | GhostBrick |       |       |       |       |       |       |       |
/// ```
#[derive(Debug, Clone, Copy, PartialEq)]
#[repr(u8)]
pub enum EntityKind {
    /// A player
    Player = 0,
    /// A brick
    Brick = 1,
    /// A ghost brick
    GhostBrick = 2,
}

impl EntityKind {
    /// Try to turn a `u8` into an `EntityKind`.
    pub fn from_u8(kind_num: u8) -> Option<EntityKind> {
        match kind_num {
            0 => Some(EntityKind::Player),
            1 => Some(EntityKind::Brick),
            2 => Some(EntityKind::GhostBrick),
            _ => None,
        }
    }
}

impl pigeon::Pack for EntityKind {
    fn pack<T: pigeon::Target>(&self, writer: &mut pigeon::Writer<T>) -> pigeon::WriteResult<()> {
        writer.write(pigeon::U2(*self as u8))
    }
}

impl<'a> pigeon::Unpack<'a> for EntityKind {
    fn unpack(reader: &mut pigeon::Reader<'a>) -> pigeon::ReadResult<Self> {
        let pigeon::U2(kind_num) = reader.read()?;
        EntityKind::from_u8(kind_num).ok_or(pigeon::ReadError::UnexpectedData)
    }
}

impl Component for EntityKind {
    type Storage = VecStorage<Self>;
}
