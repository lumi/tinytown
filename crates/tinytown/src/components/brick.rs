use {
    cgmath::{Point3, Quaternion},
    specs::prelude::*,
};

use crate::{
    brick::{BrickColorId, BrickDataId, BrickOrientation},
    util::Directional,
    vox::Vox,
};

/// A brick
#[derive(Clone, Debug)]
pub struct Brick {
    /// The color of the brick
    pub color_id: BrickColorId,
    /// The type of brick
    pub data_id: BrickDataId,
    /// The brick's orientation
    pub orientation: BrickOrientation,
    /// The brick's position
    pub pos: Vox,
    /// Whether the brick is covered in a specific direction
    pub covered: Directional<bool>,
    /// The version of this brick
    pub version: u32,
}

impl Component for Brick {
    type Storage = FlaggedStorage<Self, VecStorage<Self>>;
}

impl Brick {
    /// Create a new brick component.
    pub fn new(
        color_id: BrickColorId,
        data_id: BrickDataId,
        orientation: BrickOrientation,
        pos: Vox,
    ) -> Brick {
        Brick {
            color_id,
            data_id,
            orientation,
            pos,
            covered: Directional::all(false),
            version: 0,
        }
    }
}

/// A ghost brick
#[derive(Clone, Debug)]
pub struct GhostBrick {
    /// The player that owns the ghost brick.
    pub player: Option<Entity>,
    /// The color of the ghost brick
    pub color_id: BrickColorId,
    /// The type of ghost brick
    pub data_id: BrickDataId,
    /// The ghost brick's orientation
    pub orientation: BrickOrientation,
    /// The ghost brick's position
    pub pos: Vox,
    /// The location to draw the ghost brick at
    pub visual_pos: Point3<f32>,
    /// The rotation to draw the ghost brick with
    pub visual_rot: Quaternion<f32>,
    /// The version of this ghost brick
    pub version: u32,
}

impl Component for GhostBrick {
    type Storage = FlaggedStorage<Self, VecStorage<Self>>;
}

impl GhostBrick {
    /// Create a new ghost brick component.
    pub fn new(
        player: Option<Entity>,
        color_id: BrickColorId,
        data_id: BrickDataId,
        orientation: BrickOrientation,
        pos: Vox,
    ) -> GhostBrick {
        GhostBrick {
            player,
            color_id,
            data_id,
            orientation,
            pos,
            visual_pos: pos.world_min(),
            visual_rot: orientation.to_quaternion(),
            version: 0,
        }
    }
}
