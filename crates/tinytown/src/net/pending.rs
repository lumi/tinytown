use {core::marker::PhantomData, pigeon::Pack};

pub struct TaggedQueue<T, H: Pack> {
    buf: Box<[u8; 1024]>,
    last: usize,
    segments: Vec<(T, usize, u8)>,
    _marker: PhantomData<H>,
}

impl<T, H: Pack> TaggedQueue<T, H> {
    pub fn new() -> Self {
        let buf = Box::new([0; 1024]);
        let segments = Vec::new();
        Self {
            buf,
            last: 0,
            segments,
            _marker: PhantomData,
        }
    }

    pub fn push<P: Pack>(
        &mut self,
        tag: T,
        header: &H,
        payloads: impl IntoIterator<Item = P>,
    ) -> bool {
        let start = self.last;
        let bufsize = self.buf.len();
        let mut writer = pigeon::Writer::new(&mut self.buf[start..]);
        let header_size = header.size();
        if start + header_size > bufsize {
            return false;
        }
        writer.write(header).unwrap();
        for payload in payloads {
            let payload_size = payload.size();
            if start + writer.position() + payload_size > bufsize {
                return false;
            }
            writer.write(payload).unwrap();
        }
        let bit_offset = writer.bit_offset();
        let len = writer.finish().unwrap();
        self.last = start + len;
        self.segments.push((tag, len, bit_offset));
        true
    }

    pub fn drain<'a>(&'a mut self) -> impl Iterator<Item = (T, u8, &'a [u8])> + 'a {
        self.last = 0;
        let mut last = 0;
        let my_buf = &self.buf[..];
        self.segments.drain(..).map(move |(tag, size, bit_offset)| {
            let buf = &my_buf[last..last + size];
            last += size;
            (tag, bit_offset, buf)
        })
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum Priority {
    High = 0,
    Low = 1,
    Bulk = 2,
}

pub struct PendingQueues<T, H: Pack> {
    queues: [TaggedQueue<T, H>; 3],
    _marker: PhantomData<H>,
}

impl<T, H: Pack> PendingQueues<T, H> {
    pub fn new() -> Self {
        let queues = [TaggedQueue::new(), TaggedQueue::new(), TaggedQueue::new()];
        PendingQueues {
            queues,
            _marker: PhantomData,
        }
    }

    pub fn push<P: Pack>(
        &mut self,
        priority: Priority,
        tag: T,
        header: &H,
        payloads: impl IntoIterator<Item = P>,
    ) -> bool {
        let idx = priority as u8 as usize;
        self.queues[idx].push(tag, header, payloads)
    }

    pub fn drain<'a>(
        &'a mut self,
        priority: Priority,
    ) -> impl Iterator<Item = (T, u8, &'a [u8])> + 'a {
        let idx = priority as u8 as usize;
        self.queues[idx].drain()
    }
}

#[cfg(test)]
mod tests {
    use super::super::{protocol::PushHeader, push::PushTag};

    use super::*;

    #[test]
    fn test_tagged_queue_works_basic() {
        let mut tq = TaggedQueue::new();
        assert!(tq.push(
            PushTag::ServerTick { tick: 1234 },
            &PushHeader::ServerTick { tick: 1234 },
            std::iter::empty::<()>()
        ));
        assert_eq!(tq.last, 5);
        assert_eq!(&tq.buf[..tq.last], &[3, 0, 0, 4, 210]);
        assert_eq!(tq.segments, &[(PushTag::ServerTick { tick: 1234 }, 5, 0)]);
        assert!(tq.push(
            PushTag::ServerTick { tick: 1235 },
            &PushHeader::ServerTick { tick: 1235 },
            std::iter::empty::<()>()
        ));
        assert_eq!(tq.last, 10);
        assert_eq!(&tq.buf[..tq.last], &[3, 0, 0, 4, 210, 3, 0, 0, 4, 211]);
        assert_eq!(
            tq.segments,
            &[
                (PushTag::ServerTick { tick: 1234 }, 5, 0),
                (PushTag::ServerTick { tick: 1235 }, 5, 0),
            ]
        );
    }

    #[test]
    fn test_tagged_queue_works_unaligned() {
        let mut tq = TaggedQueue::new();
        let payloads = vec![pigeon::U3(2), pigeon::U3(1), pigeon::U3(5)];
        assert!(tq.push(
            PushTag::ServerTick { tick: 1234 },
            &PushHeader::ServerTick { tick: 1234 },
            payloads,
        ));
        assert_eq!(tq.last, 7);
        assert_eq!(
            &tq.buf[..tq.last],
            &[0x03, 0x00, 0x00, 0x04, 0xD2, 0x46, 0x80]
        );
        assert_eq!(tq.segments, &[(PushTag::ServerTick { tick: 1234 }, 7, 1)]);
        let payloads = vec![
            (pigeon::U4(9), true),
            (pigeon::U4(5), false),
            (pigeon::U4(15), true),
        ];
        assert!(tq.push(
            PushTag::ServerTick { tick: 1234 },
            &PushHeader::ServerTick { tick: 1234 },
            payloads,
        ));
        assert_eq!(tq.last, 14);
        assert_eq!(
            &tq.buf[..tq.last],
            &[0x03, 0x00, 0x00, 0x04, 0xD2, 0x46, 0x80, 0x03, 0x00, 0x00, 0x04, 0xD2, 0x9A, 0xBE]
        );
        assert_eq!(
            tq.segments,
            &[
                (PushTag::ServerTick { tick: 1234 }, 7, 1),
                (PushTag::ServerTick { tick: 1234 }, 7, 7),
            ]
        );
    }
}
