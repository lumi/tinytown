use std::collections::VecDeque;

use super::protocol::{FrameHeader, PushKind};

use crate::consts::CONTROLS_QUEUE_SIZE;

#[derive(Debug, Clone)]
pub enum PushEvent {
    Lost(PushTag),
    Delivered(PushTag),
}

#[derive(Debug, Clone)]
pub struct PushInTransit {
    pub id: u32,
    pub tag: PushTag,
}

#[derive(Debug, Clone, PartialEq)]
pub enum GhostTag {
    None,
    Player {
        physical_version: u32,
        player_version: u32,
    },
    Brick {
        brick_version: u32,
    },
    GhostBrick {
        ghost_brick_version: u32,
    },
}

#[derive(Debug, Clone, PartialEq)]
pub enum PushTag {
    Ghost {
        net_id: u32,
        tag: GhostTag,
    },
    Event {
        event_id: u32,
    },
    ClientTick {
        tick: u32,
        sent_controls: [bool; CONTROLS_QUEUE_SIZE],
    },
    ServerTick {
        tick: u32,
    },
    PlayerUpdate {
        tick: u32,
    },
}

impl PushTag {
    pub fn to_kind(&self) -> PushKind {
        match self {
            PushTag::Ghost { .. } => PushKind::Ghost,
            PushTag::Event { .. } => PushKind::Event,
            PushTag::ClientTick { .. } => PushKind::ClientTick,
            PushTag::ServerTick { .. } => PushKind::ServerTick,
            PushTag::PlayerUpdate { .. } => PushKind::PlayerUpdate,
        }
    }
}

#[derive(Debug)]
pub struct PushWindow {
    last_sent_id: u32,
    last_recv_id: u32,
    last_seen_id: u32,
    in_transit: VecDeque<PushInTransit>,
    recv_window: u32,
    payload_count: u8,
}

impl PushWindow {
    pub fn new() -> PushWindow {
        PushWindow {
            last_sent_id: 0,
            last_recv_id: 0,
            last_seen_id: 0,
            in_transit: VecDeque::with_capacity(256),
            recv_window: 0,
            payload_count: 0,
        }
    }

    pub fn recv_header<'a>(
        &'a mut self,
        push_id: u32,
        last_push_id: u32,
        push_window: u32,
    ) -> PushEventIterator<'a> {
        if self.last_recv_id < push_id {
            let diff = push_id - self.last_recv_id;
            self.recv_window = (self.recv_window >> diff) | 1;
            self.last_recv_id = push_id;
        }
        PushEventIterator {
            last_push_id,
            push_window,
            inner: self,
        }
    }

    pub fn add_push(&mut self, tag: PushTag) {
        let record = PushInTransit {
            id: self.last_sent_id + 1,
            tag,
        };
        self.in_transit.push_back(record);
        self.payload_count += 1;
    }

    pub fn last_sent_id(&self) -> u32 {
        self.last_sent_id
    }

    pub fn next_header(&mut self) -> FrameHeader {
        self.last_sent_id += 1;
        let payload_count = self.payload_count;
        self.payload_count = 0;
        FrameHeader::Push {
            push_id: self.last_sent_id,
            last_push_id: self.last_recv_id,
            push_window: self.recv_window << 1,
            payload_count,
        }
    }
}

pub struct PushEventIterator<'a> {
    inner: &'a mut PushWindow,
    last_push_id: u32,
    push_window: u32,
}

impl<'a> Iterator for PushEventIterator<'a> {
    type Item = PushEvent;

    fn next(&mut self) -> Option<Self::Item> {
        if self.inner.last_seen_id < self.last_push_id {
            if let Some(item) = self.inner.in_transit.front() {
                if item.id > self.last_push_id {
                    self.inner.last_seen_id = self.last_push_id;
                    return None;
                }
                let pkt_received = if self.last_push_id != item.id {
                    let offset = self.last_push_id - item.id - 1;
                    if offset >= 32 {
                        false
                    } else {
                        (self.push_window << offset) & 1 != 0
                    }
                } else {
                    true
                };
                let item = self.inner.in_transit.pop_front().unwrap(); // can't panic; checked earlier
                if pkt_received {
                    Some(PushEvent::Delivered(item.tag))
                } else {
                    Some(PushEvent::Lost(item.tag))
                }
            } else {
                None
            }
        } else {
            None
        }
    }
}
