use {
    cgmath::{Point3, Vector3},
    pigeon::{Pack, Unpack},
    tinytown_protocol::LossyUnitVector,
};

use crate::{
    brick::{BrickColorId, BrickDataId, BrickOrientation},
    components::{Brick, GhostBrick},
    tool::Tool,
    vox::Vox,
};

#[derive(Debug, Clone)]
pub struct FullGhostBrickInfo {
    pub data_id: BrickDataId,
    pub color_id: BrickColorId,
    pub pos: Vox,
    pub orientation: BrickOrientation,
}

impl Pack for FullGhostBrickInfo {
    fn pack<T: pigeon::Target>(&self, writer: &mut pigeon::Writer<T>) -> pigeon::WriteResult<()> {
        writer.write(self.data_id.0 as u16)?;
        writer.write(self.color_id.0 as u8)?;
        writer.write(self.pos.x)?;
        writer.write(self.pos.y)?;
        writer.write(self.pos.z)?;
        writer.write(self.orientation)?;
        Ok(())
    }
}

impl<'a> Unpack<'a> for FullGhostBrickInfo {
    fn unpack(reader: &mut pigeon::Reader<'a>) -> pigeon::ReadResult<Self> {
        let data_id = BrickDataId(reader.read::<u16>()? as usize);
        let color_id = BrickColorId(reader.read::<u8>()? as usize);
        let (pos_x, pos_y, pos_z) = reader.read()?;
        let pos = Vox::new(pos_x, pos_y, pos_z);
        let orientation = reader.read()?;
        Ok(FullGhostBrickInfo {
            data_id,
            color_id,
            pos,
            orientation,
        })
    }
}

#[derive(Debug, Clone)]
pub struct FullPlayerInfo {
    pub pos: Point3<f32>,
    pub vel: Vector3<f32>,
    pub tool: Tool,
    pub selected_color_id: BrickColorId,
    pub ghost_info: Option<FullGhostBrickInfo>,
}

impl Pack for FullPlayerInfo {
    fn pack<T: pigeon::Target>(&self, writer: &mut pigeon::Writer<T>) -> pigeon::WriteResult<()> {
        writer.write(self.pos.x)?;
        writer.write(self.pos.y)?;
        writer.write(self.pos.z)?;
        writer.write(self.vel.x)?;
        writer.write(self.vel.y)?;
        writer.write(self.vel.z)?;
        writer.write(self.tool)?;
        writer.write(self.selected_color_id.0 as u8)?;
        if let Some(ghost_info) = &self.ghost_info {
            writer.write(true)?;
            writer.write(ghost_info)?;
        } else {
            writer.write(false)?;
        }
        Ok(())
    }
}

impl<'a> Unpack<'a> for FullPlayerInfo {
    fn unpack(reader: &mut pigeon::Reader<'a>) -> pigeon::ReadResult<Self> {
        let (pos_x, pos_y, pos_z) = reader.read()?;
        let (vel_x, vel_y, vel_z) = reader.read()?;
        let pos = Point3::new(pos_x, pos_y, pos_z);
        let vel = Vector3::new(vel_x, vel_y, vel_z);
        let tool = reader.read()?;
        let selected_color_id = BrickColorId(reader.read::<u8>()? as usize);
        let has_ghost_brick: bool = reader.read()?;
        let ghost_info = if has_ghost_brick {
            let ghost_info = reader.read()?;
            Some(ghost_info)
        } else {
            None
        };
        Ok(FullPlayerInfo {
            pos,
            vel,
            tool,
            selected_color_id,
            ghost_info,
        })
    }
}

#[derive(Debug, Clone)]
pub struct BrickInfo {
    pub color_id: BrickColorId,
    pub data_id: BrickDataId,
    pub pos: Vox,
    pub orientation: BrickOrientation,
}

impl BrickInfo {
    pub fn from_component(brick: &Brick) -> BrickInfo {
        BrickInfo {
            color_id: brick.color_id,
            data_id: brick.data_id,
            pos: brick.pos,
            orientation: brick.orientation,
        }
    }

    pub fn from_ghost_component(gb: &GhostBrick) -> BrickInfo {
        BrickInfo {
            color_id: gb.color_id,
            data_id: gb.data_id,
            pos: gb.pos,
            orientation: gb.orientation,
        }
    }
}

impl Pack for BrickInfo {
    fn pack<T: pigeon::Target>(&self, writer: &mut pigeon::Writer<T>) -> pigeon::WriteResult<()> {
        writer.write(self.color_id.0 as u8)?;
        writer.write(self.data_id.0 as u16)?;
        writer.write(self.pos.x)?;
        writer.write(self.pos.y)?;
        writer.write(self.pos.z)?;
        writer.write(self.orientation)?;
        Ok(())
    }
}

impl<'a> Unpack<'a> for BrickInfo {
    fn unpack(reader: &mut pigeon::Reader<'a>) -> pigeon::ReadResult<Self> {
        let color_id = BrickColorId(reader.read::<u8>()? as usize);
        let data_id = BrickDataId(reader.read::<u16>()? as usize);
        let x = reader.read()?;
        let y = reader.read()?;
        let z = reader.read()?;
        let pos = Vox::new(x, y, z);
        let orientation = reader.read()?;
        Ok(BrickInfo {
            color_id,
            data_id,
            pos,
            orientation,
        })
    }
}

#[derive(Debug, Clone)]
pub struct PlayerGhostInfo {
    pub position: Option<Point3<f32>>,
    pub facing: Option<Vector3<f32>>,
}

impl Pack for PlayerGhostInfo {
    fn pack<T: pigeon::Target>(&self, writer: &mut pigeon::Writer<T>) -> pigeon::WriteResult<()> {
        if let Some(position) = self.position {
            writer.write(true)?;
            writer.write(position.x)?;
            writer.write(position.y)?;
            writer.write(position.z)?;
        } else {
            writer.write(false)?;
        }
        if let Some(facing) = self.facing {
            writer.write(true)?;
            writer.write(LossyUnitVector(facing))?;
        } else {
            writer.write(false)?;
        }
        Ok(())
    }
}

impl<'a> Unpack<'a> for PlayerGhostInfo {
    fn unpack(reader: &mut pigeon::Reader<'a>) -> pigeon::ReadResult<Self> {
        let position = if reader.read::<bool>()? {
            let x = reader.read()?;
            let y = reader.read()?;
            let z = reader.read()?;
            Some(Point3::new(x, y, z))
        } else {
            None
        };
        let facing = if reader.read::<bool>()? {
            let LossyUnitVector(facing) = reader.read()?;
            Some(facing)
        } else {
            None
        };
        Ok(PlayerGhostInfo { position, facing })
    }
}
