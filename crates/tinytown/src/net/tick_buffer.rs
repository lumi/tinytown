use core::mem;

/// A buffer which holds the items for the latest `size` ticks.
///
/// # Examples
///
/// ```rust
/// # use {
/// #     tinytown::net::TickBuffer,
/// # };
/// let mut tb = TickBuffer::new(32);
///
/// assert!(tb.add(12, 0u32).is_none());
/// assert!(tb.add(13, 1u32).is_none());
///
/// assert!(tb.at_tick(11).is_none());
/// assert_eq!(*tb.at_tick(12).unwrap(), 0);
/// assert_eq!(*tb.at_tick(13).unwrap(), 1);
///
/// assert!(tb.remove(11).is_none());
///
/// assert_eq!(tb.remove(12), Some(0));
///
/// assert!(tb.remove(12).is_none());
/// assert!(tb.at_tick(12).is_none());
///
/// assert!(tb.add(128, 1u32).is_none());
///
/// assert!(tb.at_tick(12).is_none());
/// assert!(tb.at_tick(13).is_none());
///
/// assert_eq!(*tb.at_tick(128).unwrap(), 1);
/// ```
#[derive(Debug, Clone)]
pub struct TickBuffer<T> {
    pub buf: Vec<Option<T>>,
    pub last_tick: u32,
    pub start: usize,
}

fn item_idx(start: usize, len: usize, idx: usize) -> usize {
    (start + idx) % len
}

impl<T> TickBuffer<T> {
    /// Create a new tick buffer.
    pub fn new(size: usize) -> Self {
        let mut buf = Vec::with_capacity(size);
        buf.extend((0..size).map(|_| None));
        TickBuffer {
            buf,
            last_tick: 0,
            start: 0,
        }
    }

    /// Get the tick value of the item that was added with the heighest tick value.
    pub fn last_tick(&self) -> u32 {
        self.last_tick
    }

    /// Add an item to the tick buffer.
    ///
    /// Returns the previous item if there was one.
    ///
    /// Does nothing if this would exceed the buffer's side on the tail end.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::net::TickBuffer,
    /// # };
    /// let mut tb = TickBuffer::new(32);
    ///
    /// assert!(tb.add(2, false).is_none());
    /// assert!(tb.add(3, true).is_none());
    /// assert_eq!(tb.add(3, false).unwrap(), true);
    /// assert_eq!(tb.add(3, false).unwrap(), false);
    ///
    /// assert!(tb.add(2, false).is_none());
    ///
    /// assert_eq!(*tb.at_tick(2).unwrap(), false);
    /// assert_eq!(*tb.at_tick(3).unwrap(), false);
    ///
    /// assert!(tb.add(64, true).is_none());
    /// assert!(tb.add(3, true).is_none());
    ///
    /// assert!(tb.at_tick(3).is_none());
    /// ```
    pub fn add(&mut self, tick: u32, item: T) -> Option<T> {
        let inner_len = self.buf.len();
        if tick > self.last_tick {
            let diff = (tick - self.last_tick) as usize;
            if diff >= inner_len {
                // going to overwrite everything anyway, better reset the entire buffer
                for item in &mut self.buf {
                    *item = None;
                }
                self.start = 0;
            } else {
                for i in 0..diff {
                    self.start = (self.start + 1) % inner_len;
                    let idx = item_idx(self.start, inner_len, i);
                    self.buf[idx] = None;
                }
            }
            self.last_tick = tick;
        }
        let offset = (self.last_tick - tick) as usize;
        let idx = item_idx(self.start, inner_len, offset);
        let old = mem::replace(&mut self.buf[idx], Some(item));
        old
    }

    /// Remove an item from the tick buffer.
    ///
    /// Returns the removed item if it exists.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::net::TickBuffer,
    /// # };
    /// let mut tb = TickBuffer::new(4);
    ///
    /// assert!(tb.remove(6).is_none());
    ///
    /// for i in 0..8 {
    ///     assert!(tb.add(i, (i * 3 + 2) / 2).is_none());
    /// }
    ///
    /// assert!(tb.remove(2).is_none());
    /// assert_eq!(tb.remove(6).unwrap(), 10);
    /// assert!(tb.remove(6).is_none());
    /// ```
    pub fn remove(&mut self, tick: u32) -> Option<T> {
        if tick > self.last_tick {
            None
        } else {
            let offset = (self.last_tick - tick) as usize;
            let inner_len = self.buf.len();
            if offset >= inner_len {
                None
            } else {
                let idx = item_idx(self.start, inner_len, inner_len - offset);
                let old = mem::replace(&mut self.buf[idx], None);
                old
            }
        }
    }

    /// Fetch an item at a specific tick, if it exists.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::net::TickBuffer,
    /// # };
    /// let mut tb = TickBuffer::new(2);
    ///
    /// assert!(tb.at_tick(0).is_none());
    /// assert!(tb.at_tick(1).is_none());
    /// assert!(tb.at_tick(2).is_none());
    /// assert!(tb.at_tick(3).is_none());
    ///
    /// for i in 0..4 {
    ///     assert!(tb.add(i, (i * 3 + 2) / 2).is_none());
    /// }
    ///
    /// assert!(tb.at_tick(0).is_none());
    /// assert!(tb.at_tick(1).is_none());
    /// assert_eq!(*tb.at_tick(2).unwrap(), 4);
    /// assert_eq!(*tb.at_tick(3).unwrap(), 5);
    /// ```
    pub fn at_tick(&self, tick: u32) -> Option<&T> {
        if tick <= self.last_tick {
            let offset = (self.last_tick - tick) as usize;
            let inner_len = self.buf.len();
            if offset < inner_len {
                let idx = item_idx(self.start, inner_len, inner_len - offset);
                self.buf[idx].as_ref()
            } else {
                None
            }
        } else {
            None
        }
    }

    /// Returns an iterator over the tick buffer, yielding (tick, offset, item_ref) triples.
    ///
    /// This iterates from highest to lowest tick.
    ///
    /// ```rust
    /// # use {
    /// #     tinytown::net::TickBuffer,
    /// # };
    /// let mut tb = TickBuffer::new(4);
    ///
    /// for i in 0..8 {
    ///     assert!(tb.add(i, (i * 3 + 2) / 2).is_none());
    /// }
    ///
    /// assert!(tb.remove(6).is_some());
    ///
    /// let mut iter = tb.iter();
    ///
    /// assert_eq!(iter.next().unwrap(), (7, 0, &11));
    /// assert_eq!(iter.next().unwrap(), (5, 2, &8));
    /// assert_eq!(iter.next().unwrap(), (4, 3, &7));
    /// assert_eq!(iter.next(), None);
    ///
    /// ```
    pub fn iter<'a>(&'a self) -> TickBufferIter<'a, T> {
        TickBufferIter {
            inner: self,
            offset: 0,
        }
    }
}

pub struct TickBufferIter<'a, T> {
    inner: &'a TickBuffer<T>,
    offset: usize,
}

impl<'a, T> Iterator for TickBufferIter<'a, T> {
    type Item = (u32, usize, &'a T);

    fn next(&mut self) -> Option<Self::Item> {
        let inner_len = self.inner.buf.len();
        loop {
            if self.offset < inner_len && self.offset <= self.inner.last_tick as usize {
                let offset = self.offset;
                self.offset += 1;
                let idx = item_idx(self.inner.start, inner_len, inner_len - offset);
                if let Some(item) = &self.inner.buf[idx] {
                    let tick = self.inner.last_tick - offset as u32;
                    break Some((tick, offset, item));
                }
            } else {
                break None;
            }
        }
    }
}
