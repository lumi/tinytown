use {
    cgmath::Vector3,
    core::iter,
    specs::prelude::*,
    std::{
        collections::VecDeque,
        io::ErrorKind,
        net::{SocketAddr, ToSocketAddrs, UdpSocket},
        time::{Duration, Instant},
    },
    tinytown_protocol::UnitVector,
};

use crate::{
    components::{Brick, GhostBrick, NetId, Physical, Player},
    consts::{CLIENT_TIMEOUT_MS, SERVER_CONTROLS_QUEUE_SIZE},
    controls::ControlState,
};

use super::{
    connection::Connection,
    controls::ControlsQueue,
    data::{BrickInfo, FullPlayerInfo, PlayerGhostInfo},
    pending::Priority,
    protocol::{FrameHeader, GhostHeader, JoinRejectReason, PushHeader},
    push::{GhostTag, PushEvent, PushTag},
    SlotId,
};

#[derive(Debug)]
pub enum ConnectionEvent {
    PlayerJoined { slot: SlotId, name: String },
    PlayerLeft { slot: SlotId },
    LostGhost { net_id: NetId, tag: GhostTag },
}

pub struct ConnectionManager {
    pub sock: UdpSocket,
    pub slots: Vec<Option<ServerConnection>>,
}

impl ConnectionManager {
    pub fn bind<A: ToSocketAddrs>(addr: A, slots: usize) -> anyhow::Result<ConnectionManager> {
        let sock = UdpSocket::bind(addr)?;
        sock.set_nonblocking(true)?;
        Ok(ConnectionManager {
            sock,
            slots: (0..slots).map(|_| None).collect(),
        })
    }

    pub fn start_tick(
        &mut self,
        tick: u32,
        mut cb: impl for<'a> FnMut(&mut ServerConnection, ConnectionEvent),
    ) -> anyhow::Result<()> {
        for slot in &mut self.slots {
            let mut timed_out = false;
            if let Some(conn) = slot {
                if conn.last_activity.elapsed() > Duration::from_millis(CLIENT_TIMEOUT_MS) {
                    timed_out = true;
                }
            }
            if timed_out {
                // TODO: more handling
                log::info!(
                    "client with slot {:?} timed out",
                    slot.as_ref().unwrap().slot
                );
                let mut conn = slot.take().unwrap();
                let slot = conn.slot;
                cb(&mut conn, ConnectionEvent::PlayerLeft { slot });
            }
        }
        for slot in &mut self.slots {
            if let Some(conn) = slot {
                conn.on_tick(tick);
            }
        }
        Ok(())
    }

    pub fn handle_frames(
        &mut self,
        mut cb: impl for<'a> FnMut(&mut ServerConnection, ConnectionEvent),
    ) -> anyhow::Result<()> {
        let mut buf = [0; 508];
        loop {
            let (addr, pkt) = match self.sock.recv_from(&mut buf[..]) {
                Ok((size, addr)) => (addr, &buf[..size]),
                Err(err) => {
                    if err.kind() == ErrorKind::WouldBlock {
                        break Ok(());
                    } else {
                        break Err(err.into());
                    }
                }
            };
            log::debug!("packet received from {}", addr);
            log::trace!("packet data(len={}): {:x?}", pkt.len(), pkt);
            let mut reader = pigeon::Reader::new(pkt);
            if let Ok(frame) = reader.read::<FrameHeader>() {
                match frame {
                    FrameHeader::JoinRequest { client_cookie } => {
                        let challenge =
                            super::protocol::generate_server_challenge(addr.ip(), client_cookie);
                        let frame = FrameHeader::JoinChallenge { challenge };
                        let len = pigeon::pack_buffer(&frame, &mut buf[..]).unwrap();
                        self.sock.send_to(&buf[..len], addr)?;
                    }
                    FrameHeader::JoinResponse {
                        client_cookie,
                        challenge,
                        name,
                    } => {
                        let real_challenge =
                            super::protocol::generate_server_challenge(addr.ip(), client_cookie);
                        if challenge == real_challenge {
                            let mut found_slot = false;
                            for (idx, slot) in self.slots.iter_mut().enumerate() {
                                if slot.is_none() {
                                    let slot_id = SlotId(idx);
                                    log::info!("new client on slot {:?}: name={:?}", slot_id, name);
                                    *slot =
                                        Some(ServerConnection::new(slot_id, name.to_owned(), addr));
                                    cb(
                                        slot.as_mut().unwrap(),
                                        ConnectionEvent::PlayerJoined {
                                            slot: slot_id,
                                            name: name.to_owned(),
                                        },
                                    );
                                    let frame = FrameHeader::JoinAccept { server_tick: 0 }; // TODO: put the real tick here
                                    let len = pigeon::pack_buffer(&frame, &mut buf[..]).unwrap();
                                    self.sock.send_to(&buf[..len], addr)?;
                                    found_slot = true;
                                    break;
                                }
                            }
                            if !found_slot {
                                let frame = FrameHeader::JoinReject {
                                    reason: JoinRejectReason::Full,
                                };
                                let len = pigeon::pack_buffer(&frame, &mut buf[..]).unwrap();
                                self.sock.send_to(&buf[..len], addr)?;
                            }
                        }
                    }
                    frame_header => {
                        if let Some(mut connection) = self
                            .slots
                            .iter_mut()
                            .filter_map(|s| s.as_mut())
                            .find(|c| c.addr == addr)
                        {
                            let mut queue = VecDeque::new();
                            connection.on_recv(frame_header, &mut reader, &mut queue);
                            for evt in queue.drain(..) {
                                cb(&mut connection, evt);
                            }
                        }
                    }
                }
            } else {
                log::warn!("ignoring malformed packet from {}", addr);
            }
        }
    }

    pub fn flush_frames(&mut self) -> anyhow::Result<()> {
        let mut buf = [0; 1024];
        for slot in &mut self.slots {
            if let Some(connection) = slot.as_mut() {
                let len = connection.connection.write_frame(&mut buf[..]);
                self.sock.send_to(&buf[..len], connection.addr)?;
            }
        }
        Ok(())
    }
}

pub struct ServerConnection {
    pub slot: SlotId,
    pub name: String,
    pub addr: SocketAddr,
    pub last_activity: Instant,
    pub connection: Connection,
    pub controls: ControlsQueue,
    pub player_entity: Option<Entity>,
}

impl ServerConnection {
    pub fn new(slot: SlotId, name: String, addr: SocketAddr) -> ServerConnection {
        ServerConnection {
            slot,
            name,
            addr,
            last_activity: Instant::now(),
            connection: Connection::accepting(),
            controls: ControlsQueue::new(SERVER_CONTROLS_QUEUE_SIZE),
            player_entity: None,
        }
    }

    pub fn on_recv<'a, 'b: 'a>(
        &mut self,
        frame_header: FrameHeader,
        reader: &'b mut pigeon::Reader<'a>,
        queue: &mut VecDeque<ConnectionEvent>,
    ) {
        self.last_activity = Instant::now();
        let controls_queue = &mut self.controls;
        let push_status_cb = |push_event| {
            log::debug!("push status cb {:?}", push_event);
            match push_event {
                PushEvent::Lost(push_info) => match push_info {
                    PushTag::Ghost { net_id, tag } => {
                        queue.push_back(ConnectionEvent::LostGhost {
                            net_id: NetId(net_id),
                            tag,
                        });
                    }
                    _ => (),
                },
                PushEvent::Delivered(_push_info) => {}
            }
        };
        let push_recv_cb = |reader: &mut pigeon::Reader, push_header: PushHeader| {
            log::debug!("push recv cb {:?}", push_header);
            match push_header {
                PushHeader::ClientTick {
                    tick,
                    controls_count,
                } => {
                    for _ in 0..controls_count {
                        match reader.read::<(pigeon::U4, u16, UnitVector, ControlState)>() {
                            Ok((pigeon::U4(offset), player_crc, UnitVector(facing), controls)) => {
                                let new_tick = tick - offset as u32;
                                log::debug!(
                                    "received controls frame: {:?} {:?} {:?} {:?}",
                                    new_tick,
                                    facing,
                                    controls,
                                    player_crc
                                );
                                controls_queue.submit(new_tick, player_crc, controls, facing);
                            }
                            Err(err) => {
                                log::error!("failed to read a controls frame: {:?}", err);
                            }
                        }
                    }
                }
                _ => (),
            }
            Ok(())
        };
        self.connection
            .on_recv(frame_header, reader, push_status_cb, push_recv_cb);
    }

    pub fn on_tick(&mut self, tick: u32) {
        self.connection.add_push(
            Priority::High,
            PushTag::ServerTick { tick },
            &PushHeader::ServerTick { tick },
            iter::empty::<&()>(),
        );
    }

    pub fn controls_for_tick(&self, tick: u32) -> Option<(ControlState, Vector3<f32>, u16)> {
        self.controls
            .at_tick(tick)
            .map(|entry| (entry.controls.clone(), entry.facing, entry.player_crc))
    }

    pub fn add_brick_ghost(&mut self, net_id: NetId, brick: &Brick) {
        self.connection.add_push(
            Priority::Low,
            PushTag::Ghost {
                net_id: net_id.0,
                tag: GhostTag::Brick {
                    brick_version: brick.version,
                },
            },
            &PushHeader::Ghost {
                net_id: net_id.0,
                header: GhostHeader::Brick,
            },
            iter::once(&BrickInfo::from_component(brick)),
        );
    }

    pub fn add_ghost_brick_ghost(&mut self, net_id: NetId, gb: &GhostBrick) {
        self.connection.add_push(
            Priority::Low,
            PushTag::Ghost {
                net_id: net_id.0,
                tag: GhostTag::GhostBrick {
                    ghost_brick_version: gb.version,
                },
            },
            &PushHeader::Ghost {
                net_id: net_id.0,
                header: GhostHeader::GhostBrick,
            },
            iter::once(&BrickInfo::from_ghost_component(gb)),
        );
    }

    pub fn add_ghost_delete(&mut self, net_id: NetId) {
        self.connection.add_push(
            Priority::Low,
            PushTag::Ghost {
                net_id: net_id.0,
                tag: GhostTag::None,
            },
            &PushHeader::Ghost {
                net_id: net_id.0,
                header: GhostHeader::None,
            },
            iter::empty::<()>(),
        );
    }

    pub fn add_player_update(&mut self, tick: u32, player_info: &FullPlayerInfo) {
        self.connection.add_push(
            Priority::High,
            PushTag::PlayerUpdate { tick },
            &PushHeader::PlayerUpdate { tick },
            iter::once(player_info),
        );
    }

    pub fn update_player_ghost(
        &mut self,
        net_id: NetId,
        player_version: u32,
        player: &Player,
        physical_version: u32,
        physical: &Physical,
    ) {
        let position = if physical_version == physical.version {
            Some(physical.pos)
        } else {
            None
        };
        let facing = if player_version == player.version {
            Some(player.facing)
        } else {
            None
        };
        if position.is_some() || facing.is_some() {
            let player_ghost_info = PlayerGhostInfo { position, facing };
            self.connection.add_push(
                Priority::Low,
                PushTag::Ghost {
                    net_id: net_id.0,
                    tag: GhostTag::Player {
                        // TODO: might need more special handling here
                        physical_version,
                        player_version,
                    },
                },
                &PushHeader::Ghost {
                    net_id: net_id.0,
                    header: GhostHeader::Player,
                },
                iter::once(player_ghost_info),
            );
        }
    }
}
