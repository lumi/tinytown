use std::{
    collections::VecDeque,
    time::{Duration, Instant},
};

use pigeon::Pack;

use super::{
    pending::{PendingQueues, Priority},
    protocol::{FrameHeader, PushHeader},
    push::{PushEvent, PushTag, PushWindow},
};

pub struct Connection {
    push_window: PushWindow,
    push_timing: VecDeque<(u32, Instant)>,
    push_timing_measurements: VecDeque<Duration>,
    pending: PendingQueues<PushTag, PushHeader>,
    initiating: bool,
    dropped: Vec<PushTag>,
}

impl Connection {
    pub fn initiating() -> Connection {
        Connection {
            push_window: PushWindow::new(),
            push_timing: VecDeque::new(),
            push_timing_measurements: VecDeque::new(),
            initiating: true,
            pending: PendingQueues::new(),
            dropped: Vec::new(),
        }
    }

    pub fn accepting() -> Connection {
        Connection {
            push_window: PushWindow::new(),
            push_timing: VecDeque::new(),
            push_timing_measurements: VecDeque::new(),
            initiating: false,
            pending: PendingQueues::new(),
            dropped: Vec::new(),
        }
    }

    pub fn is_initiating(&self) -> bool {
        self.initiating
    }

    pub fn rtt_ms(&self) -> u32 {
        let mut acc_ms = 0;
        for duration in &self.push_timing_measurements {
            let ms = duration.as_millis() as u32;
            acc_ms += ms;
        }
        let len = self.push_timing_measurements.len() as u32;
        acc_ms / len
    }

    pub fn on_recv<'a>(
        &mut self,
        frame_header: FrameHeader,
        reader: &'a mut pigeon::Reader<'a>,
        mut push_status_cb: impl FnMut(PushEvent),
        mut push_recv_cb: impl FnMut(&mut pigeon::Reader<'a>, PushHeader) -> pigeon::ReadResult<()>,
    ) {
        for info in self.dropped.drain(..) {
            push_status_cb(PushEvent::Lost(info));
        }
        log::trace!("received frame header: {:?}", frame_header);
        reader.skip_align();
        match frame_header {
            FrameHeader::Push {
                push_id,
                last_push_id,
                push_window,
                payload_count,
            } => {
                while let Some(&(id, sent)) = self.push_timing.front() {
                    if id < last_push_id {
                        self.push_timing.pop_front().unwrap();
                    } else if id == last_push_id {
                        self.push_timing.pop_front().unwrap();
                        let elapsed = sent.elapsed();
                        log::trace!("rtt measurement for push frame {}: {:?}", id, elapsed);
                        if self.push_timing_measurements.len()
                            == self.push_timing_measurements.capacity()
                        {
                            self.push_timing_measurements.pop_front();
                        }
                        self.push_timing_measurements.push_back(elapsed);
                    } else {
                        break;
                    }
                }
                for evt in self
                    .push_window
                    .recv_header(push_id, last_push_id, push_window)
                {
                    match evt {
                        PushEvent::Lost(info) => push_status_cb(PushEvent::Lost(info)),
                        PushEvent::Delivered(info) => push_status_cb(PushEvent::Delivered(info)),
                    }
                }
                for i in 0..payload_count {
                    // TODO: log bit offset too
                    log::trace!("payload {} reader position={}", i, reader.position());
                    match reader.read::<PushHeader>() {
                        Ok(ph) => {
                            if let Err(err) = push_recv_cb(reader, ph) {
                                log::error!("could not decode push payload: {}", err);
                            }
                        }
                        Err(err) => {
                            log::error!("invalid push header received: err={:?}", err);
                        }
                    }
                }
            }
            _ => log::error!("received an invalid frame for this state, or a malformed frame"),
        }
    }

    pub fn add_push<P: Pack>(
        &mut self,
        priority: Priority,
        tag: PushTag,
        header: &PushHeader,
        payloads: impl IntoIterator<Item = P>,
    ) {
        if self.pending.push(priority, tag.clone(), header, payloads) {
            self.push_window.add_push(tag);
        } else {
            self.dropped.push(tag);
            log::warn!("dropping a push because the pending buffer is full");
        }
    }

    pub fn write_frame<'a>(&mut self, buf: &mut [u8]) -> usize {
        let ret = {
            let mut writer = pigeon::Writer::new(&mut *buf);
            let header = FrameHeader::Push {
                push_id: 0,
                last_push_id: 0,
                push_window: 0,
                payload_count: 0,
            };
            writer.write(&header).unwrap();
            writer.pad_align(); // TODO: may not be ideal
            log::trace!("flushing high priority pushes");
            for (tag, bit_offset, buf) in self.pending.drain(Priority::High) {
                if writer.fits_bytes(buf) {
                    let buflen = buf.len();
                    log::trace!(
                        "flushing push: len={} bit_offset={} tag={:?}",
                        buflen,
                        bit_offset,
                        tag,
                    );
                    log::trace!("flushing push: buf={:x?}", &buf[..buflen]);
                    if bit_offset > 0 {
                        writer.write_bytes(&buf[..buflen - 1]).unwrap();
                        writer
                            .write_u8_bits(bit_offset, buf[buflen - 1] >> (8 - bit_offset))
                            .unwrap();
                    } else {
                        writer.write_bytes(buf).unwrap();
                    }
                } else {
                    self.dropped.push(tag);
                    log::warn!("dropping a high-priority push because the packet is full");
                }
            }
            log::trace!("flushing low priority pushes");
            for (tag, bit_offset, buf) in self.pending.drain(Priority::Low) {
                if writer.fits_bytes(buf) {
                    let buflen = buf.len();
                    log::trace!(
                        "flushing push: len={} bit_offset={} tag={:?}",
                        buflen,
                        bit_offset,
                        tag,
                    );
                    log::trace!("flushing push: buf={:x?}", &buf[..buflen]);
                    if bit_offset > 0 {
                        writer.write_bytes(&buf[..buflen - 1]).unwrap();
                        writer
                            .write_u8_bits(bit_offset, buf[buflen - 1] >> (8 - bit_offset))
                            .unwrap();
                    } else {
                        writer.write_bytes(buf).unwrap();
                    }
                } else {
                    self.dropped.push(tag);
                    log::warn!("dropping a low-priority push because the packet is full");
                }
            }
            log::trace!("flushing bulk pushes");
            for (tag, bit_offset, buf) in self.pending.drain(Priority::Bulk) {
                if writer.fits_bytes(buf) {
                    let buflen = buf.len();
                    log::trace!(
                        "flushing push: len={} bit_offset={} tag={:?}",
                        buflen,
                        bit_offset,
                        tag,
                    );
                    log::trace!("flushing push: buf={:x?}", &buf[..buflen]);
                    if bit_offset > 0 {
                        writer.write_bytes(&buf[..buflen - 1]).unwrap();
                        writer
                            .write_u8_bits(bit_offset, buf[buflen - 1] >> (8 - bit_offset))
                            .unwrap();
                    } else {
                        writer.write_bytes(buf).unwrap();
                    }
                } else {
                    self.dropped.push(tag);
                    log::warn!("dropping a bulk push because the packet is full");
                }
            }
            writer.finish().unwrap()
        };
        {
            let mut writer = pigeon::Writer::new(&mut *buf);
            let header = self.push_window.next_header();
            writer.write(header).unwrap();
            writer.finish().unwrap();
        }
        let push_id = self.push_window.last_sent_id();
        self.push_timing.push_back((push_id, Instant::now()));
        log::trace!("writing frame(len={}): {:x?}", ret, &buf[0..ret]);
        ret
    }
}
