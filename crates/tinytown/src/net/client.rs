use {
    cgmath::{Point3, Vector3},
    crc::crc16,
    std::{
        io::ErrorKind,
        net::{ToSocketAddrs, UdpSocket},
        time::{Duration, Instant},
    },
    tinytown_protocol::UnitVector,
};

use crate::{
    brick::{BrickColorId, BrickDataId, BrickOrientation},
    components::NetId,
    consts::{CLIENT_TIMEOUT_MS, CONTROLS_QUEUE_SIZE},
    controls::ControlState,
    vox::Vox,
};

use super::{
    connection::Connection,
    controls::ControlsQueue,
    data::{BrickInfo, FullPlayerInfo, PlayerGhostInfo},
    pending::Priority,
    protocol::{
        ClientCookie, FrameHeader, GhostHeader, JoinRejectReason, PushHeader, ServerChallenge,
    },
    push::{PushEvent, PushTag},
};

#[derive(Debug, Clone)]
pub enum ConnectionEvent {
    Joined {
        server_tick: u32,
    },
    JoinRejected {
        reason: JoinRejectReason,
    },
    DeleteGhost {
        net_id: NetId,
    },
    UpdatePlayerGhost {
        net_id: NetId,
        position: Option<Point3<f32>>,
        facing: Option<Vector3<f32>>,
    },
    UpdateGhostBrickGhost {
        net_id: NetId,
        color_id: BrickColorId,
        data_id: BrickDataId,
        orientation: BrickOrientation,
        pos: Vox,
    },
    UpdateBrickGhost {
        net_id: NetId,
        color_id: BrickColorId,
        data_id: BrickDataId,
        orientation: BrickOrientation,
        pos: Vox,
    },
    UpdateOwnPlayer {
        tick: u32,
        player_info: FullPlayerInfo,
    },
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum State {
    SendingJoinRequest,
    SendingJoinResponse,
    Connected,
}

pub struct ClientConnection {
    sock: UdpSocket,
    name: String,
    state: State,
    last_send_time: Instant,
    last_activity: Instant,
    challenge: ServerChallenge,
    client_cookie: ClientCookie,
    server_tick: u32,
    connection: Connection,
    controls: ControlsQueue,
    player_crc: u16,
}

impl ClientConnection {
    pub fn connect<A: ToSocketAddrs>(addr: A, name: &str) -> anyhow::Result<ClientConnection> {
        let name = name.to_owned();
        let sock = UdpSocket::bind("0.0.0.0:0")?;
        sock.set_nonblocking(true)?;
        sock.connect(addr)?;
        let client_cookie = super::protocol::generate_client_cookie()?;
        Ok(ClientConnection {
            sock,
            name,
            state: State::SendingJoinRequest,
            last_send_time: Instant::now() - Duration::from_secs(3600), // a bit of a hack...
            last_activity: Instant::now(),
            challenge: [0; 8],
            client_cookie,
            server_tick: 0, // TODO: keep up-to-date
            connection: Connection::initiating(),
            controls: ControlsQueue::new(CONTROLS_QUEUE_SIZE),
            player_crc: 0,
        })
    }

    pub fn server_tick(&self) -> u32 {
        self.server_tick
    }

    pub fn push_controls(&mut self, tick: u32, controls: ControlState, facing: Vector3<f32>) {
        self.controls
            .submit(tick, self.player_crc, controls, facing);
    }

    pub fn update_player_state(&mut self, player_info: &FullPlayerInfo) {
        // TODO: maybe add rotation?
        let mut buf = [0; 128];
        pigeon::Writer::with(&mut buf[..], |f| f.write(player_info)).unwrap();
        self.player_crc = crc16::checksum_x25(&buf[..]);
    }

    pub fn process(&mut self, mut cb: impl FnMut(ConnectionEvent)) -> anyhow::Result<()> {
        let mut buf = [0; 1024];
        loop {
            let pkt = match self.sock.recv(&mut buf[..]) {
                Ok(size) => &buf[..size],
                Err(err) => {
                    if err.kind() == ErrorKind::WouldBlock {
                        break;
                    } else {
                        return Err(err.into());
                    }
                }
            };
            log::debug!("packet received from host");
            log::trace!("packet data(len={}): {:x?}", pkt.len(), pkt);
            self.last_activity = Instant::now();
            let server_tick = &mut self.server_tick;
            let controls = &mut self.controls;
            let mut reader = pigeon::Reader::new(pkt);
            if let Ok(header) = reader.read::<FrameHeader>() {
                match header {
                    FrameHeader::JoinChallenge { challenge } => {
                        self.challenge.copy_from_slice(&challenge[..]);
                        self.state = State::SendingJoinResponse;
                    }
                    FrameHeader::JoinAccept { server_tick } => {
                        self.state = State::Connected;
                        self.server_tick = server_tick;
                        cb(ConnectionEvent::Joined { server_tick });
                    }
                    FrameHeader::JoinReject { reason } => {
                        cb(ConnectionEvent::JoinRejected { reason });
                    }
                    frame_header => {
                        let push_status_cb = |push_event| {
                            log::debug!("push status cb {:?}", push_event);
                            match push_event {
                                PushEvent::Delivered(push_info) => {
                                    match push_info {
                                        PushTag::ClientTick {
                                            tick,
                                            sent_controls,
                                        } => {
                                            for i in 0..CONTROLS_QUEUE_SIZE {
                                                if sent_controls[i] {
                                                    let t = tick - i as u32;
                                                    log::trace!("acked controls frame for tick {}, offset {}", t, i);
                                                    controls.discard(t);
                                                }
                                            }
                                        }
                                        _ => (),
                                    }
                                }
                                PushEvent::Lost(_push_info) => (),
                            }
                        };
                        let push_recv_cb =
                            |reader: &mut pigeon::Reader, push_header: PushHeader| {
                                log::debug!("push recv cb {:?}", push_header);
                                match push_header {
                                    PushHeader::ServerTick { tick } => {
                                        log::trace!("  server tick: tick={}", tick);
                                        *server_tick = tick;
                                    }
                                    PushHeader::Ghost { net_id, header } => {
                                        log::trace!(
                                            "  ghost: net_id={} header={:?}",
                                            net_id,
                                            header
                                        );
                                        match header {
                                            GhostHeader::None => {
                                                log::trace!("    none (deletion request)");
                                                cb(ConnectionEvent::DeleteGhost {
                                                    net_id: net_id.into(),
                                                });
                                            }
                                            GhostHeader::Player => {
                                                if let Ok(PlayerGhostInfo { position, facing }) =
                                                    reader.read()
                                                {
                                                    log::trace!(
                                                        "    player: position={:?} facing={:?}",
                                                        position,
                                                        facing
                                                    );
                                                    cb(ConnectionEvent::UpdatePlayerGhost {
                                                        net_id: net_id.into(),
                                                        position,
                                                        facing,
                                                    });
                                                } else {
                                                    log::error!(
                                                        "    failed to read associated data"
                                                    );
                                                }
                                            }
                                            GhostHeader::GhostBrick => {
                                                if let Ok(BrickInfo {
                                                    color_id,
                                                    data_id,
                                                    pos,
                                                    orientation,
                                                }) = reader.read()
                                                {
                                                    log::trace!("    ghost brick: color_id={:?} data_id={:?} orientation={:?} pos={:?}", color_id, data_id, orientation, pos);
                                                    cb(ConnectionEvent::UpdateGhostBrickGhost {
                                                        net_id: net_id.into(),
                                                        color_id,
                                                        data_id,
                                                        orientation,
                                                        pos,
                                                    });
                                                } else {
                                                    log::error!(
                                                        "    failed to read associated data"
                                                    );
                                                }
                                            }
                                            GhostHeader::Brick => {
                                                if let Ok(BrickInfo {
                                                    color_id,
                                                    data_id,
                                                    pos,
                                                    orientation,
                                                }) = reader.read()
                                                {
                                                    log::trace!("    brick: color_id={:?} data_id={:?} orientation={:?} pos={:?}", color_id, data_id, orientation, pos);
                                                    cb(ConnectionEvent::UpdateBrickGhost {
                                                        net_id: net_id.into(),
                                                        color_id,
                                                        data_id,
                                                        orientation,
                                                        pos,
                                                    });
                                                } else {
                                                    log::error!(
                                                        "    failed to read associated data"
                                                    );
                                                }
                                            }
                                        }
                                    }
                                    PushHeader::PlayerUpdate { tick } => {
                                        if let Ok(player_info) = reader.read() {
                                            cb(ConnectionEvent::UpdateOwnPlayer {
                                                tick,
                                                player_info,
                                            });
                                        }
                                    }
                                    _ => (),
                                }
                                Ok(())
                            };
                        self.connection.on_recv(
                            frame_header,
                            &mut reader,
                            push_status_cb,
                            push_recv_cb,
                        );
                    }
                }
            } else {
                log::warn!("ignoring malformed packet from host");
            }
        }
        if self.last_activity.elapsed() > Duration::from_millis(CLIENT_TIMEOUT_MS) {
            log::error!("last activity from the server too far in the past! disconnected!");
            // TODO: disconnect
        }
        Ok(())
    }

    pub fn flush_frames(&mut self) -> anyhow::Result<()> {
        let mut buf = [0; 508];
        match self.state {
            State::SendingJoinRequest => {
                if self.last_send_time.elapsed() > Duration::from_millis(250) {
                    let frame = FrameHeader::JoinRequest {
                        client_cookie: self.client_cookie,
                    };
                    let len = pigeon::pack_buffer(&frame, &mut buf[..]).unwrap();
                    self.sock.send(&buf[..len])?;
                    self.last_send_time = Instant::now();
                }
            }
            State::SendingJoinResponse => {
                if self.last_send_time.elapsed() > Duration::from_millis(250) {
                    let frame = FrameHeader::JoinResponse {
                        client_cookie: self.client_cookie,
                        challenge: self.challenge,
                        name: self.name.clone(),
                    };
                    let len = pigeon::pack_buffer(&frame, &mut buf[..]).unwrap();
                    self.sock.send(&buf[..len])?;
                    self.last_send_time = Instant::now();
                }
            }
            State::Connected => {
                let last_tick = self.controls.last_tick();
                let my_controls = &self.controls;
                let mut sent_controls = [false; CONTROLS_QUEUE_SIZE];
                log::trace!("writing controls for tick {}", last_tick);
                let controls: Vec<_> = my_controls
                    .iter()
                    .map(|(tick, offset, entry)| {
                        log::trace!(
                            "writing controls for tick {}: offset={} controls={:?} facing={:?} player_crc={}",
                            tick,
                            offset,
                            entry.controls,
                            entry.facing,
                            entry.player_crc
                        );
                        sent_controls[offset] = true;
                        (
                            pigeon::U4(offset as u8),
                            entry.player_crc,
                            UnitVector(entry.facing),
                            &entry.controls,
                        )
                    })
                    .collect();
                let header = PushHeader::ClientTick {
                    tick: last_tick,
                    controls_count: controls.len() as u8,
                };
                self.connection.add_push(
                    Priority::High,
                    PushTag::ClientTick {
                        tick: last_tick,
                        sent_controls,
                    },
                    &header,
                    controls,
                );
                let len = self.connection.write_frame(&mut buf[..]);
                self.sock.send(&buf[..len])?;
            }
        }
        Ok(())
    }

    pub fn is_connected(&self) -> bool {
        if let State::Connected = self.state {
            true
        } else {
            false
        }
    }
}
