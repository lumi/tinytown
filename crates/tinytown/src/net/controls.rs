use cgmath::Vector3;

use crate::controls::ControlState;

use super::tick_buffer::TickBuffer;

#[derive(Debug, Clone)]
pub struct ControlsQueueEntry {
    pub player_crc: u16,
    pub controls: ControlState,
    pub facing: Vector3<f32>,
}

#[derive(Debug, Clone)]
pub struct ControlsQueue {
    buffer: TickBuffer<ControlsQueueEntry>,
}

impl ControlsQueue {
    pub fn new(max_size: usize) -> ControlsQueue {
        let buffer = TickBuffer::new(max_size);
        ControlsQueue { buffer }
    }

    pub fn submit(
        &mut self,
        tick: u32,
        player_crc: u16,
        controls: ControlState,
        facing: Vector3<f32>,
    ) {
        self.buffer.add(
            tick,
            ControlsQueueEntry {
                player_crc,
                controls,
                facing,
            },
        );
    }

    pub fn discard(&mut self, tick: u32) {
        self.buffer.remove(tick);
    }

    pub fn last_tick(&self) -> u32 {
        self.buffer.last_tick()
    }

    pub fn iter(&self) -> impl Iterator<Item = (u32, usize, &ControlsQueueEntry)> {
        self.buffer.iter()
    }

    pub fn at_tick(&self, tick: u32) -> Option<&ControlsQueueEntry> {
        self.buffer.at_tick(tick)
    }
}
