use {
    pigeon::{Pack, Unpack},
    sha2::{Digest, Sha256},
    std::{convert::TryInto, net::IpAddr},
};

pub const PROTOCOL_VERSION: u8 = 0;

pub type ClientCookie = [u8; 8];
pub type ServerChallenge = [u8; 8];

// TODO: i'm sure this can be done better
pub fn generate_client_cookie() -> Result<ClientCookie, getrandom::Error> {
    let mut cookie = [0; 8];
    getrandom::getrandom(&mut cookie[..])?;
    Ok(cookie)
}

// TODO: i'm sure this can be done better
pub fn generate_server_challenge(
    client_addr: IpAddr,
    client_cookie: ClientCookie,
) -> ServerChallenge {
    let mut hasher = Sha256::new();
    hasher.input(&client_cookie[..]);
    match client_addr {
        IpAddr::V4(addr) => {
            hasher.input(&addr.octets()[..]);
        }
        IpAddr::V6(addr) => {
            hasher.input(&addr.octets()[..]);
        }
    }
    let result = hasher.result();
    result[..8].try_into().unwrap()
}

#[derive(Debug, Clone)]
pub enum FrameHeader {
    JoinRequest {
        client_cookie: ClientCookie,
    },
    JoinChallenge {
        challenge: ServerChallenge,
    },
    JoinResponse {
        client_cookie: ClientCookie,
        challenge: ServerChallenge,
        name: String,
    },
    JoinAccept {
        server_tick: u32,
    },
    JoinReject {
        reason: JoinRejectReason,
    },
    Push {
        push_id: u32,
        last_push_id: u32,
        push_window: u32,
        payload_count: u8,
    },
}

impl Pack for FrameHeader {
    fn pack<T: pigeon::Target>(&self, writer: &mut pigeon::Writer<T>) -> pigeon::WriteResult<()> {
        writer.write(b't')?;
        writer.write(b't')?;
        writer.write(PROTOCOL_VERSION)?;
        match self {
            FrameHeader::JoinRequest { client_cookie } => {
                writer.write(1u8)?;
                writer.write_bytes(client_cookie)?;
            }
            FrameHeader::JoinChallenge { challenge } => {
                writer.write(2u8)?;
                writer.write_bytes(challenge)?;
            }
            FrameHeader::JoinResponse {
                client_cookie,
                challenge,
                name,
            } => {
                writer.write(3u8)?;
                writer.write_bytes(client_cookie)?;
                writer.write_bytes(challenge)?;
                let name_bytes = name.as_bytes();
                writer.write(name_bytes.len() as u8)?;
                writer.write_bytes(name_bytes)?;
            }
            FrameHeader::JoinAccept { server_tick } => {
                writer.write(4u8)?;
                writer.write(server_tick)?;
            }
            FrameHeader::JoinReject { reason } => {
                writer.write(5u8)?;
                writer.write(reason)?;
            }
            FrameHeader::Push {
                push_id,
                last_push_id,
                push_window,
                payload_count,
            } => {
                writer.write(6u8)?;
                writer.write(push_id)?;
                writer.write(last_push_id)?;
                writer.write(push_window)?;
                writer.write(payload_count)?;
            }
        }
        Ok(())
    }
}

impl<'a> Unpack<'a> for FrameHeader {
    fn unpack(reader: &mut pigeon::Reader) -> pigeon::ReadResult<FrameHeader> {
        reader.expect(b't')?;
        reader.expect(b't')?;
        reader.expect(PROTOCOL_VERSION)?;
        let frame_type_id: u8 = reader.read()?;
        match frame_type_id {
            1 => {
                let mut client_cookie = [0; 8];
                reader.read_bytes_to(&mut client_cookie[..])?;
                Ok(FrameHeader::JoinRequest { client_cookie })
            }
            2 => {
                let mut challenge = [0; 8];
                reader.read_bytes_to(&mut challenge[..])?;
                Ok(FrameHeader::JoinChallenge { challenge })
            }
            3 => {
                let mut client_cookie = [0; 8];
                let mut challenge = [0; 8];
                reader.read_bytes_to(&mut client_cookie[..])?;
                reader.read_bytes_to(&mut challenge[..])?;
                let mut name_bytes = [0; 256];
                let name_bytes_count = reader.read::<u8>()? as usize;
                let name_bytes_unaligned = reader.read_bytes(name_bytes_count)?;
                name_bytes_unaligned.copy_to_slice(&mut name_bytes[..name_bytes_count]);
                let name = String::from_utf8(name_bytes[..name_bytes_count].to_vec())
                    .map_err(|_| pigeon::ReadError::UnexpectedData)?;
                Ok(FrameHeader::JoinResponse {
                    client_cookie,
                    challenge,
                    name,
                })
            }
            4 => {
                let server_tick: u32 = reader.read()?;
                Ok(FrameHeader::JoinAccept { server_tick })
            }
            5 => {
                let reason = reader.read()?;
                Ok(FrameHeader::JoinReject { reason })
            }
            6 => {
                let push_id: u32 = reader.read()?;
                let last_push_id: u32 = reader.read()?;
                let push_window: u32 = reader.read()?;
                let payload_count: u8 = reader.read()?;
                Ok(FrameHeader::Push {
                    push_id,
                    last_push_id,
                    push_window,
                    payload_count,
                })
            }
            _ => Err(pigeon::ReadError::UnexpectedData),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum JoinRejectReason {
    Full = 1,
}

impl Pack for JoinRejectReason {
    fn pack<T: pigeon::Target>(&self, writer: &mut pigeon::Writer<T>) -> pigeon::WriteResult<()> {
        writer.write(*self as u8)
    }
}

impl<'a> Unpack<'a> for JoinRejectReason {
    fn unpack(reader: &mut pigeon::Reader<'a>) -> pigeon::ReadResult<JoinRejectReason> {
        JoinRejectReason::from_u8(reader.read::<u8>()?)
            .ok_or_else(|| pigeon::ReadError::UnexpectedData)
    }
}

impl JoinRejectReason {
    pub fn from_u8(byte: u8) -> Option<JoinRejectReason> {
        match byte {
            1 => Some(JoinRejectReason::Full),
            _ => None,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum PushKind {
    Event = 0,
    ClientTick = 1,
    Ghost = 2,
    ServerTick = 3,
    PlayerUpdate = 4,
}

impl Pack for PushKind {
    fn pack<T: pigeon::Target>(&self, writer: &mut pigeon::Writer<T>) -> pigeon::WriteResult<()> {
        writer.write(*self as u8)
    }
}

impl<'a> Unpack<'a> for PushKind {
    fn unpack(reader: &mut pigeon::Reader<'a>) -> pigeon::ReadResult<PushKind> {
        let kind_u8 = reader.read::<u8>()?;
        match kind_u8 {
            0 => Ok(PushKind::Event),
            1 => Ok(PushKind::ClientTick),
            2 => Ok(PushKind::Ghost),
            3 => Ok(PushKind::ServerTick),
            4 => Ok(PushKind::PlayerUpdate),
            _ => Err(pigeon::ReadError::UnexpectedData),
        }
    }
}

#[derive(Debug, Clone)]
pub enum GhostHeader {
    None,
    Player,
    Brick,
    GhostBrick,
}

impl Pack for GhostHeader {
    fn pack<T: pigeon::Target>(&self, writer: &mut pigeon::Writer<T>) -> pigeon::WriteResult<()> {
        match *self {
            GhostHeader::None => {
                writer.write(pigeon::U2(0))?;
            }
            GhostHeader::Player => {
                writer.write(pigeon::U2(1))?;
            }
            GhostHeader::Brick => {
                writer.write(pigeon::U2(2))?;
            }
            GhostHeader::GhostBrick => {
                writer.write(pigeon::U2(3))?;
            }
        }
        Ok(())
    }
}

impl<'a> Unpack<'a> for GhostHeader {
    fn unpack(reader: &mut pigeon::Reader<'a>) -> pigeon::ReadResult<GhostHeader> {
        let pigeon::U2(tynum) = reader.read()?;
        match tynum {
            0 => Ok(GhostHeader::None),
            1 => Ok(GhostHeader::Player),
            2 => Ok(GhostHeader::Brick),
            3 => Ok(GhostHeader::GhostBrick),
            _ => unreachable!(), // out of range for a 2-bit uint
        }
    }
}

#[derive(Debug, Clone)]
pub enum PushHeader {
    Event { event_id: u32 },
    ClientTick { tick: u32, controls_count: u8 },
    Ghost { net_id: u32, header: GhostHeader },
    ServerTick { tick: u32 },
    PlayerUpdate { tick: u32 },
}

impl Pack for PushHeader {
    fn pack<T: pigeon::Target>(&self, writer: &mut pigeon::Writer<T>) -> pigeon::WriteResult<()> {
        writer.write(self.kind())?;
        match self {
            PushHeader::Event { event_id } => {
                writer.write(event_id)?;
            }
            PushHeader::ClientTick {
                tick,
                controls_count,
            } => {
                writer.write(tick)?;
                writer.write(controls_count)?;
            }
            PushHeader::Ghost { net_id, header } => {
                writer.write(net_id)?;
                writer.write(header)?;
            }
            PushHeader::ServerTick { tick } => {
                writer.write(tick)?;
            }
            PushHeader::PlayerUpdate { tick } => {
                writer.write(tick)?;
            }
        }
        Ok(())
    }
}

impl<'a> Unpack<'a> for PushHeader {
    fn unpack(reader: &mut pigeon::Reader<'a>) -> pigeon::ReadResult<PushHeader> {
        let kind = reader.read()?;
        match kind {
            PushKind::Event => {
                let event_id = reader.read()?;
                Ok(PushHeader::Event { event_id })
            }
            PushKind::ClientTick => {
                let tick = reader.read()?;
                let controls_count = reader.read()?;
                Ok(PushHeader::ClientTick {
                    tick,
                    controls_count,
                })
            }
            PushKind::Ghost => {
                let net_id = reader.read()?;
                let header = reader.read()?;
                Ok(PushHeader::Ghost { net_id, header })
            }
            PushKind::ServerTick => {
                let tick = reader.read()?;
                Ok(PushHeader::ServerTick { tick })
            }
            PushKind::PlayerUpdate => {
                let tick = reader.read()?;
                Ok(PushHeader::PlayerUpdate { tick })
            }
        }
    }
}

impl PushHeader {
    pub fn kind(&self) -> PushKind {
        match self {
            PushHeader::Event { .. } => PushKind::Event,
            PushHeader::ClientTick { .. } => PushKind::ClientTick,
            PushHeader::Ghost { .. } => PushKind::Ghost,
            PushHeader::ServerTick { .. } => PushKind::ServerTick,
            PushHeader::PlayerUpdate { .. } => PushKind::PlayerUpdate,
        }
    }
}
