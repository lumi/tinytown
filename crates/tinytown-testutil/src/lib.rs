use {
    cgmath::{prelude::*, Point2, Point3, Vector2, Vector3},
    core::{f32::consts::PI, ops::Range},
    proptest::{prop_compose, proptest},
};

#[cfg(test)]
use approx::assert_ulps_eq;

// 3d
prop_compose! {
    pub fn prop_vec3
        (xr: Range<f32>, yr: Range<f32>, zr: Range<f32>)
        (x in xr, y in yr, z in zr)
        -> Vector3<f32> {
        Vector3::new(x, y, z)
    }
}

prop_compose! {
    pub fn prop_point3
        (xr: Range<f32>, yr: Range<f32>, zr: Range<f32>)
        (x in xr, y in yr, z in zr) -> Point3<f32> {
        Point3::new(x, y, z)
    }
}

prop_compose! {
    pub fn prop_vec3_all(ar: Range<f32>)(v in prop_vec3(ar.clone(), ar.clone(), ar)) -> Vector3<f32> {
        v
    }
}

prop_compose! {
    pub fn prop_point3_all(ar: Range<f32>)(v in prop_vec3_all(ar)) -> Point3<f32> {
        Point3::from_vec(v)
    }
}

prop_compose! {
    pub fn prop_vec3_unit()(yaw in 0. .. PI * 2., pitch in 0. .. PI * 2.) -> Vector3<f32> {
        Vector3::new(pitch.cos() * yaw.cos(), pitch.sin(), pitch.cos() * yaw.sin())
    }
}

prop_compose! {
    pub fn prop_point3_between(min: Point3<f32>, max: Point3<f32>)
                              (p in prop_point3(min.x .. max.x, min.y .. max.y, min.z .. max.z)) -> Point3<f32> {
        p
    }
}

// 2d
prop_compose! {
    pub fn prop_vec2
        (xr: Range<f32>, yr: Range<f32>)
        (x in xr, y in yr)
        -> Vector2<f32> {
        Vector2::new(x, y)
    }
}

prop_compose! {
    pub fn prop_point2
        (xr: Range<f32>, yr: Range<f32>)
        (x in xr, y in yr)
        -> Point2<f32> {
        Point2::new(x, y)
    }
}

prop_compose! {
    pub fn prop_vec2_all(ar: Range<f32>)(v in prop_vec2(ar.clone(), ar)) -> Vector2<f32> {
        v
    }
}

prop_compose! {
    pub fn prop_point2_all(ar: Range<f32>)(v in prop_point2(ar.clone(), ar)) -> Point2<f32> {
        v
    }
}

prop_compose! {
    pub fn prop_vec2_unit()(theta in 0. .. 2. * PI) -> Vector2<f32> {
        Vector2::new(theta.cos(), theta.sin())
    }
}

prop_compose! {
    pub fn prop_point2_between(min: Point2<f32>, max: Point2<f32>)(p in prop_point2(min.x .. max.x, min.y .. max.y)) -> Point2<f32> {
        p
    }
}

// tests
proptest! {
    #[test]
    fn prop_vec3_unit_always_unit(unit in prop_vec3_unit()) {
        assert_ulps_eq!(unit.magnitude2(), 1.);
    }
}
