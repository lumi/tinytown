use cgmath::{Point3, Vector3};

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct RaycastHit<V> {
    pub point: Point3<f32>,
    pub normal: Vector3<f32>,
    pub distance: f32,
    pub value: V,
}
