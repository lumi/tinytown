#![deny(missing_docs)]

/// A collision mask
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct CollisionMask(u8);

impl CollisionMask {
    /// The empty collision mask
    pub const EMPTY: CollisionMask = CollisionMask(0);
    /// The collision mask for physical entities
    pub const PHYSICAL: CollisionMask = CollisionMask(1);
    /// The collision mask for bricks
    pub const BRICK: CollisionMask = CollisionMask(2);

    /// Checks whether the target mask contains any elements this mask has.
    ///
    /// ```rust
    /// # use tinytown_collision::CollisionMask;
    /// assert!(!CollisionMask::EMPTY.contains(CollisionMask::PHYSICAL));
    /// assert!(!CollisionMask::BRICK.contains(CollisionMask::PHYSICAL));
    /// ```
    #[inline(always)]
    pub fn contains(self, other: CollisionMask) -> bool {
        self.0 & other.0 != 0
    }

    /// Creates the union of two masks.
    ///
    /// ```rust
    /// # use tinytown_collision::CollisionMask;
    ///
    /// let bricks_and_physicals = CollisionMask::BRICK.union(CollisionMask::PHYSICAL);
    ///
    /// assert!(bricks_and_physicals.contains(CollisionMask::BRICK));
    /// assert!(bricks_and_physicals.contains(CollisionMask::PHYSICAL));
    /// ```
    #[inline(always)]
    pub fn union(self, other: CollisionMask) -> CollisionMask {
        CollisionMask(self.0 | other.0)
    }

    /// Creates the intersection of two masks.
    ///
    /// ```rust
    /// # use tinytown_collision::CollisionMask;
    ///
    /// let bricks_and_physicals = CollisionMask::BRICK.union(CollisionMask::PHYSICAL);
    ///
    /// let just_bricks = bricks_and_physicals.intersect(CollisionMask::BRICK);
    ///
    /// assert!(just_bricks.contains(CollisionMask::BRICK));
    /// assert!(!just_bricks.contains(CollisionMask::PHYSICAL));
    /// ```
    #[inline(always)]
    pub fn intersect(self, other: CollisionMask) -> CollisionMask {
        CollisionMask(self.0 & other.0)
    }
}
