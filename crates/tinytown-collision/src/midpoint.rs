#![deny(missing_docs)]

use cgmath::Point3;

/// A trait for shapes which have a midpoint.
pub trait Midpoint {
    /// Return the midpoint of a shape.
    fn midpoint(&self) -> Point3<f32>;
}
