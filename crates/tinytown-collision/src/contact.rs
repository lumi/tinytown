use cgmath::Vector3;

#[derive(Debug, Clone, Copy)]
pub struct ContactPair<V: Copy + PartialEq> {
    pub value_a: V,
    pub value_b: V,
    pub contact: Contact,
}

#[derive(Debug, Clone, Copy)]
pub struct Contact {
    pub depth: f32,
    pub normal: Vector3<f32>,
}
