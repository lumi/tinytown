#![deny(missing_docs)]

use cgmath::{prelude::*, Point3, Vector3};

use super::Midpoint;

/// Trait for shapes which have a support function.
///
/// The support point of a shape in a direction is the furthest point on that shape in that
/// direction.
pub trait Support {
    /// Calculates the support point in a direction.
    fn support_point(&self, dir: Vector3<f32>) -> Point3<f32>;
}

/// The Minkowski difference of two shapes with support functions.
///
/// This is the Minkowski sum of the first shape with the negation of the second shape.
#[derive(Copy, Clone)]
pub struct MinkowskiDifference<'a, 'b, A: Support + 'a, B: Support + 'b> {
    /// The first shape
    pub first: &'a A,
    /// The second shape
    pub second: &'b B,
}

impl<'a, 'b, A: Support + 'a, B: Support + 'b> MinkowskiDifference<'a, 'b, A, B> {
    /// Constructs a MinowskiDifference with these two shapes.
    pub fn new(first: &'a A, second: &'b B) -> MinkowskiDifference<'a, 'b, A, B> {
        MinkowskiDifference { first, second }
    }
}

impl<'a, 'b, A: Support + Midpoint + 'a, B: Support + Midpoint + 'b> Midpoint
    for MinkowskiDifference<'a, 'b, A, B>
{
    fn midpoint(&self) -> Point3<f32> {
        Point3::from_vec(self.first.midpoint() - self.second.midpoint())
    }
}

impl<'a, 'b, A: Support + 'a, B: Support + 'b> Support for MinkowskiDifference<'a, 'b, A, B> {
    fn support_point(&self, dir: Vector3<f32>) -> Point3<f32> {
        Point3::from_vec(self.first.support_point(dir) - self.second.support_point(-dir))
    }
}
