#![deny(missing_docs)]

use {
    approx::{AbsDiffEq, RelativeEq, UlpsEq},
    cgmath::{prelude::*, Point3, Vector3},
    core::ops::{Add, AddAssign, Sub, SubAssign},
};

use crate::ray::Ray;

/// An axis-aligned bounding box, defined by its bounds.
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Aabb {
    /// The minimum point of the AABB.
    pub min: Point3<f32>,
    /// The maximum point of the AABB.
    pub max: Point3<f32>,
}

impl Aabb {
    /// Construct a new AABB from minimum and maximum bounds.
    /// It is assumed that the minimum bound's components are all less than or equal to the maximum
    /// bound's components.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #    tinytown_collision::Aabb,
    /// #    cgmath::{Point3, Vector3},
    /// #    approx::assert_ulps_eq,
    /// # };
    /// let aabb = Aabb::new(Point3::new(2., 1., 0.), Point3::new(3., 1.5, 1.));
    ///
    /// assert_ulps_eq!(aabb.min, Point3::new(2., 1., 0.));
    /// assert_ulps_eq!(aabb.max, Point3::new(3., 1.5, 1.));
    /// ```
    pub fn new(min: Point3<f32>, max: Point3<f32>) -> Aabb {
        #[cfg(test)]
        assert!(min.x <= max.x && min.y <= max.y && min.z <= max.z);
        Aabb { min, max }
    }

    /// Construct a new AABB from a minimum bound and a size.
    /// It is assumed that all components of the size vector are positive or zero.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #    tinytown_collision::Aabb,
    /// #    cgmath::{Point3, Vector3},
    /// #    approx::assert_ulps_eq,
    /// # };
    /// let aabb = Aabb::with_size(Point3::new(2., 1., 0.), Vector3::new(3., 1.5, 1.));
    ///
    /// assert_ulps_eq!(aabb.min, Point3::new(2., 1., 0.));
    /// assert_ulps_eq!(aabb.max, Point3::new(5., 2.5, 1.));
    /// ```
    pub fn with_size(min: Point3<f32>, size: Vector3<f32>) -> Aabb {
        #[cfg(test)]
        assert!(size.x >= 0. && size.y >= 0. && size.z >= 0.);
        Aabb {
            min,
            max: min + size,
        }
    }

    /// Construct a new AABB from a center point and a radius.
    /// It is assumed that the radius' components are positive or zero.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #    tinytown_collision::Aabb,
    /// #    cgmath::{Point3, Vector3},
    /// #    approx::assert_ulps_eq,
    /// # };
    /// let aabb = Aabb::new_centered(Point3::new(2., 1., 0.), Vector3::new(1., 0.5, 1.));
    ///
    /// assert_ulps_eq!(aabb.min, Point3::new(1., 0.5, -1.));
    /// assert_ulps_eq!(aabb.max, Point3::new(3., 1.5, 1.));
    /// ```
    pub fn new_centered(center: Point3<f32>, radius: Vector3<f32>) -> Aabb {
        #[cfg(test)]
        assert!(radius.x >= 0. && radius.y >= 0. && radius.z >= 0.);
        Aabb {
            min: center - radius,
            max: center + radius,
        }
    }

    /// Get the center point of the AABB.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #    tinytown_collision::Aabb,
    /// #    cgmath::{Point3, Vector3},
    /// #    approx::assert_ulps_eq,
    /// # };
    /// let aabb = Aabb::new(Point3::new(1., 2., 3.), Point3::new(2., 5., 5.));
    ///
    /// assert_ulps_eq!(aabb.center(), Point3::new(1.5, 3.5, 4.));
    /// ```
    pub fn center(self) -> Point3<f32> {
        self.min.midpoint(self.max)
    }

    /// Get the radius of the AABB.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #    tinytown_collision::Aabb,
    /// #    cgmath::{Point3, Vector3},
    /// #    approx::assert_ulps_eq,
    /// # };
    /// let aabb = Aabb::new(Point3::new(1., 2., 3.), Point3::new(2., 5., 5.));
    ///
    /// assert_ulps_eq!(aabb.radius(), Vector3::new(0.5, 1.5, 1.));
    /// ```
    pub fn radius(self) -> Vector3<f32> {
        self.max.to_vec() * 0.5 - self.min.to_vec() * 0.5
    }

    /// Get the size of the AABB.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #    tinytown_collision::Aabb,
    /// #    cgmath::{Point3, Vector3},
    /// #    approx::assert_ulps_eq,
    /// # };
    /// let aabb = Aabb::new(Point3::new(1., 2., 3.), Point3::new(2., 5., 5.));
    ///
    /// assert_ulps_eq!(aabb.size(), Vector3::new(1., 3., 2.));
    /// ```
    pub fn size(self) -> Vector3<f32> {
        self.max.to_vec() - self.min.to_vec()
    }

    /// Get the intersection AABB of two AABBs.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #    tinytown_collision::Aabb,
    /// #    cgmath::{Point3, Vector3},
    /// #    approx::assert_ulps_eq,
    /// # };
    /// let aabb_a = Aabb::new(Point3::new(1., 1., 1.), Point3::new(2., 5., 4.));
    /// let aabb_b = Aabb::new(Point3::new(0., 1., -1.), Point3::new(2., 5., 3.));
    ///
    /// let intersection = aabb_a.intersection(aabb_b).unwrap();
    ///
    /// assert_ulps_eq!(intersection.min, Point3::new(1., 1., 1.));
    /// assert_ulps_eq!(intersection.max, Point3::new(2., 5., 3.));
    /// ```
    pub fn intersection(self, other: Aabb) -> Option<Aabb> {
        let min = Point3::new(
            self.min.x.max(other.min.x),
            self.min.y.max(other.min.y),
            self.min.z.max(other.min.z),
        );
        let max = Point3::new(
            self.max.x.min(other.max.x),
            self.max.y.min(other.max.y),
            self.max.z.min(other.max.z),
        );
        if max.x > min.x && max.y > min.y && max.z > min.z {
            Some(Aabb { min, max })
        } else {
            None
        }
    }

    /// Get whether two AABBs intersect.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #    tinytown_collision::Aabb,
    /// #    cgmath::{Point3, Vector3},
    /// # };
    /// let aabb_a = Aabb::new(Point3::new(1., 1., 1.), Point3::new(3., 3., 5.));
    /// let aabb_b = Aabb::new(Point3::new(0., 1., -1.), Point3::new(2., 5., 3.));
    ///
    /// assert!(aabb_a.intersects(aabb_b));
    /// ```
    pub fn intersects(self, other: Aabb) -> bool {
        // TODO: maybe make more efficient?
        self.intersection(other).is_some()
    }

    /// Sweeps the AABB along a vector and returns the minimum AABB around that.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #    tinytown_collision::Aabb,
    /// #    cgmath::{Point3, Vector3},
    /// #    approx::assert_ulps_eq,
    /// # };
    /// let aabb = Aabb::new(Point3::new(1., 1., 1.), Point3::new(3., 3., 5.));
    ///
    /// let expanded_aabb = aabb.expand_out(Vector3::new(2., -1., 4.));
    ///
    /// assert_ulps_eq!(expanded_aabb.min, Point3::new(1., 0., 1.));
    /// assert_ulps_eq!(expanded_aabb.max, Point3::new(5., 3., 9.));
    /// ```
    pub fn expand_out(self, sweep: Vector3<f32>) -> Aabb {
        // TODO: there might be a better way to do this (probably is)
        let new = Aabb::new(self.min + sweep, self.max + sweep);
        self.union(new)
    }

    /// Calculates the minimum AABB around the union of two AABBs.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #    tinytown_collision::Aabb,
    /// #    cgmath::{Point3, Vector3},
    /// #    approx::assert_ulps_eq,
    /// # };
    /// let aabb_a = Aabb::new(Point3::new(1., 1., 1.), Point3::new(3., 3., 5.));
    /// let aabb_b = Aabb::new(Point3::new(5., 1., -7.), Point3::new(9., 3., 3.));
    ///
    /// let union = aabb_a.union(aabb_b);
    ///
    /// assert_ulps_eq!(union.min, Point3::new(1., 1., -7.));
    /// assert_ulps_eq!(union.max, Point3::new(9., 3., 5.));
    /// ```
    pub fn union(self, other: Aabb) -> Aabb {
        let min = Point3::new(
            self.min.x.min(other.min.x),
            self.min.y.min(other.min.y),
            self.min.z.min(other.min.z),
        );
        let max = Point3::new(
            self.max.x.max(other.max.x),
            self.max.y.max(other.max.y),
            self.max.z.max(other.max.z),
        );
        Aabb { min, max }
    }

    /// Calculates the volume of the AABB.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #    tinytown_collision::Aabb,
    /// #    cgmath::{Point3, Vector3},
    /// #    approx::assert_ulps_eq,
    /// # };
    /// let aabb = Aabb::new(Point3::new(1., 1., 1.), Point3::new(3., 3., 5.));
    ///
    /// assert_ulps_eq!(aabb.volume(), 16.);
    /// ```
    pub fn volume(self) -> f32 {
        (self.max - self.min).product()
    }

    /// Expands the AABB in all directions.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #    tinytown_collision::Aabb,
    /// #    cgmath::{Point3, Vector3},
    /// #    approx::assert_ulps_eq,
    /// # };
    /// let aabb = Aabb::new(Point3::new(1., 1., 1.), Point3::new(3., 3., 5.));
    ///
    /// let expanded_aabb = aabb.expand(1.);
    ///
    /// assert_ulps_eq!(expanded_aabb.min, Point3::new(0., 0., 0.));
    /// assert_ulps_eq!(expanded_aabb.max, Point3::new(4., 4., 6.));
    /// ```
    pub fn expand(self, margin: f32) -> Aabb {
        let margins = Vector3::new(margin, margin, margin);
        let min = self.min - margins;
        let max = self.max + margins;
        Aabb { min, max }
    }

    /// Get the normal at a specific point.
    /// If the point is not on the surface, get the normal of the closest point on the surface.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #    tinytown_collision::Aabb,
    /// #    cgmath::{Point3, Vector3},
    /// #    approx::assert_ulps_eq,
    /// # };
    /// let aabb = Aabb::new(Point3::new(1., 1., 1.), Point3::new(3., 3., 5.));
    ///
    /// assert_ulps_eq!(aabb.normal_at(Point3::new(3., 2., 3.)), Vector3::new( 1.,  0.,  0.));
    /// assert_ulps_eq!(aabb.normal_at(Point3::new(2., 3., 3.)), Vector3::new( 0.,  1.,  0.));
    /// assert_ulps_eq!(aabb.normal_at(Point3::new(2., 2., 5.)), Vector3::new( 0.,  0.,  1.));
    /// assert_ulps_eq!(aabb.normal_at(Point3::new(1., 2., 3.)), Vector3::new(-1.,  0.,  0.));
    /// assert_ulps_eq!(aabb.normal_at(Point3::new(2., 1., 3.)), Vector3::new( 0., -1.,  0.));
    /// assert_ulps_eq!(aabb.normal_at(Point3::new(2., 2., 1.)), Vector3::new( 0.,  0., -1.));
    /// ```
    pub fn normal_at(self, pos: Point3<f32>) -> Vector3<f32> {
        let dir = pos - self.center();
        let dir_abs = dir.div_element_wise(self.size()).map(|x| x.abs());
        if dir_abs.x > dir_abs.y {
            if dir_abs.x > dir_abs.z {
                Vector3::new(dir.x.signum(), 0., 0.)
            } else {
                Vector3::new(0., 0., dir.z.signum())
            }
        } else if dir_abs.y > dir_abs.z {
            Vector3::new(0., dir.y.signum(), 0.)
        } else {
            Vector3::new(0., 0., dir.z.signum())
        }
    }

    /// Gets whether a point is inside an AABB.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #    tinytown_collision::Aabb,
    /// #    cgmath::{Point3, Vector3},
    /// # };
    /// let aabb = Aabb::new(Point3::new(1., 1., 1.), Point3::new(3., 3., 5.));
    ///
    /// assert!(aabb.contains_point(Point3::new(1.5, 2.5, 4.)));
    /// assert!(!aabb.contains_point(Point3::new(1.5, 2.5, 6.)));
    /// ```
    pub fn contains_point(self, point: Point3<f32>) -> bool {
        point.x >= self.min.x
            && point.y >= self.min.y
            && point.z >= self.min.z
            && point.x <= self.max.x
            && point.y <= self.max.y
            && point.z <= self.max.z
    }

    /// Calculates whether an AABB is completely contained within this one.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     cgmath::{Vector3, Point3},
    /// #     tinytown_collision::Aabb,
    /// # };
    /// let aabb_a = Aabb::new(Point3::new(0., -0.5, 1.), Point3::new(2., 1.5, 3.));
    /// let aabb_b = Aabb::new(Point3::new(1.3, 0.1, 1.2), Point3::new(1.7, 0.5, 1.8));
    ///
    /// assert!(aabb_a.contains_aabb(aabb_b));
    /// ```
    pub fn contains_aabb(self, other: Aabb) -> bool {
        other.min.x >= self.min.x
            && other.min.y >= self.min.y
            && other.min.z >= self.min.z
            && other.max.x <= self.max.x
            && other.max.y <= self.max.y
            && other.max.z <= self.max.z
    }

    /// Calculate the length along the ray when it hits the first point inside the AABB.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #    tinytown_collision::{Aabb, Ray},
    /// #    cgmath::{Point3, Vector3},
    /// #    approx::assert_ulps_eq,
    /// # };
    /// let aabb = Aabb::new(Point3::new(1., 1., 1.), Point3::new(3., 3., 5.));
    /// let ray = Ray::new(Point3::new(-10., 2., 3.), Vector3::new(1., 0., 0.));
    ///
    /// assert_ulps_eq!(aabb.cast_ray(ray).unwrap(), 11.);
    /// ```
    pub fn cast_ray(self, ray: Ray) -> Option<f32> {
        if self.contains_point(ray.origin) {
            Some(0.)
        } else {
            let mut t = [0.; 3];
            let dir: [f32; 3] = *ray.dir.as_ref();
            let start: [f32; 3] = *ray.origin.as_ref();
            let min: [f32; 3] = *self.min.as_ref();
            let max: [f32; 3] = *self.max.as_ref();
            for i in 0..3 {
                if dir[i] == 0. {
                    t[i] = 0.;
                } else if dir[i] > 0. {
                    t[i] = (min[i] - start[i]) / dir[i];
                } else {
                    t[i] = (max[i] - start[i]) / dir[i];
                }
            }
            let mi = if t[0] > t[1] {
                if t[0] > t[2] {
                    0
                } else {
                    2
                }
            } else if t[1] > t[2] {
                1
            } else {
                2
            };
            if t[mi] > 0. {
                let pt: [f32; 3] = *ray.point_at(t[mi]).as_ref();
                let i2 = (mi + 1) % 3;
                let i3 = (mi + 2) % 3;
                if pt[i2] >= min[i2] && pt[i2] <= max[i2] && pt[i3] >= min[i3] && pt[i3] <= max[i3]
                {
                    Some(t[mi])
                } else {
                    None
                }
            } else {
                None
            }
        }
    }
}

impl AbsDiffEq for Aabb {
    type Epsilon = f32;

    fn default_epsilon() -> f32 {
        f32::default_epsilon()
    }

    fn abs_diff_eq(&self, other: &Self, epsilon: f32) -> bool {
        AbsDiffEq::abs_diff_eq(&self.min, &other.min, epsilon)
            && AbsDiffEq::abs_diff_eq(&self.max, &other.max, epsilon)
    }
}

impl RelativeEq for Aabb {
    fn default_max_relative() -> f32 {
        f32::default_max_relative()
    }

    fn relative_eq(&self, other: &Self, epsilon: f32, max_relative: f32) -> bool {
        RelativeEq::relative_eq(&self.min, &other.min, epsilon, max_relative)
            && RelativeEq::relative_eq(&self.max, &other.max, epsilon, max_relative)
    }
}

impl UlpsEq for Aabb {
    fn default_max_ulps() -> u32 {
        f32::default_max_ulps()
    }

    fn ulps_eq(&self, other: &Self, epsilon: f32, max_ulps: u32) -> bool {
        UlpsEq::ulps_eq(&self.min, &other.min, epsilon, max_ulps)
            && UlpsEq::ulps_eq(&self.max, &other.max, epsilon, max_ulps)
    }
}

impl Add<Vector3<f32>> for Aabb {
    type Output = Aabb;

    fn add(self, offset: Vector3<f32>) -> Aabb {
        let min = self.min + offset;
        let max = self.max + offset;
        Aabb { min, max }
    }
}

impl AddAssign<Vector3<f32>> for Aabb {
    fn add_assign(&mut self, offset: Vector3<f32>) {
        self.min += offset;
        self.max += offset;
    }
}

impl Sub<Vector3<f32>> for Aabb {
    type Output = Aabb;

    fn sub(self, offset: Vector3<f32>) -> Aabb {
        let min = self.min - offset;
        let max = self.max - offset;
        Aabb { min, max }
    }
}

impl SubAssign<Vector3<f32>> for Aabb {
    fn sub_assign(&mut self, offset: Vector3<f32>) {
        self.min -= offset;
        self.max -= offset;
    }
}

#[cfg(test)]
mod tests {
    use {
        approx::assert_ulps_eq,
        cgmath::{Point3, Vector3},
    };

    use super::*;

    #[test]
    fn aabb_normal_at() {
        let aabb = Aabb::new(Point3::new(0., 0.2, 0.), Point3::new(2.5, 0.4, 2.5));

        let surface_pos = Point3::new(0.25, 0.4, 0.25);

        assert_ulps_eq!(aabb.normal_at(surface_pos), Vector3::new(0., 1., 0.));
    }
}
