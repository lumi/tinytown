#![deny(unsafe_code)]

mod aabb;
mod consts;
mod contact;
mod mask;
mod math_ext;
mod midpoint;
mod ray;
mod raycast;
mod shape;
mod support;
#[cfg(test)]
mod tests;

pub mod broad;
pub mod narrow;

pub use crate::{
    aabb::Aabb,
    contact::{Contact, ContactPair},
    mask::CollisionMask,
    math_ext::EuclidElementWise,
    midpoint::Midpoint,
    ray::Ray,
    raycast::RaycastHit,
    shape::{Shape, TransformedShape},
    support::{MinkowskiDifference, Support},
};
