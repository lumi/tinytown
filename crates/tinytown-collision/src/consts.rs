pub const TOLERANCE: f32 = 0.00001;
pub const GJK_MAX_ITERATIONS: usize = 200;
#[cfg(not(test))]
pub const EPA_MAX_ITERATIONS: usize = 200;
#[cfg(test)]
pub const EPA_MAX_ITERATIONS: usize = 1000;
