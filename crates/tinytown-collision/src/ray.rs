#![deny(missing_docs)]

use cgmath::{Point3, Vector3};

/// A ray with an origin and a direction.
/// The direction is assumed to be a unit vector.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Ray {
    /// The origin point of the ray.
    pub origin: Point3<f32>,
    /// The direction the ray is going.
    pub dir: Vector3<f32>,
}

impl Ray {
    /// Create a new ray from an origin and a direction.
    /// The direction is assumed to be a unit vector.
    ///
    /// ```rust
    /// # use {
    /// #     tinytown_collision::Ray,
    /// #     cgmath::{Point3, Vector3},
    /// #     approx::assert_ulps_eq,
    /// # };
    /// let ray = Ray::new(Point3::new(1., 2., 3.), Vector3::new(0., 1., 0.));
    ///
    /// assert_ulps_eq!(ray.origin, Point3::new(1., 2., 3.));
    /// assert_ulps_eq!(ray.dir, Vector3::new(0., 1., 0.));
    /// ```
    pub fn new(origin: Point3<f32>, dir: Vector3<f32>) -> Ray {
        Ray { origin, dir }
    }

    /// Get a point along the ray at distance a distance `t`.
    ///
    /// ```rust
    /// # use {
    /// #     tinytown_collision::Ray,
    /// #     cgmath::{Point3, Vector3},
    /// #     approx::assert_ulps_eq,
    /// # };
    /// let ray = Ray::new(Point3::new(1., 2., 3.), Vector3::new(0., 1., 0.));
    ///
    /// assert_ulps_eq!(ray.point_at(1.), Point3::new(1., 3., 3.));
    /// assert_ulps_eq!(ray.point_at(2.5), Point3::new(1., 4.5, 3.));
    /// ```
    pub fn point_at(self, t: f32) -> Point3<f32> {
        self.origin + t * self.dir
    }

    /// Calculate the point where the y coordinate drops to 0.
    ///
    /// ```rust
    /// # use {
    /// #     tinytown_collision::Ray,
    /// #     cgmath::{Point3, Vector3},
    /// #     approx::assert_ulps_eq,
    /// # };
    /// let ray_a = Ray::new(Point3::new(1., 2., 3.), Vector3::new(0., -1., 0.));
    /// let ray_b = Ray::new(Point3::new(1., 2., 3.), Vector3::new(0., 1., 0.));
    ///
    /// assert_ulps_eq!(ray_a.intersects_floor().unwrap(), 2.);
    /// assert_eq!(ray_b.intersects_floor(), None);
    /// ```
    pub fn intersects_floor(self) -> Option<f32> {
        if self.dir.y >= 0. {
            None
        } else {
            Some(self.origin.y / -self.dir.y)
        }
    }
}
