#![deny(missing_docs)]

//! A module containing an AABB Bounding Volume Hierarchy.

use {slotmap::HopSlotMap, std::collections::VecDeque};

use crate::{Aabb, Ray, RaycastHit};

slotmap::new_key_type! {
    struct NodeIndex;
}

const FAT_AABB_MARGIN: f32 = 0.2;

#[derive(Debug, Clone, Copy)]
struct Node<V: Copy + PartialEq> {
    parent_idx: Option<NodeIndex>,
    tree_aabb: Aabb,
    inner: NodeInner<V>,
}

#[derive(Debug, Clone, Copy)]
enum NodeInner<V: Copy + PartialEq> {
    Leaf { aabb: Aabb, value: V },
    Branch { children: [NodeIndex; 2] },
}

impl<V: Copy + PartialEq> Node<V> {
    fn leaf(aabb: Aabb, value: V) -> Node<V> {
        Node {
            parent_idx: None,
            tree_aabb: aabb.expand(FAT_AABB_MARGIN),
            inner: NodeInner::Leaf { aabb, value },
        }
    }
}

/// A Bounding Volume Hierarchy, implemented using 3d AABBs.
#[derive(Debug, Clone)]
pub struct Bvh<V: Copy + PartialEq> {
    root_idx: Option<NodeIndex>,
    nodes: HopSlotMap<NodeIndex, Node<V>>,
    queue: VecDeque<NodeIndex>,
}

impl<V: Copy + PartialEq> Bvh<V> {
    /// Create a new bounding volume hierarchy.
    pub fn new() -> Self {
        Self {
            root_idx: None,
            nodes: HopSlotMap::with_key(),
            queue: VecDeque::new(),
        }
    }

    /// Delete all of the nodes in this BVH.
    pub fn clear(&mut self) {
        self.root_idx = None;
        self.nodes.clear();
    }

    /// Query an AABB in the tree, this will return all AABBs that intersect this one.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown_collision::{Aabb, broad::bvh::{Bvh, AabbQueryCache}},
    /// #     cgmath::{Point3, Vector3},
    /// # };
    /// let mut bvh = Bvh::new();
    ///
    /// let aabb_a = Aabb::new(Point3::new(0., 0., 0.), Point3::new(2., 2., 2.));
    /// let aabb_b = Aabb::new(Point3::new(-2., -2., -2.), Point3::new(0., 0., 0.));
    /// let aabb_c = Aabb::new(Point3::new(-8., -9.5, -9.), Point3::new(12., 10.5, 11.));
    /// let aabb_d = Aabb::new(Point3::new(-0.5, -0.5, -2.), Point3::new(3.5, 3.5, 2.));
    ///
    /// bvh.add(aabb_a, 0);
    /// bvh.add(aabb_b, 1);
    /// bvh.add(aabb_c, 2);
    /// bvh.add(aabb_d, 3);
    ///
    /// let query_aabb = Aabb::new(Point3::new(0.5, 0.5, -1.), Point3::new(2.5, 2.5, 2.));
    ///
    /// let mut found = [false; 4];
    /// let contains = [true, false, true, true];
    ///
    /// let mut cache = AabbQueryCache::new();
    ///
    /// for num in bvh.query_aabb(&mut cache, query_aabb) {
    ///     found[num] = true;
    /// }
    ///
    /// assert_eq!(found, contains);
    /// ```
    pub fn query_aabb<'a, 'b>(
        &'a self,
        cache: &'b mut AabbQueryCache,
        query_aabb: Aabb,
    ) -> AabbQueryIterator<'a, 'b, V> {
        cache.clear();
        if let Some(root_idx) = self.root_idx {
            cache.queue.push_front(root_idx);
        }
        AabbQueryIterator {
            bvh: self,
            queue: &mut cache.queue,
            query_aabb,
        }
    }

    /// Add an AABB to the bounding volume hierarchy.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown_collision::{Aabb, broad::bvh::Bvh},
    /// #     cgmath::{Point3, Vector3},
    /// # };
    /// let mut bvh = Bvh::new();
    ///
    /// let aabb_a = Aabb::new(Point3::new(0., 0., 0.), Point3::new(2., 2., 2.));
    /// let aabb_b = Aabb::new(Point3::new(-2., -2., -2.), Point3::new(0., 0., 0.));
    /// let aabb_c = Aabb::new(Point3::new(-8., -9.5, -9.), Point3::new(12., 10.5, 11.));
    /// let aabb_d = Aabb::new(Point3::new(-0.5, -0.5, -2.), Point3::new(3.5, 3.5, 2.));
    ///
    /// bvh.add(aabb_a, 1);
    /// bvh.add(aabb_b, 2);
    /// bvh.add(aabb_c, 3);
    /// bvh.add(aabb_d, 4);
    /// ```
    pub fn add(&mut self, aabb: Aabb, value: V) {
        let node = Node::leaf(aabb, value);
        match self.root_idx {
            Some(root_idx) => {
                self.add_to_node(root_idx, node);
            }
            None => {
                let root_idx = self.nodes.insert(node);
                self.root_idx = Some(root_idx);
            }
        }
    }

    fn add_to_node(&mut self, cur_idx: NodeIndex, new: Node<V>) {
        let current = self.nodes[cur_idx];
        match current.inner {
            NodeInner::Leaf { .. } => {
                let mut current_clone = current.clone();
                current_clone.parent_idx = Some(cur_idx);
                let node_idx_a = self.nodes.insert(current_clone);
                let mut new_modified = new;
                new_modified.parent_idx = Some(cur_idx);
                let node_idx_b = self.nodes.insert(new_modified);
                let children = [node_idx_a, node_idx_b];
                let branch = Node {
                    parent_idx: current.parent_idx,
                    tree_aabb: current.tree_aabb.union(new.tree_aabb),
                    inner: NodeInner::Branch { children },
                };
                self.nodes[cur_idx] = branch;
            }
            NodeInner::Branch { children } => {
                // recurse into child with minimum volume
                let mut min_volume = f32::INFINITY;
                let mut min_index = None;
                for i in 0..children.len() {
                    let child_idx = children[i];
                    let child = self.nodes[child_idx];
                    let union = child.tree_aabb.union(new.tree_aabb);
                    let volume = union.volume();
                    if volume < min_volume {
                        min_volume = volume;
                        min_index = Some(child_idx);
                    }
                }
                self.nodes[cur_idx].tree_aabb = self.nodes[cur_idx].tree_aabb.union(new.tree_aabb);
                self.add_to_node(min_index.unwrap(), new);
            }
        }
    }

    /// Remove an AABB from the bounding volume hierarchy.
    ///
    /// Returns whether the node was actually removed.
    pub fn remove(&mut self, aabb: Aabb, value: V) -> bool {
        self.queue.clear();
        if let Some(root_idx) = self.root_idx {
            let root = self.nodes[root_idx];
            if let NodeInner::Leaf {
                value: leaf_value, ..
            } = root.inner
            {
                if value == leaf_value {
                    self.nodes.remove(root_idx);
                    self.root_idx = None;
                    return true;
                }
            } else {
                self.queue.push_front(root_idx);
            }
        }
        while let Some(node_idx) = self.queue.pop_back() {
            let node = self.nodes[node_idx];
            match node.inner {
                NodeInner::Leaf { .. } => (),
                NodeInner::Branch { ref children } => {
                    for (i, &child_idx) in children.iter().enumerate() {
                        let child = self.nodes[child_idx];
                        if !child.tree_aabb.contains_aabb(aabb) {
                            continue;
                        }
                        match child.inner {
                            NodeInner::Leaf {
                                value: leaf_value, ..
                            } => {
                                if value != leaf_value {
                                    continue;
                                }
                                let other_idx = children[1 - i];
                                self.nodes[other_idx].parent_idx = node.parent_idx;
                                if let Some(parent_idx) = node.parent_idx {
                                    let parent = &mut self.nodes[parent_idx];
                                    if let NodeInner::Branch { ref mut children } = parent.inner {
                                        for child_idx in children {
                                            if *child_idx == node_idx {
                                                *child_idx = other_idx;
                                            }
                                        }
                                    } else {
                                        unreachable!()
                                    }
                                } else if self.root_idx == Some(node_idx) {
                                    self.root_idx = Some(other_idx);
                                }
                                self.nodes.remove(node_idx);
                                self.nodes.remove(child_idx);
                                self.recalculate_aabbs(other_idx);
                                return true;
                            }
                            NodeInner::Branch { .. } => {
                                self.queue.push_front(child_idx);
                            }
                        }
                    }
                }
            }
        }
        false
    }

    fn recalculate_aabbs(&mut self, node_idx: NodeIndex) {
        let mut cur_idx = node_idx;
        loop {
            let node = self.nodes[cur_idx];
            match node.inner {
                NodeInner::Leaf { .. } => (),
                NodeInner::Branch { ref children } => {
                    let child_a = self.nodes[children[0]];
                    let child_b = self.nodes[children[1]];
                    self.nodes[cur_idx].tree_aabb = child_a.tree_aabb.union(child_b.tree_aabb);
                }
            }
            if let Some(parent_idx) = node.parent_idx {
                cur_idx = parent_idx;
            } else {
                break;
            }
        }
    }

    /// Update an AABB in the bounding volume hierarchy.
    ///
    /// Returns whether the node was actually updated.
    pub fn update(&mut self, old_aabb: Aabb, new_aabb: Aabb, old_value: V, new_value: V) -> bool {
        let ret = self.remove(old_aabb, old_value);
        if ret {
            self.add(new_aabb, new_value);
        }
        ret
    }

    /// Cast a ray into the BVH, fetching the first shape that the ray hits.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown_collision::{Aabb, Ray, RaycastHit, broad::bvh::Bvh},
    /// #     cgmath::{Point3, Vector3},
    /// #     approx::assert_ulps_eq,
    /// # };
    /// let mut bvh = Bvh::new();
    ///
    /// let aabbs = [
    ///     Aabb::new(Point3::new(0., 0., 0.), Point3::new(2., 2., 2.)),
    ///     Aabb::new(Point3::new(0.9, 0.5, 0.9), Point3::new(3.1, 2.5, 2.9)),
    ///     Aabb::new(Point3::new(5., -0.5, 0.), Point3::new(7., 1.5, 2.)),
    ///     Aabb::new(Point3::new(-11., 3., 8.), Point3::new(-9., 5., 10.)),
    /// ];
    ///
    /// for (i, &aabb) in aabbs.iter().enumerate() {
    ///     bvh.add(aabb, i);
    /// }
    ///
    /// let ray = Ray::new(Point3::new(10., 1., 1.), Vector3::new(-1., 0., 0.));
    ///
    /// let narrow_test = |idx| {
    ///     let aabb: Aabb = aabbs[idx];
    ///     let distance = aabb.cast_ray(ray).unwrap();
    ///     let point = ray.point_at(distance);
    ///     let normal = aabb.normal_at(point);
    ///     Some(RaycastHit { point, normal, distance, value: idx })
    /// };
    ///
    /// let hit = bvh.cast_ray(ray, 20., narrow_test).unwrap();
    ///
    /// assert_ulps_eq!(hit.distance, 3.);
    /// assert_ulps_eq!(hit.point, Point3::new(7., 1., 1.));
    /// assert_ulps_eq!(hit.normal, Vector3::new(1., 0., 0.));
    /// assert_eq!(hit.value, 2);
    /// ```
    pub fn cast_ray<A>(
        &self,
        ray: Ray,
        limit: f32,
        mut narrow_test: impl FnMut(V) -> Option<RaycastHit<A>>,
    ) -> Option<RaycastHit<A>> {
        if let Some(root_idx) = self.root_idx {
            let mut queue = VecDeque::with_capacity(1);
            let mut best_dist = limit;
            let mut best_result = None;
            queue.push_back(root_idx);
            while let Some(item) = queue.pop_front() {
                let node = self.nodes[item];
                let tightest_aabb = match node.inner {
                    NodeInner::Leaf { aabb, .. } => aabb,
                    NodeInner::Branch { .. } => node.tree_aabb,
                };
                if let Some(node_dist) = tightest_aabb.cast_ray(ray) {
                    if node_dist >= best_dist {
                        continue;
                    }
                    match node.inner {
                        NodeInner::Leaf { value, .. } => {
                            if let Some(narrow_hit) = narrow_test(value) {
                                if narrow_hit.distance < best_dist {
                                    best_dist = narrow_hit.distance;
                                    best_result = Some(narrow_hit);
                                }
                            }
                        }
                        NodeInner::Branch { children } => {
                            queue.push_back(children[0]);
                            queue.push_back(children[1]);
                        }
                    }
                }
            }
            best_result
        } else {
            None
        }
    }

    /// Find all pairs of colliding nodes.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     tinytown_collision::{Aabb, Contact, ContactPair, broad::bvh::{Bvh, CollidingPairsCache}},
    /// #     cgmath::{Point3, Vector3},
    /// # };
    /// let mut bvh = Bvh::new();
    ///
    /// let aabb_a = Aabb::new(Point3::new(0., 0., 0.), Point3::new(2., 2., 2.));
    /// let aabb_b = Aabb::new(Point3::new(-2., -2., -2.), Point3::new(1., 1., 1.));
    /// let aabb_c = Aabb::new(Point3::new(-8., -9.5, -9.), Point3::new(-3., -3., -5.));
    /// let aabb_d = Aabb::new(Point3::new(10.5, -0.5, -2.), Point3::new(13.5, 3.5, 2.));
    ///
    /// bvh.add(aabb_a, 0);
    /// bvh.add(aabb_b, 1);
    /// bvh.add(aabb_c, 2);
    /// bvh.add(aabb_d, 3);
    ///
    /// let mut found = false;
    ///
    /// let mut cache = CollidingPairsCache::new();
    ///
    /// for (value_a, value_b) in bvh.query_colliding_pairs(&mut cache) {
    ///     if value_a == 1 && value_b == 0 || value_a == 0 && value_b == 1 {
    ///         found = true;
    ///     }
    ///     else {
    ///         panic!("unexpected values in set: {:?}", (value_a, value_b));
    ///     }
    /// }
    ///
    /// assert!(found);
    /// ```
    pub fn query_colliding_pairs<'a, 'b>(
        &'a self,
        cache: &'b mut CollidingPairsCache,
    ) -> CollidingPairsIterator<'a, 'b, V> {
        cache.clear();
        if let Some(root_idx) = self.root_idx {
            let root = self.nodes[root_idx];
            match root.inner {
                NodeInner::Leaf { .. } => (),
                NodeInner::Branch { children } => {
                    cache.queue.push_front((children[0], children[1]));
                }
            }
        }
        CollidingPairsIterator {
            bvh: self,
            queue: &mut cache.queue,
        }
    }

    /// Iterator over everything contained in this bounding volume hierarchy.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use {
    /// #     std::collections::HashSet,
    /// #     tinytown_collision::{Aabb, Contact, ContactPair, broad::bvh::{Bvh, CollidingPairsCache}},
    /// #     cgmath::{Point3, Vector3},
    /// # };
    /// let mut bvh = Bvh::new();
    ///
    /// let aabbs = [
    ///     Aabb::new(Point3::new(0., 0., 0.), Point3::new(2., 2., 2.)),
    ///     Aabb::new(Point3::new(0.9, 0.5, 0.9), Point3::new(3.1, 2.5, 2.9)),
    ///     Aabb::new(Point3::new(5., -0.5, 0.), Point3::new(7., 1.5, 2.)),
    ///     Aabb::new(Point3::new(-11., 3., 8.), Point3::new(-9., 5., 10.)),
    /// ];
    ///
    /// let mut hs = HashSet::new();
    ///
    /// for (i, &aabb) in aabbs.iter().enumerate() {
    ///     bvh.add(aabb, i);
    ///     hs.insert(i);
    /// }
    ///
    /// for idx in bvh.iter() {
    ///     assert!(hs.remove(idx));
    /// }
    ///
    /// assert!(hs.is_empty());
    /// ```
    pub fn iter(&self) -> impl Iterator<Item = &V> {
        self.nodes.values().filter_map(|node| match node.inner {
            NodeInner::Leaf { ref value, .. } => Some(value),
            NodeInner::Branch { .. } => None,
        })
    }
}

#[cfg(test)]
impl<V: Copy + PartialEq> Bvh<V> {
    #[doc(hidden)]
    pub fn check_invariants(&self) {
        self.check_parents_contain_children();
        self.check_parent_child_relationship();
        self.check_parent_wraps_tightly();
    }

    #[doc(hidden)]
    pub fn check_parents_contain_children(&self) {
        if let Some(root_idx) = self.root_idx {
            self.check_parents_contain_children_inner(root_idx);
        }
    }

    fn check_parents_contain_children_inner(&self, node_idx: NodeIndex) {
        let node = self.nodes[node_idx];
        match node.inner {
            NodeInner::Leaf { aabb, .. } => {
                assert!(node.tree_aabb.contains_aabb(aabb));
            }
            NodeInner::Branch { ref children } => {
                for &child_idx in children {
                    let child = self.nodes[child_idx];
                    assert!(node.tree_aabb.contains_aabb(child.tree_aabb));
                    self.check_parents_contain_children_inner(child_idx);
                }
            }
        }
    }

    #[doc(hidden)]
    pub fn check_parent_child_relationship(&self) {
        if let Some(root_idx) = self.root_idx {
            let root = self.nodes[root_idx];
            assert!(root.parent_idx.is_none());
            self.check_parent_child_relationship_inner(root_idx);
        }
    }

    fn check_parent_child_relationship_inner(&self, node_idx: NodeIndex) {
        let node = self.nodes[node_idx];
        match node.inner {
            NodeInner::Leaf { .. } => (),
            NodeInner::Branch { ref children } => {
                for &child_idx in children {
                    let child = self.nodes[child_idx];
                    assert_eq!(child.parent_idx, Some(node_idx));
                    self.check_parent_child_relationship_inner(child_idx);
                }
            }
        }
    }

    #[doc(hidden)]
    pub fn check_parent_wraps_tightly(&self) {
        if let Some(root_idx) = self.root_idx {
            self.check_parent_wraps_tightly_inner(root_idx);
        }
    }

    fn check_parent_wraps_tightly_inner(&self, node_idx: NodeIndex) {
        let node = self.nodes[node_idx];
        match node.inner {
            NodeInner::Leaf { .. } => (),
            NodeInner::Branch { ref children } => {
                let child_a = self.nodes[children[0]];
                let child_b = self.nodes[children[1]];
                let union_aabb = child_a.tree_aabb.union(child_b.tree_aabb);
                approx::assert_ulps_eq!(union_aabb, node.tree_aabb);
            }
        }
    }
}

#[cfg(test)]
impl<V: Copy + PartialEq + std::fmt::Debug> Bvh<V> {
    #[doc(hidden)]
    pub fn debug_print(&self) {
        if let Some(root_idx) = self.root_idx {
            self.debug_print_inner(0, root_idx);
        } else {
            eprintln!("bvh: empty");
        }
    }

    fn debug_print_inner(&self, depth: usize, node_idx: NodeIndex) {
        let node = self.nodes[node_idx];
        let mut prefix = String::with_capacity(depth * 2);
        for _ in 0..depth * 2 {
            prefix.push(' ');
        }
        match node.inner {
            NodeInner::Leaf { aabb, value } => {
                eprintln!("{}leaf {:?}", prefix, node.tree_aabb);
                eprintln!("{}  aabb: {:?}", prefix, aabb);
                eprintln!("{}  value: {:?}", prefix, value);
            }
            NodeInner::Branch { ref children } => {
                eprintln!("{}branch {:?}", prefix, node.tree_aabb);
                for &child_idx in children {
                    self.debug_print_inner(depth + 1, child_idx);
                }
            }
        }
    }
}

/// A cache which can be re-used to reduce allocations in AABB queries.
pub struct AabbQueryCache {
    queue: VecDeque<NodeIndex>,
}

impl AabbQueryCache {
    /// Create a new cache.
    pub fn new() -> AabbQueryCache {
        AabbQueryCache {
            queue: VecDeque::new(),
        }
    }

    fn clear(&mut self) {
        self.queue.clear();
    }
}

/// An iterator for iterating through AABBs which intersect this one.
pub struct AabbQueryIterator<'a, 'b, V: Copy + PartialEq> {
    bvh: &'a Bvh<V>,
    queue: &'b mut VecDeque<NodeIndex>,
    query_aabb: Aabb,
}

impl<'a, 'b, V: Copy + PartialEq> Iterator for AabbQueryIterator<'a, 'b, V> {
    type Item = V;

    fn next(&mut self) -> Option<Self::Item> {
        while let Some(node_idx) = self.queue.pop_back() {
            let current = self.bvh.nodes[node_idx];
            match current.inner {
                NodeInner::Leaf { aabb, value } => {
                    if self.query_aabb.intersects(aabb) {
                        return Some(value);
                    }
                }
                NodeInner::Branch { ref children } => {
                    for &child_idx in children {
                        let child = self.bvh.nodes[child_idx];
                        if self.query_aabb.intersects(child.tree_aabb) {
                            self.queue.push_back(child_idx);
                        }
                    }
                }
            }
        }
        None
    }
}

/// A cache which can be re-used to reduce allocations in colliding pairs queries.
pub struct CollidingPairsCache {
    queue: VecDeque<(NodeIndex, NodeIndex)>,
}

impl CollidingPairsCache {
    /// Create a new cache.
    pub fn new() -> CollidingPairsCache {
        CollidingPairsCache {
            queue: VecDeque::new(),
        }
    }

    fn clear(&mut self) {
        self.queue.clear();
    }
}

/// An iterator for iterating through pairs of colliding AABBs.
pub struct CollidingPairsIterator<'a, 'b, V: Copy + PartialEq> {
    bvh: &'a Bvh<V>,
    queue: &'b mut VecDeque<(NodeIndex, NodeIndex)>,
}

impl<'a, 'b, V: Copy + PartialEq> Iterator for CollidingPairsIterator<'a, 'b, V> {
    type Item = (V, V);

    fn next(&mut self) -> Option<Self::Item> {
        while let Some((child_a_idx, child_b_idx)) = self.queue.pop_back() {
            let child_a = self.bvh.nodes[child_a_idx];
            let child_b = self.bvh.nodes[child_b_idx];
            match (child_a.inner, child_b.inner) {
                (
                    NodeInner::Leaf {
                        aabb: aabb_a,
                        value: value_a,
                    },
                    NodeInner::Leaf {
                        aabb: aabb_b,
                        value: value_b,
                    },
                ) => {
                    if aabb_a.intersects(aabb_b) {
                        return Some((value_a, value_b));
                    }
                }
                (NodeInner::Leaf { aabb, .. }, NodeInner::Branch { children }) => {
                    self.queue.push_front((children[0], children[1]));
                    if aabb.intersects(child_b.tree_aabb) {
                        self.queue.push_front((child_a_idx, children[0]));
                        self.queue.push_front((child_a_idx, children[1]));
                    }
                }
                (NodeInner::Branch { children }, NodeInner::Leaf { aabb, .. }) => {
                    self.queue.push_front((children[0], children[1]));
                    if child_a.tree_aabb.intersects(aabb) {
                        self.queue.push_front((children[0], child_b_idx));
                        self.queue.push_front((children[1], child_b_idx));
                    }
                }
                (
                    NodeInner::Branch {
                        children: children_a,
                    },
                    NodeInner::Branch {
                        children: children_b,
                    },
                ) => {
                    self.queue.push_front((children_a[0], children_a[1]));
                    self.queue.push_front((children_b[0], children_b[1]));
                    if child_a.tree_aabb.intersects(child_b.tree_aabb) {
                        self.queue.push_front((children_a[0], children_b[0]));
                        self.queue.push_front((children_a[0], children_b[1]));
                        self.queue.push_front((children_a[1], children_b[0]));
                        self.queue.push_front((children_a[1], children_b[1]));
                    }
                }
            }
        }
        None
    }
}
